var config = require('./config');
var app = require('./server/api');
var logger = require('./server/utils/logger');
var fs = require('fs');

require('http').createServer(app).listen(config.server.port, function () {
    // var options = {
    //     key: fs.readFileSync('./cert/cert.crt'),
    //     cert: fs.readFileSync('./cert/private.pem')
    // };
    //
    // console.log('key', options.key.toString());
    // console.log('cert', options.cert.toString());
    logger.log('info', "Web server successfully started at port %d", config.server.port);
});
