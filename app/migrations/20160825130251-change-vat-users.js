'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function changeVatUsers() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('users', 'vat',
            {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield changeVatUsers();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function resetChangesVat() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('users', 'vat',
            {
              type: Sequelize.INTEGER,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield resetChangesVat();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
