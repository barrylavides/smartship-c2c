'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addDocumentAccessForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'document_access',
            {
              type: Sequelize.INTEGER,
              defaultValue: 1
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addDocumentAccessForOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeDocumentAccessColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'document_access').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeDocumentAccessColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
