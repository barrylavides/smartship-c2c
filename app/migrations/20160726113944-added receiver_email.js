'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addReceiverEmailOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'receiver_email',
            {
              type: Sequelize.STRING,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addReceiverEmailOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeEmailColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'receiver_email').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeEmailColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
