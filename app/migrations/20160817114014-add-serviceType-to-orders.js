'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addServiceTypeForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'serviceType',
            {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addServiceTypeForOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeServiceTypeColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'serviceType').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeServiceTypeColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
