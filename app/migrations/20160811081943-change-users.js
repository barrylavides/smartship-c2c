'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function removeFbID() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('users', 'facebook_id').then(function () {
          resolve();
        });
      });
    };
    
    function removeMainEmail() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('users', 'mainEmail').then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield removeFbID();
        yield removeMainEmail();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function addFacebookIDColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('users', 'facebook_id',
            {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    }

    function addMainEmail() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('users', 'mainEmail',
            {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    }

    (() => {
      co(function *() {
        yield addFacebookIDColumn();
        yield addMainEmail();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};