'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initPanBanks() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('panBanks',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              name: {
                type: Sequelize.STRING
              },
              value: {
                type: Sequelize.STRING
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initPanBanks();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removePanBanks() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('panBanks').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removePanBanks();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
