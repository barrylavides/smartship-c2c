'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addPackageFlagForUsers() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('users', 'have_package',
            {
              type: Sequelize.INTEGER,
              defaultValue: 0
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addPackageFlagForUsers();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removePackageFlagColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('users', 'have_package').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removePackageFlagColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
