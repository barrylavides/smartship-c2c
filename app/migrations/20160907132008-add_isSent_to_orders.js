'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addIsSendForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'is_sent',
            {
              type: Sequelize.INTEGER
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addIsSendForOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeIsSentColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'is_sent').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeIsSentColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
