'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addStatusForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'status',
            {
              type: Sequelize.INTEGER,
              allowNull: true,
              defaultValue: 0
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function addTrackingIdForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'tracking_id',
            {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addStatusForOrders();
        yield addTrackingIdForOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeStatusColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'status').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    function removeTrackingIdColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'tracking_id').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeStatusColumn();
        yield removeTrackingIdColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
