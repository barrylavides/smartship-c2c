'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initPackages() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('packages',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              name: {
                type: Sequelize.STRING
              },
              cost: {
                type: Sequelize.INTEGER
              },
              low_limit: {
                type: Sequelize.INTEGER
              },
              offers_limit: {
                type: Sequelize.INTEGER
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initPackages();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removePackages() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('packages').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removePackages();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
