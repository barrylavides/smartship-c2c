'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function renameSocialId() {
      return new Promise((resolve, reject) => {
        queryInterface.renameColumn('socials', 'socialId', 'social_id',
            {
              type: Sequelize.STRING,
              allowNull: false,
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function renameSocialType() {
      return new Promise((resolve, reject) => {
        queryInterface.renameColumn('socials', 'socialType', 'social_type',
            {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function renameSocialEmail() {
      return new Promise((resolve, reject) => {
        queryInterface.renameColumn('socials', 'socialEmail', 'social_email',
            {
              type: Sequelize.STRING
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };
    
    function changePasswordType() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('users', 'password',
            {
              type: Sequelize.TEXT
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield renameSocialId();
        yield renameSocialType();
        yield renameSocialEmail();
        yield changePasswordType();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  // down: function (queryInterface, Sequelize, done) {
  //   function addFacebookIDColumn() {
  //     return new Promise((resolve, reject) => {
  //       queryInterface.addColumn('users', 'facebook_id',
  //           {
  //             type: Sequelize.INTEGER,
  //             allowNull: false
  //           },
  //           {
  //             engine: 'InnoDB', // default: 'InnoDB'
  //             charset: 'utf8' // default: null
  //           }).then(function () {
  //         resolve();
  //       });
  //     });
  //   }
  //
  //   function addMainEmail() {
  //     return new Promise((resolve, reject) => {
  //       queryInterface.addColumn('users', 'mainEmail',
  //           {
  //             type: Sequelize.STRING,
  //             allowNull: true
  //           },
  //           {
  //             engine: 'InnoDB', // default: 'InnoDB'
  //             charset: 'utf8' // default: null
  //           }).then(function () {
  //         resolve();
  //       });
  //     });
  //   }
  //
  //   (() => {
  //     co(function *() {
  //       yield addFacebookIDColumn();
  //       yield addMainEmail();
  //     }).then(() => {
  //       done();
  //     }).catch(err => {
  //       console.log(err);
  //     }/*next(err)*/);
  //   })();
  //
  // }
};