'use strict';
const co = require("co");

module.exports = {
    up: function (queryInterface, Sequelize, done) {
        function initOrders() {
            return new Promise((resolve, reject) => {
                queryInterface.createTable('orders',
                    {
                        id: {
                            type: Sequelize.INTEGER,
                            primaryKey: true,
                            autoIncrement: true
                        },
                        user_id: {
                            type: Sequelize.INTEGER,
                            allowNull: false,
                            references: {
                                model: 'users',
                                key: 'id'
                            },
                            onUpdate: "NO ACTION",
                            onDelete: "CASCADE"
                        },
                        order_name: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        receiver_name: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        phone: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        postcode: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        district: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        province: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        address: {
                            type: Sequelize.STRING,
                            allowNull: false
                        },
                        created_at: {
                            type: Sequelize.DATE,
                            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                        },
                        updated_at: {
                            type: Sequelize.DATE,
                            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                        }
                    },
                    {
                        engine: 'InnoDB', // default: 'InnoDB'
                        charset: 'utf8' // default: null
                    }).then(function () {
                    resolve();
                });
            });
        };

        (() => {
            co(function *() {
                yield initOrders();
            }).then(function(){
                done();
            }).catch(function (err) {
                console.log(err);
            });
        })();
    },

    down: function (queryInterface, Sequelize, done) {
        function removeOrders() {
            return new Promise((resolve, reject) => {
                queryInterface.dropTable('orders').then(function () {
                    resolve();
                }).catch(function (err) {
                    reject(err);
                });
            })
        }
        (() => {
            co(function *() {
                yield removeOrders();
            }).then(() => {
                done();
            }).catch(err => {
                console.log(err);
            }/*next(err)*/);
        })();

    }
};
