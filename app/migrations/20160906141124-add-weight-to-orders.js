'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addWeightToOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'weight',
            {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function addRateToOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'rate',
            {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addWeightToOrders();
        yield addRateToOrders()
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeWeightColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'weight').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    function removeRateColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'weight').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeWeightColumn();
        yield removeRateColumn()
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
