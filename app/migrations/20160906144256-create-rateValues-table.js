'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initRateValues() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('rateValues',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              min_weight: {
                type: Sequelize.DECIMAL(10, 2)
              },
              max_weight: {
                type: Sequelize.DECIMAL(10, 2)
              },
              rate: {
                type: Sequelize.DECIMAL(10, 2)
              },
              service_type: {
                type: Sequelize.INTEGER
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initRateValues();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeRateValues() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('rateValues').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removeRateValues();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
