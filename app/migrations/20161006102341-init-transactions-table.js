'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initTransactions() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('transactions',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                  model: 'users',
                  key: 'id'
                },
                onUpdate: "NO ACTION",
                onDelete: "CASCADE"
              },
              invoice_no: {
                type: Sequelize.STRING
              },
              last4: {
                type: Sequelize.INTEGER
              },
              amount: {
                type: Sequelize.STRING
              },
              status: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0
              },
              created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
              },
              updated_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initTransactions();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeTransactions() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('transactions').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removeTransactions();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
