INSERT INTO packages (`name`, `cost`, `low_limit`, `offers_limit`, `type`) VALUES
('Starter', 3000, 110, 2, 1),
('Regular', 7000, 275, 5, 2),
('Power', 12000, 825, 15, 3),
('Curious', 100, 100, 2, 4)