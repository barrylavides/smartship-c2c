'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
      function initUsers(){
          return new Promise((resolve, reject) => {
              queryInterface.createTable('users',
                  {
                      id: {
                          type: Sequelize.INTEGER,
                          primaryKey: true,
                          autoIncrement: true
                      },
                      email: {
                          type: Sequelize.STRING,
                          allowNull: false
                      },
                      first_name: {
                          type: Sequelize.STRING,
                          allowNull: false
                      },
                      last_name: {
                          type: Sequelize.STRING,
                          allowNull: false
                      },
                      national_id: {
                          type: Sequelize.STRING,
                          allowNull: true
                      },
                      address: {
                          type: Sequelize.STRING,
                          allowNull: true
                      },
                      postcode_id: {
                          type: Sequelize.INTEGER,
                          allowNull: true,
                          references: {
                              model: 'postcodes',
                              key: 'id'
                          },
                          onUpdate: "NO ACTION",
                          onDelete: "CASCADE"
                      },
                      city_id: {
                          type: Sequelize.INTEGER,
                          allowNull: true,
                          references: {
                              model: 'cities',
                              key: 'id'
                          },
                          onUpdate: "NO ACTION",
                          onDelete: "CASCADE"
                      },
                      region_id: {
                          type: Sequelize.INTEGER,
                          allowNull: true,
                          references: {
                              model: 'regions',
                              key: 'id'
                          },
                          onUpdate: "NO ACTION",
                          onDelete: "CASCADE"
                      },
                      phone: {
                          type: Sequelize.STRING,
                          allowNull: true
                      },
                      vat: {
                          type: Sequelize.INTEGER,
                          allowNull: true
                      },
                      status: {
                          type: Sequelize.INTEGER,
                          allowNull: false,
                          defaultValue: 0
                      },
                      facebook_id: {
                          type: Sequelize.STRING,
                          allowNull: false,
                      },
                      created_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      },
                      updated_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      }
                  },
                  {
                      engine: 'InnoDB', // default: 'InnoDB'
                      charset: 'utf8' // default: null
                  }).then(function () {
                  //add index
                  queryInterface.addIndex(
                      'users',
                      ['email'],
                      {
                          indexName: 'idxUserEmail',
                          indicesType: 'UNIQUE'
                      }
                  );
                  resolve();
              });
          });
      };

      /**
       * init regions table
       *
       * @returns {Promise}
       */
      function initRegions(){
          return new Promise((resolve, reject) => {
              queryInterface.createTable('regions',
                  {
                      id: {
                          type: Sequelize.INTEGER,
                          primaryKey: true,
                          autoIncrement: true
                      },
                      name: {
                          type: Sequelize.STRING,
                          allowNull: false
                      },
                      country: {
                          type: Sequelize.STRING,
                          allowNull: true,
                          defaultValue: "TH"
                      },
                      created_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      },
                      updated_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      }
                  },
                  {
                      engine: 'InnoDB', // default: 'InnoDB'
                      charset: 'utf8' // default: null
                  }).then(function () {
                  resolve();
              });
          });
      }

      function initCities(){
          return new Promise((resolve, reject) => {
              queryInterface.createTable('cities',
                  {
                      id: {
                          type: Sequelize.INTEGER,
                          primaryKey: true,
                          autoIncrement: true
                      },
                      name: {
                          type: Sequelize.STRING,
                          allowNull: false
                      },
                      region_id: {
                          type: Sequelize.INTEGER,
                          allowNull: false,
                          references: {
                              model: 'regions',
                              key: 'id'
                          },
                          onUpdate: "NO ACTION",
                          onDelete: "CASCADE"
                      },
                      created_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      },
                      updated_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      }
                  },
                  {
                      engine: 'InnoDB', // default: 'InnoDB'
                      charset: 'utf8' // default: null
                  }).then(function () {
                  resolve();
              });
          });
      }

      function initPostcodes(){
          return new Promise((resolve, reject) => {
              queryInterface.createTable('postcodes',
                  {
                      id: {
                          type: Sequelize.INTEGER,
                          primaryKey: true,
                          autoIncrement: true
                      },
                      code: {
                          type: Sequelize.INTEGER,
                          allowNull: false
                      },
                      created_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      },
                      updated_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      }
                  },
                  {
                      engine: 'InnoDB', // default: 'InnoDB'
                      charset: 'utf8' // default: null
                  }).then(function () {
                  resolve();
              });
          });
      }

      function initPostcode2City(){
          return new Promise((resolve, reject) => {
              queryInterface.createTable('postcode2cities',
                  {
                      id: {
                          type: Sequelize.INTEGER,
                          primaryKey: true,
                          autoIncrement: true
                      },
                      postcode_id: {
                          type: Sequelize.INTEGER,
                          allowNull: false,
                          references: {
                              model: 'postcodes',
                              key: 'id'
                          },
                          onUpdate: "NO ACTION",
                          onDelete: "CASCADE"
                      },
                      city_id: {
                          type: Sequelize.INTEGER,
                          allowNull: false,
                          references: {
                              model: 'cities',
                              key: 'id'
                          },
                          onUpdate: "NO ACTION",
                          onDelete: "CASCADE"
                      },
                      created_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      },
                      updated_at: {
                          type: Sequelize.DATE,
                          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
                      }
                  },
                  {
                      engine: 'InnoDB', // default: 'InnoDB'
                      charset: 'utf8' // default: null
                  }).then(function () {
                  resolve();
              });
          });
      }

      (() => {
          co(function *() {
              yield initRegions();
              yield initCities();
              yield initPostcodes();
              yield initPostcode2City();
              yield initUsers();
          }).then(function(){
              done();
          }).catch(function (err) {
              console.log(err);
          });
      })();

  },

  down: function (queryInterface, Sequelize, done) {
      function removeUsers() {
          return new Promise((resolve, reject) => {
              queryInterface.dropTable('users').then(function () {
                  resolve();
              }).catch(function (err) {
                  reject(err);
              });
          })
      }
      function removeRegions() {
          return new Promise((resolve, reject) => {
              queryInterface.dropTable('regions').then(function () {
                  resolve();
              }).catch(function (err) {
                  reject(err);
              });
          })
      }
      function removeCities() {
          return new Promise((resolve, reject) => {
              queryInterface.dropTable('cities').then(function () {
                  resolve();
              }).catch(function (err) {
                  reject(err);
              });
          })
      }

      function removePostcodes() {
          return new Promise((resolve, reject) => {
              queryInterface.dropTable('postcodes').then(function () {
                  resolve();
              }).catch(function (err) {
                  reject(err);
              });
          })
      }

      function removePostcode2City() {
          return new Promise((resolve, reject) => {
              queryInterface.dropTable('postcode2cities').then(function () {
                  resolve();
              }).catch(function (err) {
                  reject(err);
              });
          })
      }

      (() => {
          co(function *() {
              yield removeUsers();
              yield removePostcode2City();
              yield removeCities();
              yield removeRegions();
              yield removePostcodes();
          }).then(() => {
              done();
          }).catch(err => {
              console.log(err);
          }/*next(err)*/);
      })();
  }
};
