'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {

    function changeCreatedAtTransactions() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('transactions', 'created_at',
            {
              type: Sequelize.INTEGER
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function changeUpdatedAtTransactions() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('transactions', 'updated_at',
            {
              type: Sequelize.INTEGER
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield changeCreatedAtTransactions();
        yield changeUpdatedAtTransactions()
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {

    function resetCreatedAtTransactions() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('transactions', 'created_at', {
              type: Sequelize.DATE,
              defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    function resetUpdatedAtTransactions() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('transactions', 'updated_at', {
              type: Sequelize.DATE,
              defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield resetCreatedAtTransactions();
        yield resetUpdatedAtTransactions()
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
