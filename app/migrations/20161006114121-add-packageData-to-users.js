'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addAmountForUsers() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('users', 'available_amount',
            {
              type: Sequelize.DECIMAL(10, 2),
              defaultValue: 0
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function addCountForUsers() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('users', 'available_count',
            {
              type: Sequelize.INTEGER,
              defaultValue: 0
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function addLowLimitForUsers() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('users', 'current_low_limit',
            {
              type: Sequelize.INTEGER,
              defaultValue: 0
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addAmountForUsers();
        yield addCountForUsers();
        yield addLowLimitForUsers();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeAmountColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('users', 'available_amount').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    function removeCountColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('users', 'available_count').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    function removeLowLimitColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('users', 'current_low_limit').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeAmountColumn();
        yield removeCountColumn();
        yield removeLowLimitColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
