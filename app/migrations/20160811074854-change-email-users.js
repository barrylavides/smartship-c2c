'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function changeEmailUsers() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('users', 'email',
            {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield changeEmailUsers();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function resetChangesEmail() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('users', 'email',
            {
              type: Sequelize.STRING,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield resetChangesEmail();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
