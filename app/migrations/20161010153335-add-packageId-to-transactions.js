'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addPackageIdForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('transactions', 'package_id',
            {
              type: Sequelize.INTEGER,
              allowNull: false,
              references: {
                model: 'packages',
                key: 'id'
              },
              onUpdate: "NO ACTION",
              onDelete: "CASCADE"
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addPackageIdForOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removePackageIdColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('transactions', 'package_id').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removePackageIdColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
