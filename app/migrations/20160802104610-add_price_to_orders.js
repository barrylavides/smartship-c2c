'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addPriceToOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'price',
            {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addPriceToOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removePriceColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'price').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removePriceColumn()
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};