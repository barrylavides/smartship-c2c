'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function removePriceOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'price').then(function () {
          resolve();
        });
      });
    };

    function removeSentFlagOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'is_sent').then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield removePriceOrders();
        yield removeSentFlagOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function addPriceToOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'price',
            {
              type: Sequelize.DECIMAL(10,2),
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addPriceToOrders();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
