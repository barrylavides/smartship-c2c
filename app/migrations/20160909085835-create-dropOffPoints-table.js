'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initDropOffPoints() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('dropOffPoints',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              post_office: {
                type: Sequelize.STRING
              },
              postcode: {
                type: Sequelize.INTEGER
              },
              phone: {
                type: Sequelize.STRING
              },
              latitude: {
                type: Sequelize.DECIMAL(10,8)
              },
              longitude: {
                type: Sequelize.DECIMAL(11, 8)
              },
              address: {
                type: Sequelize.STRING
              },
              service_type: {
                type: Sequelize.INTEGER
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initDropOffPoints();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeDropOffPoints() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('dropOffPoints').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removeDropOffPoints();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
