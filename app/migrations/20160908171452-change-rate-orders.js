'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {

    function changeRateToOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('orders', 'rate',
            {
              type: Sequelize.DECIMAL(10, 2),
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield changeRateToOrders()
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {

    function resetChangeRateToOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('orders', 'rate', {
              type: Sequelize.STRING,
              allowNull: true
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield resetChangeRateToOrders()
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
