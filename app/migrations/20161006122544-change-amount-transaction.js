'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {

    function changeAmountToTransaction() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('transactions', 'amount',
            {
              type: Sequelize.DECIMAL(10, 2)
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield changeAmountToTransaction()
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {

    function resetChangeAmountToTransaction() {
      return new Promise((resolve, reject) => {
        queryInterface.changeColumn('transactions', 'amount', {
              type: Sequelize.STRING
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield resetChangeAmountToTransaction()
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
