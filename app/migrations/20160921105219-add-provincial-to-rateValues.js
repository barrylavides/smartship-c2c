'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addProvincialForDropOffPoints() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('rateValues', 'provincial',
            {
              type: Sequelize.DECIMAL(10, 2)
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addProvincialForDropOffPoints();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeProvincialColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('rateValues', 'provincial').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeProvincialColumn();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
