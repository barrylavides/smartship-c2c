'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function addDeclareValForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'declare_value',
            {
              type: Sequelize.DECIMAL(10,2),
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    function addInsuranceForOrders() {
      return new Promise((resolve, reject) => {
        queryInterface.addColumn('orders', 'insurance',
            {
              type: Sequelize.INTEGER,
              allowNull: false
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield addDeclareValForOrders();
        yield addInsuranceForOrders();
      }).then(function () {
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeDeclareColumn() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'declare_value').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    function removeInsurance() {
      return new Promise((resolve, reject) => {
        queryInterface.removeColumn('orders', 'insurance').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }

    (() => {
      co(function *() {
        yield removeDeclareColumn();
        yield removeInsurance();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
