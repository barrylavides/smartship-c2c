'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initSocials() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('socials',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                  model: 'users',
                  key: 'id'
                },
                onUpdate: "NO ACTION",
                onDelete: "CASCADE"
              },
              socialId: {
                type: Sequelize.STRING,
                allowNull: false
              },
              socialType: {
                type: Sequelize.INTEGER,
                allowNull: false
              },
              socialEmail: {
                type: Sequelize.STRING,
                allowNull: false
              },
              created_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
              },
              updated_at: {
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initSocials();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeSocials() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('socials').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removeSocials();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
