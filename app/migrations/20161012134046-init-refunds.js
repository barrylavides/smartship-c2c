'use strict';
const co = require("co");

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    function initRefunds() {
      return new Promise((resolve, reject) => {
        queryInterface.createTable('refunds',
            {
              id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
              },
              reference_no: {
                type: Sequelize.STRING,
                allowNull: false
              },
              user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                  model: 'users',
                  key: 'id'
                },
                onUpdate: "NO ACTION",
                onDelete: "CASCADE"
              },
              amount: {
                type: Sequelize.DECIMAL(10, 2)
              },
              status: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 1
              },
              created_at: {
                type: Sequelize.INTEGER
              },
              updated_at: {
                type: Sequelize.INTEGER
              }
            },
            {
              engine: 'InnoDB', // default: 'InnoDB'
              charset: 'utf8' // default: null
            }).then(function () {
          resolve();
        });
      });
    };

    (() => {
      co(function *() {
        yield initRefunds();
      }).then(function(){
        done();
      }).catch(function (err) {
        console.log(err);
      });
    })();
  },

  down: function (queryInterface, Sequelize, done) {
    function removeRefunds() {
      return new Promise((resolve, reject) => {
        queryInterface.dropTable('refunds').then(function () {
          resolve();
        }).catch(function (err) {
          reject(err);
        });
      })
    }
    (() => {
      co(function *() {
        yield removeRefunds();
      }).then(() => {
        done();
      }).catch(err => {
        console.log(err);
      }/*next(err)*/);
    })();

  }
};
