'use strict';

module.exports = {
  up: function (queryInterface, Sequelize, done) {
    queryInterface.sequelize.query(`DELETE FROM rateValues;`)
        .then(() => {
          queryInterface.sequelize.query(`
          INSERT INTO rateValues(min_weight, max_weight, rate, service_type, provincial) VALUES
            (0, 0.25, 35, 1, null),
            (0.25, 0.5, 38.5, 1, null),
            (0.5, 1, 57, 1, null),
            (1, 2, 72, 1, null),
            (2, 5, 91.5, 1, null),
            (5, 10, 115.5, 1, null),
            (10, 15, 185, 1, null),
            (15, null, 230, 1, null),
            (0, 0.25, 43, 4, 54),
            (0.25, 0.5, 43, 4, 54),
            (0.5, 1, 43, 4, 54),
            (1, 2, 54, 4, 64),
            (2, 5, 75, 4, 96),
            (5, 10, 103, 4, 124),
            (10, 15, 124, 4, 176),
            (15, null, 176, 4, 218),
            (0, 0.25, 35.5, 5, 53.5),
            (0.25, 0.5, 40, 5, 55),
            (0.5, 1, 45.5, 5, 65.5),
            (1, 2, 65, 5, 80),
            (2, 5, 77.5, 5, 103.5),
            (5, 10, 95, 5, 120),
            (10, 15, 155, 5, 190),
            (15, null, 200, 5, 230)`);
        }).then(() => {
          done();
    });
  },

  down: function (queryInterface, Sequelize) {}
};
