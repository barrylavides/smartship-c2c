var bodyParser = require('body-parser');
var controllers = require('./controllers');
var errorHandler = require('./utils/errorHandler');
var express = require('express');
var router = require('./routes');
var loggerNode = require('./utils/autoLogger');

//initialize the app
var app = module.exports = express();


// app.use(express.static(__dirname + '/../adminpanel'));
app.use(express.static(__dirname + '/../frontend/app'));
app.use(express.static(__dirname + '/../frontend'));
app.use(express.static(__dirname + '/../frontend/swagger'));

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.raw({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));

app.use(loggerNode.requestHandler);

app.use('/password/confirm', controllers.static.changePasswordPage);
app.use('/password/success', controllers.static.changePasswordSuccess);

app.use('/api', router.default);
app.get('/adminpanel/*', controllers.static.adminPanel);
app.get('/print', controllers.static.adminPanel);
app.get('/*', controllers.static.frontendApp);

app.use(loggerNode.errorHandler);


//set up http error handler
app.use(errorHandler(app));