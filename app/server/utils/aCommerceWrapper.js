'use strict';
const request = require('request');
const constants = require('../constants.json');
const config = require('../../config');
const aCommerceHelper = require('./aCommerceHelper');
const aCommerceChecker = require('./aCommerceChecker');

class aCommerce {

    static ordersAPIServices (api, method, data, orderId, token, cb) {
        if (!cb) {
            return;
        }

        if (!token) {
            token = aCommerceChecker.token
        }

        let requestData = {};

        requestData.url = config.aCommerce.credentials.baseUrl + '/partner/' +
            config.aCommerce.credentials.partnerId + api + orderId;

        console.log(requestData);
        console.log('Token', token);

        requestData.headers = {
            'Content-Type': 'application/json',
            'X-Subject-Token': token.token.token_id
        };

        if (method === 'put' && data) {
            requestData.json = data
        }

        request[method](requestData, function (error, response, body) {
            if (response.statusCode == 401) {
                aCommerce.ordersAPIServices(api, method, data, orderId, aCommerceChecker.updateToken(), cb);
                return;
            }

            if (typeof body === 'string') {
                body = JSON.parse(body)
            }

            console.log('BODY', body);
            console.log('TYPE', typeof body);
            cb(body);
        })
    }

}

module.exports = aCommerce;