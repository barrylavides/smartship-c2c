var recaptcha = require('express-recaptcha');
var logger = require('./logger');
var config = require('../../config/env.dist');

recaptcha.init(config.SITE_KEY, config.SECRET_KEY);

function verify(req, res, next) {
    recaptcha.verify(req, function (error) {
        logger.info('recaptcha verify', req.body);
        logger.info('recaptcha verify arguments', arguments);
        if (error) {
            logger.error('recaptcha verify', error);
            return next(error);
        }

        next();
    });
}

module.exports = verify;