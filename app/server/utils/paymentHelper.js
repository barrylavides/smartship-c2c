'use strict';

const request = require('sync-request');
const constants = require('../constants.json');
const config = require('../../config');
const crypto = require('crypto');
const fs = require('fs');
const forge = require('node-forge');
const parseString = require('xml2js').parseString;

class PaymentHelper {
    static generatePaymentRequest (body, packageData, user, transaction) {

        let merchantID = config.PAYMENT_2C2P_DATA.MERCHANT_ID;
        let secretKey = config.PAYMENT_2C2P_DATA.SECRET_KEY;

        let storeCard = "N";

        if (+body.storeCard === 1) {
            storeCard = "Y";
        }

        let uniqueTransactionCode = "Invoice" + Math.floor(Date.now() / 1000);
        let encData = body.encryptedCardInfo;
        let cardholderName = body.cardholderName;
        let cardholderEmail = user.email;

        let description = packageData.name + " package";
        let amount = PaymentHelper.priceFormatTo12Digit(packageData.cost);

        //TODO: For now, only for Thailand

        let currencyCode = "764";
        let country = "TH";
        let panBank = body.panBank;

        let apiVersion = config.PAYMENT_2C2P_DATA.API_VERSION;
        let timeStamp = Math.floor(Date.now() / 1000);

        let packageInfo = {};
        packageInfo.cost = packageData.cost;
        packageInfo.lowLimit = packageData.lowLimit;
        packageInfo.offersLimit = packageData.offersLimit;
        packageInfo.type = packageData.type;

        let userDefined3 = JSON.stringify(packageInfo);

        let stringToHash = apiVersion + timeStamp + merchantID + uniqueTransactionCode + description + amount
            + currencyCode + panBank + country + cardholderName + cardholderEmail + body.frontState + transaction.id
            + userDefined3 + storeCard + encData;

        let cipher = crypto.createHmac('sha1', secretKey);

        let hash = cipher
            .update(stringToHash)
            .digest('hex');

        hash = hash.toUpperCase();

        let xml = "<PaymentRequest>" +
            "<version>"+apiVersion+"</version>" +
            "<timeStamp>"+timeStamp+"</timeStamp>" +
            "<merchantID>"+merchantID+"</merchantID>" +
            "<uniqueTransactionCode>"+uniqueTransactionCode+"</uniqueTransactionCode>" +
            "<desc>"+description+"</desc>" +
            "<amt>"+amount+"</amt>" +
            "<currencyCode>"+currencyCode+"</currencyCode>" +
            "<userDefined1>"+body.frontState+"</userDefined1>" +
            "<userDefined2>"+transaction.id+"</userDefined2>" +
            "<userDefined3>"+userDefined3+"</userDefined3>" +
            "<storeCard>"+storeCard+"</storeCard>" +
            "<panBank>"+panBank+"</panBank>" +
            "<panCountry>"+country+"</panCountry>" +
            "<cardholderName>"+cardholderName+"</cardholderName>" +
            "<cardholderEmail>"+cardholderEmail+"</cardholderEmail>" +
            "<secureHash>"+hash+"</secureHash>" +
            "<encCardData>"+encData+"</encCardData>" +
            "</PaymentRequest>";

        let payload = new Buffer(xml).toString('base64');

        return payload;

    }

    static decryptPaymentResponse (text, cb) {

        if (!cb) {
            return;
        }

        let arrData = text.match(/.{1,64}/g);
        text = '';

        arrData.forEach(function (val) {
            text += val + "\r\n";
        });

        text = '-----BEGIN PKCS7-----\r\n' + text + '-----END PKCS7-----\r\n';

        let privateKey = fs.readFileSync(__dirname + '/../../cert/private.pem');

        let p7 = forge.pkcs7.messageFromPem(text);
        let pk = forge.pki.decryptRsaPrivateKey(privateKey, config.PAYMENT_2C2P_DATA.PASSWORD_FOR_KEY);
        p7.decrypt(p7.recipients[0], pk);

        parseString(p7.content, function (err, result) {
            if (err) {
                return cb(err, null)
            }
            cb(null, result)
        });
    }

    static priceFormatTo12Digit (price) {

        price = price * 100;

        let priceDigitFormat = "";
        price = "" + price;

        let priceLength = price.length;
        let countFormat = 12 - priceLength;

        for (let i = 0; i < countFormat; i++) {
            priceDigitFormat += "0"
        }

        price = priceDigitFormat + price;

        return price;
    }

}

module.exports = PaymentHelper;