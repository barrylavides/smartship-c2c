'use strict';

const moment = require('moment');

class FiltersHelper {
    static ordersFilters (query, searchConditions) {

        if (query.status) {
            searchConditions.status = query.status;

            if (query.status == 1) {
                searchConditions.status = {$in: [0, 1]}
            }
        }

        if (query.trackingId) {
            searchConditions.trackingId = {$like: '%' + query.trackingId + '%'}
        }


        if (query.createdAt) {
            searchConditions.createdAt = {
                $gt: moment.unix(+(query.createdAt)).format('YYYY-MM-DD'),
                $lt: moment.unix(+(query.createdAt) + 24*60*60).format('YYYY-MM-DD')
            }
        }

        if (query.updatedAt) {
            searchConditions.updatedAt = {
                $gt: moment.unix(+(query.updatedAt)).format('YYYY-MM-DD'),
                $lt: moment.unix(+(query.updatedAt) + 24*60*60).format('YYYY-MM-DD')
            }
        }

        if (query.receiverName) {
            searchConditions.receiverName = {$like: query.receiverName + '%'}
        }

        if (query.receiverEmail) {
            searchConditions.receiverEmail = {$like: query.receiverEmail + '%'}
        }

        return searchConditions;
    }

    static transactionsFilters (query, searchConditions) {
        if (query.invoiceNo) {
            searchConditions.invoiceNo = {$like: '%' + query.invoiceNo + '%'}
        }

        if (!query.fromDate && !query.toDate) {
            return searchConditions;
        }

        let dateFilter = {};

        if (query.fromDate) {
            dateFilter.$gte = query.fromDate
        }

        if (query.toDate) {
            dateFilter.$lte = query.toDate
        }


        searchConditions.createdAt = dateFilter;

        return searchConditions;
    }

    static chargesFilters (query, searchConditions) {
        if (query.trackingId) {
            searchConditions.trackingId = {$like: '%' + query.trackingId + '%'}
        }

        if (!query.fromDate && !query.toDate) {
            return searchConditions;
        }

        let dateFilter = {};

        if (query.fromDate) {
            dateFilter.$gte = moment.unix(+(query.fromDate)).format('YYYY-MM-DD')
        }

        if (query.toDate) {
            dateFilter.$lte = moment.unix(+(query.toDate)).format('YYYY-MM-DD')
        }

        searchConditions.createdAt = dateFilter;

        return searchConditions;
    }

    static refundsFilters (query, searchConditions) {
        if (query.referenceNo) {
            searchConditions.referenceNo = {$like: '%' + query.referenceNo + '%'}
        }

        return searchConditions;
    }
}

module.exports = FiltersHelper;