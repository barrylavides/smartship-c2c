'use strict';

const constants = require('./../constants.json');
const config = require('./../../config');

class Format {

    /**
     * format user data
     *
     * @param data {object}
     * @returns {object}
     */
    static user(data) {
        let model = {
            id: data.id,
            email: data.email,
            facebookId: data.facebookId,
            firstName: data.firstName,
            lastName: data.lastName,
            status: data.status,
            createdAt: data.createdAt,
            updatedAt: data.updatedAt
        };

        return model;
    };

    /**
     * format user from facebook
     * @param data
     * @returns {{email: (*|users.email|{type, allowNull}|RegExp|{type, validate}|boolean)}}
     */
    static registerUserFromFacebook(data){
        let userModel = {
            firstName: data.first_name,
            lastName: data.last_name,
            status: 0,
            role: 1
        };

        let socialModel = {
            socialId: data.id,
            socialType: 1
        };

        if (data.email) {
            userModel.email = data.email;
            socialModel.socialEmail = data.email
        }

        return {
            user: userModel,
            social: socialModel
        };
    };

    static authAdminFormat(data) {
        let model = {
            id: data.id,
            email: data.email,
            role: data.role
        }

        return model;
    }

    static dataForSynchronize (user) {
        let userModel = {
            firstName: user.user.firstName,
            lastName: user.user.lastName,
            id: user.user.id,
            address: user.user.address,
            phone: user.user.phone,
            nationalId: user.user.nationalId,
            postcodeId: user.user.postcodeId,
            facebookId: user.socialId,
            language: user.user.language,
            vat: user.user.vat,
            status: user.user.status,
            cityId: user.user.cityId,
            role: user.user.role,
            havePackage: user.user.havePackage,
            authType: 2,
            socialType: user.socialType,
            email: user.socialEmail,
            createdAt: user.user.createdAt,
            updatedAt: user.user.updatedAt
        };

        if (user.user.email) {
            userModel.email = user.user.email
        }

        if (user.user.password) {
            userModel.password = true;
        }

        return userModel;
    }

    static formatNewUser (userCreate, socialUserCreate) {
        let user = {
            firstName: userCreate.firstName,
            lastName: userCreate.lastName,
            id: userCreate.id,
            authType: 2,
            facebookId: socialUserCreate.socialId,
            status: userCreate.status,
            role: userCreate.role,
            socialType: socialUserCreate.socialType,
            availableAmount: userCreate.availableAmount,
            availableCount: userCreate.availableCount,
            currentLowLimit: userCreate.currentLowLimit,
            packageType: userCreate.packageType,
            havePackage: userCreate.havePackage,
            createdAt: userCreate.createdAt,
            updatedAt: userCreate.updatedAt
        };

        if (socialUserCreate.socialEmail) {
            user.email = socialUserCreate.socialEmail
        }

        return user;
    }

    static mainAuthUser (user) {
        let result = {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            vat: user.vat,
            socials: user.socials,
            password: true,
            phone: user.phone,
            cityId: user.cityId,
            address: user.address,
            status: user.status,
            nationalId: user.nationalId,
            postcodeId: user.postcodeId,
            language: user.language,
            authType: 1,
            availableAmount: user.availableAmount,
            availableCount: user.availableCount,
            currentLowLimit: user.currentLowLimit,
            packageType: user.packageType,
            havePackage: user.havePackage,
            role: user.role
        };

        return result;
    }

    static createUser (body) {
        let createBody = {
            firstName: body.firstName,
            lastName: body.lastName,
            email: body.email,
            password: body.password,
            role: 1
        };

        return createBody;
    }

    static resAfterRegister (userCreate) {
        let result = {
            id: userCreate.id,
            firstName: userCreate.firstName,
            lastName: userCreate.lastName,
            email: userCreate.email,
            status: userCreate.status,
            availableAmount: userCreate.availableAmount,
            availableCount: userCreate.availableCount,
            currentLowLimit: userCreate.currentLowLimit,
            packageType: userCreate.packageType,
            havePackage: userCreate.havePackage,
            password: true,
            authType: 1,
            role: userCreate.role
        };

        return result;
    }

    static validateVatForRegister (body) {
        if (body.vat) {
            let num = Number(body.vat);
            if (!num) {
                return {
                    err: true,
                    message: 'VAT must be an integer type!',
                    status: 400
                }
            }
        }

        return body;
    }

    static patchUser (body, existUser) {

        let updateBody = {
            message: ''
        };

        if (body.password) {
            let salt = existUser.generateSalt();

            let hashPassword = existUser.encryptPass(body.password, salt);

            updateBody = {
                message: 'Password has been added!',
                password: hashPassword,
                salt: salt
            };
        }

        if (body.email) {
            updateBody = {
                message: 'Email has been edited!',
                email: body.email
            }
        }

        return updateBody;
    }

    static resForPatch (updatedUser, existUser, updateBody) {

        let result = {
            firstName: updatedUser.firstName,
            lastName: updatedUser.lastName,
            availableAmount: updatedUser.availableAmount,
            availableCount: updatedUser.availableCount,
            currentLowLimit: updatedUser.currentLowLimit,
            packageType: updatedUser.packageType,
            havePackage: updatedUser.havePackage,
            email: updatedUser.email,
            role: updatedUser.role,
            vat: updatedUser.vat,
            phone: updatedUser.phone,
            nationalId: updatedUser.nationalId,
            address: updatedUser.address,
            cityId: updatedUser.cityId,
            postcodeId: updatedUser.postcodeId,
            socials: existUser.socials,
            password: true,
            message: updateBody.message
        };

        return result;
    }

    static userSaveOptions (userObj, body) {
        let user = userObj;
        user.status = 1;

        for(var key in body){
            user[key] = body[key];
        }

        delete user.dataValues.salt;

        return user;
    }

    static registerFormatter (userObj) {

        let user = {};

        if (userObj.password === true) {
            user.password = userObj.password;
        }

        user.id = userObj.id;
        user.socials = userObj.socials;
        user.email = userObj.email;
        user.firstName = userObj.firstName;
        user.lastName = userObj.lastName;
        user.nationalId = userObj.nationalId;
        user.address = userObj.address;
        user.regionId = userObj.regionId;
        user.role = userObj.role;
        user.cityId = userObj.cityId;
        user.language = userObj.language;
        user.postcodeId = userObj.postcodeId;
        user.phone = userObj.phone;
        user.vat = userObj.vat;
        user.status = userObj.status;
        user.createdAt = userObj.createdAt;
        user.updatedAt = userObj.updatedAt;
        user.socials = userObj.socials;

        return user;
    }

    static sendUser (user, confirm) {
        confirm.from = constants.ELASTIC_EMAIL.FROM_VAL;
        confirm.sender = constants.ELASTIC_EMAIL.SENDER_VAL;
        confirm.recipient = user.email;

        confirm.replaces = {
            link: config.server.baseUrl + '/home'
        };

        return confirm;
    }

}

module.exports = Format;