'use strict';


class Num2Words {

    static changeNum2WordsString (str) {

        var pointExist;
        console.log(str);
        if (~str.indexOf('point')) {
            pointExist = true;
            var str1 = str.slice(0, str.indexOf('point'));
            var str2 = str.slice(str.indexOf('point') + 5);
            str1 = str1 + ' Baht and';
            str = str1 + str2 + ' Satang';
        }

        if (~str.indexOf('hundred')) {
            var str7 = str.slice(0, str.indexOf('hundred') + 7);
            var str8 = str.slice(str.indexOf('hundred') + 7);
            str = str7 + ' and ' + str8
        }

        if (~str.indexOf('zero')) {
            var str3 = str.slice(0, str.indexOf('zero'));
            var str4 = str.slice(str.indexOf('zero') + 4);

            str = str3 + str4;
        }

        // if (~str.indexOf('thousand')) {
        //     var str5 = str.slice(0, str.indexOf('thousand') + 8);
        //     var str6 = str.slice(str.indexOf('thousand') + 8);
        //
        //     str = str5 + ' and' + str6;
        // }
        
        if (~str.indexOf('Satang')) {
            let flag;
            let arr = str.split(' ');

            if (arr[arr.indexOf('Baht') + 2] === 'one') {
                flag = 1;
            }

            arr[arr.indexOf('Baht') + 2] = Num2Words.Num2WordsReplace(arr[arr.indexOf('Baht') + 2], arr[arr.indexOf('Baht') + 3]);

            if (flag) {
                arr[arr.indexOf('Baht') + 3] = '';
            }

            str = arr.join(' ');
        }

        if (!pointExist) {
            str = str + ' Baht';
        }

        return str;
    }

    static Num2WordsReplace (str, nextElem) {

        if (str === 'one') {
            str = '';

            if (nextElem === 'zero') {
                str = 'ten'
            }

            if (nextElem === 'one') {
                str = 'eleven'
            }

            if (nextElem === 'two') {
                str = 'twelve'
            }

            if (nextElem === 'three') {
                str = 'thirteen'
            }

            if (nextElem === 'four') {
                str = 'fourteen'
            }

            if (nextElem === 'five') {
                str = 'fifteen'
            }

            if (nextElem === 'six') {
                str = 'sixteen'
            }

            if (nextElem === 'seven') {
                str = 'seventeen'
            }

            if (nextElem === 'eight') {
                str = 'eighteen'
            }

            if (nextElem === 'nine') {
                str = 'nineteen'
            }

            if (!nextElem || nextElem === 'Satang') {
                str = 'ten Satang'
            }

        }

        if (str === 'two') {
            str = 'twenty'
        }

        if (str === 'three') {
            str = 'thirty'
        }

        if (str === 'four') {
            str = 'forty'
        }

        if (str === 'five') {
            str = 'fifty'
        }

        if (str === 'six') {
            str = 'sixty'
        }

        if (str === 'seven') {
            str = 'seventy'
        }

        if (str === 'eight') {
            str = 'eighty'
        }

        if (str === 'nine') {
            str = 'ninety'
        }

        return str;
    }

    // static changeNum2WordsString (str) {
    //     if (~str.indexOf('point')) {
    //         var str1 = str.slice(0, str.indexOf('point'));
    //         var str2 = str.slice(str.indexOf('point') + 5);
    //         str1 = str1 + ' Baht and';
    //         str = str1 + str2;
    //     }
    //
    //     if (~str.indexOf('zero')) {
    //         var str3 = str.slice(0, str.indexOf('zero'));
    //         var str4 = str.slice(str.indexOf('zero') + 4);
    //
    //         str = str3 + str4;
    //     }
    //
    //     str = str + ' Satang';
    //
    //     return str;
    // }
}

module.exports = Num2Words.changeNum2WordsString;

