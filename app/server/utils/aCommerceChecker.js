'use strict';

const request = require('sync-request');
const constants = require('../constants.json');
const config = require('../../config');

class aCommerceChecker {
    constructor() {


        this.token = this.updateToken();
    }

    updateToken () {

        let res = request('POST', config.aCommerce.credentials.baseUrl + '/identity/token', {
            json: {
                "auth": {
                    "apiKeyCredentials": {
                        "username": config.aCommerce.credentials.username,
                        "apiKey": config.aCommerce.credentials.apikey
                    }
                }
            }
        });

        let result = JSON.parse(res.getBody('utf-8'));

        this.token = result;
        return result;
    }
}

let aCommerce = new aCommerceChecker();

module.exports = aCommerce;