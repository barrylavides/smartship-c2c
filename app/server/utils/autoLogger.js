'use strict';

const db = require('../models/v1').sequelize;
let logger = require('node-logger-web');

let date = new Date();
date.setMinutes((date.getMinutes()) + 1);

let dbConfig = {
    host: 'localhost',
    user: "root",
    password: "root",
    name: "test_db"
};

let storage = new logger.SequelizeMySqlStorage({
    db: db
    //export: {
    //    path: __dirname + '/../../logs/export/',
    //    time: '00 * * * * 1-5'
    //}
});


let Logger = logger({
    fileName: __dirname + '/../../logs/checkLogs',
    storage: storage,
    isActive: true
});

module.exports = Logger;