'use strict';

const constants = require('../constants.json');

class StatusRepresentation {
    static orderStatus (status) {
        return constants.ORDER_STATUS[status]
    }

    static reverseOrderStatus (status, service) {

        if (status === 0 || status === 1) {
            status = 'Pending'
        } else {
            for (var key in constants.SHIPPING_STATUS) {
                if (constants.SHIPPING_STATUS[key] === status) {
                    status = key
                }
            }
        }


        for (var key in constants.DROP_OFF) {
            if (constants.DROP_OFF[key] === service) {
                service = key;
            }
        }
        return {
            status: status,
            service: service
        }
    }
}

module.exports = StatusRepresentation;