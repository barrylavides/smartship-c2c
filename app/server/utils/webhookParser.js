'use strict';

const statusRepresentation = require('./orderStatusRepresenter');
const moment = require('moment');

class WebhookParser {
    static acommerceWebhook (body) {
        let orderUpdateBody = {};
        let orderStatus;

        if (body.shipOrderStatus) {
            orderUpdateBody.updatedAt = moment(body.shipOrderStatus[0].updatedTime).format('YYYY-MM-DD hh:mm:ss');
        }

        if (body.shipPackage && body.shipPackage.length !== 0 && !body.shipOrderDetailedStatus) {
            let shippingBody = body.shipPackage[body.shipPackage.length - 1].statusHistory[0];

            orderStatus = statusRepresentation.orderStatus(shippingBody.shippingStatus);

            orderUpdateBody.updatedAt = moment(shippingBody.updatedTime).format('YYYY-MM-DD hh:mm:ss');

            if (body.shipPackage[body.shipPackage.length - 1].trackingId) {
                orderUpdateBody.trackingId = body.shipPackage[body.shipPackage.length - 1].trackingId;
            }
        }

        if (body.shipOrderDetailedStatus) {
            let shipOrderDetail = body.shipOrderDetailedStatus[0];

            orderUpdateBody.updatedAt = moment(shipOrderDetail.updatedTime).format('YYYY-MM-DD hh:mm:ss');

            orderStatus = statusRepresentation.orderStatus(shipOrderDetail.shipOrderDetailedStatus);
        }

        orderUpdateBody.status = orderStatus;

        return orderUpdateBody;
    }
}

module.exports = WebhookParser;