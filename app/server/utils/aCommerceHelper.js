'use strict';

const request = require('request');
const constants = require('../constants.json');
const config = require('../../config');

class aCommerceHelper {
    static generateSaveOrderBody (data) {

        data.body.service = aCommerceHelper.aCommerceDataFormat(data.body.service, constants.aCommerce.SERVICES);

        data.body.type = aCommerceHelper.aCommerceDataFormat(data.body.type, constants.aCommerce.TYPES);

        data.body.receiver.phone = "" + data.body.receiver.phone;

        let email;

        if (!data.user.email) {
            email = data.user.socials[0].socialEmail
        } else {
            email = data.user.email
        }

        let shippingData = {
            "shipCreatedTime": new Date().toISOString(),
            "shipSender": {
                "addressee": data.user.firstName + ' ' + data.user.lastName,
                "address1": data.user.address,
                "district": data.city.name,
                "city": "",
                "country": "Thailand",
                "province": data.city.region.name,
                "postalCode": data.postcode.code,
                "email": email,
                "phone": data.user.phone
            },
            "shipShipment": {
                "addressee": data.body.receiver.addressee,
                "address1": data.body.receiver.address,
                "address2": data.body.receiver.address2,
                "district": data.body.receiver.district.name,
                "city": "",
                "province": data.body.receiver.province,
                "postalCode": data.body.receiver.postcode,
                "phone": data.body.receiver.phone,
                "country": "Thailand",
                "email": data.body.receiver.email
            },
            "shipShippingType": data.body.type,
            "shipPaymentType": "NON_COD",
            "shipServiceType": data.body.service,
            "shipCurrency": "THB",
            "shipPackages":[
                {}
            ]
        };

        if (data.body.insurance) {
            shippingData.shipInsurance = data.body.insurance;
            shippingData.shipTotalDeclareValue = data.body.declareValue;
        }

        if (data.body.service === 'DROP_SHIP' || data.body.service === 'DELIVERY') {

            data.body.pickup.phone = "" + data.body.pickup.phone;

            shippingData.shipPickup = {
                "addressee": data.body.pickup.addressee,
                "address1": data.body.pickup.address,
                "address2": data.body.pickup.address2,
                "district": data.body.pickup.district,
                "city": "",
                "province": data.body.pickup.province,
                "postalCode": data.body.pickup.postcode,
                "phone": data.body.pickup.phone,
                "country": "Thailand",
                "email": data.body.pickup.email
            };
        }


        if (data.body.gross) {
            shippingData.shipGrossTotal = data.body.gross;
        }

        console.log('RESULT FROM SERVER!!!', shippingData);

        return shippingData;
    }

    static aCommerceDataFormat (field, constants) {
        for (var key in constants) {
            if (field == constants[key]) {
                field = key
            }
        }

        return field;
    }

    static authenticateService (cb) {
        let authObject;

        request.post({
            url: config.aCommerce.credentials.baseUrl + '/identity/token',
            json: {
                "auth": {
                    "apiKeyCredentials": {
                        "username": config.aCommerce.credentials.username,
                        "apiKey": config.aCommerce.credentials.apikey
                    }
                }
            }
        }, function (error, response, body) {
            if (body) {
                authObject = body;
                cb(authObject);
            }
        })
    }


}


module.exports = aCommerceHelper;