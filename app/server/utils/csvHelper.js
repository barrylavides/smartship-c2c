'use strict';

const Representation = require('./orderStatusRepresenter');
const constants = require('./../constants.json');

class csvHelper {
    static resultFormatter (orderResult) {

        let fields = constants.CSV_FIELDS;
        let mainResult = [];
        let orderObject = {};

        for (var i = 0; i < orderResult.length; i++) {
            let orderRepresentation = Representation.reverseOrderStatus(orderResult[i].status,
                orderResult[i].serviceType);

            orderObject = {
                "Order Name": orderResult[i].orderName,
                "Tracking Number": orderResult[i].trackingId ? orderResult[i].trackingId : "Not available",
                "Address": orderResult[i].address,
                "District": orderResult[i].district,
                "Postcode": orderResult[i].postcode,
                "Province": orderResult[i].province,
                "Drop Off Partner": orderRepresentation.service,
                "Order Status": orderRepresentation.status,
                "Weight": orderResult[i].weight ? orderResult[i].weight : "Not available"
            };

            mainResult.push(orderObject)
        }

        return {
            fields: fields,
            result: mainResult
        }
    }
}

module.exports = csvHelper;