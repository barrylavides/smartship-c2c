'use strict';

const constants = require('./../constants.json');


class BulkOrderHelper {
    static formatterBulkUpload (data, user, count) {

        let errorsCollection = [];

        let dataFields = Object.keys(data);

        let key;
        let n = dataFields.length;
        let newDataObj = {};
        while (n--) {
            key = dataFields[n];
            newDataObj[key.toLowerCase().replace(/(^\s+|\s+$)/g,'')] = data[key];
        }

        for (let i = 0; i < dataFields.length; i++) {
            dataFields[i] = dataFields[i].toLowerCase();
            dataFields[i] = dataFields[i].replace(/(^\s+|\s+$)/g,'');
        }

        let orderObj = {};

        for (var i = 0; i < constants.ORDER_BULK_FIELDS.length; i++) {
            if (dataFields.indexOf(constants.ORDER_BULK_FIELDS[i]) === -1) {
                errorsCollection[0] = 'Fields is invalid';
            }
        }

        newDataObj.count = count;

        for (let key in newDataObj) {
            if (constants.ORDER_BULK_FIELDS.indexOf(key) !== -1 && !newDataObj[key]) {
                errorsCollection.push(key.charAt(0).toUpperCase() + key.substr(1).toLowerCase() + ' field is required. Line: ' + newDataObj.count)
            }
        }

        if (newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE] &&
            constants.aCommerce.SERVICES_VARIATION.THAIPOST.indexOf(newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE].toLowerCase()) !== -1) {
            newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE] = constants.aCommerce.SERVICES.SMARTSHIP_THAIPOST
        }
        else if (newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE] &&
            constants.aCommerce.SERVICES_VARIATION.KERRY.indexOf(newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE].toLowerCase()) !== -1) {
            newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE] = constants.aCommerce.SERVICES.SMARTSHIP_KERRY;
        }
        else if (newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE] &&
            constants.aCommerce.SERVICES_VARIATION.DROP_OFF.indexOf(newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE].toLowerCase()) !== -1) {
            newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE] = constants.aCommerce.SERVICES.SMARTSHIP_DROPOFF;
        }
        else {
            errorsCollection.push('Service type is invalid. Line: ' + newDataObj.count)
        }

        if (newDataObj[constants.aCommerce.CSV_FIELDS.SHIPPING_TYPE] &&
            constants.aCommerce.TYPES_VARIATION.EXPRESS.indexOf(newDataObj[constants.aCommerce.CSV_FIELDS.SHIPPING_TYPE].toLowerCase()) !== -1) {
            newDataObj[constants.aCommerce.CSV_FIELDS.SHIPPING_TYPE] = constants.aCommerce.TYPES.EXPRESS_1_2_DAYS;
        } else {
            errorsCollection.push('Shipping type is invalid. Line: ' + newDataObj.count)
        }

        if (newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER] &&
            newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER].length > 11) {
            errorsCollection.push('Phone is too long. Line: ' + newDataObj.count)
        }

        if (newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER] &&
            newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER][0] !== '6'
            && newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER][1] !== '6') {
            errorsCollection.push('Phone format is invalid. Correct example: 66123123123. Line: ' + newDataObj.count)
        }

        if (newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER] &&
            !/\d{9}/.test(newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER])) {
            errorsCollection.push('Phone format is invalid. Line: ' + newDataObj.count);
        }

        console.log(newDataObj);
        if (newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_EMAIL_ADDRESS] &&
            !/^[_a-z0-9.!#$%&'*+/=?^_`{|}~-]+(\.[_a-z0-9.!#$%&'*+/=?^_`{|}~-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/
                .test(newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_EMAIL_ADDRESS])) {
            errorsCollection.push('Email is invalid. Line: ' + newDataObj.count)
        }

        if (newDataObj.hasOwnProperty(constants.aCommerce.CSV_FIELDS.DECLARE_VALUE) && newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE] !== '') {
            newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE] = newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE].replace(/,/g, '.');
        }

        if (newDataObj.hasOwnProperty(constants.aCommerce.CSV_FIELDS.DECLARE_VALUE) && +newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE] <= 0
            && newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE] !== '') {
            errorsCollection.push('Declared value can not be negative or zero. Line: ' + newDataObj.count)
        }
        else if (newDataObj.hasOwnProperty(constants.aCommerce.CSV_FIELDS.DECLARE_VALUE) && newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE] !== '' &&
            !/^(?!\.?$)\d{0,6}(\.\d{0,2})?$/.test(+newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE])) {
            errorsCollection.push('Declared value format is invalid. Line: ' + newDataObj.count)
        }

        if (newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT]) {
            newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT] = newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT].split(' ');

            for (let i = 0; i < newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT].length; i ++) {
                newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT][i] = newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT][i].toLowerCase();
                newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT][i] = newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT][i][0].toUpperCase()
                    + newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT][i].substr(1);
            }

            newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT] = newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT].join(' ')
        }


        orderObj.userId = user.id;
        orderObj.orderName = newDataObj[constants.aCommerce.CSV_FIELDS.ITEM_NAME];
        orderObj.receiverName = newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_NAME];
        orderObj.receiverEmail = newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_EMAIL_ADDRESS];
        orderObj.serviceType = newDataObj[constants.aCommerce.CSV_FIELDS.SERVICE_TYPE];
        orderObj.phone = newDataObj[constants.aCommerce.CSV_FIELDS.RECEIVER_PHONE_NUMBER];
        orderObj.postcode = +(newDataObj[constants.aCommerce.CSV_FIELDS.POSTCODE]);
        orderObj.district = newDataObj[constants.aCommerce.CSV_FIELDS.DISTRICT];
        orderObj.address = newDataObj[constants.aCommerce.CSV_FIELDS.ADDRESS];
        orderObj.count = newDataObj.count;

        if (+newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE]) {
            orderObj.insurance = 1;
            orderObj.declareValue = +newDataObj[constants.aCommerce.CSV_FIELDS.DECLARE_VALUE]
        }

        return {
            order: orderObj,
            errors: errorsCollection
        }
    }

    static postcodeAndDistrictValidation (postcodesFind, mainResult, errorsCollection) {

        let dbData = [];

        postcodesFind.forEach(function (postcodeObj) {

            let addressObj = {};

            let fullDistrict = [];

            addressObj.postcode = postcodeObj.code;

            postcodeObj.cities.forEach(function (districtObj) {

                fullDistrict.push({
                    name: districtObj.name,
                    province: districtObj.region.name
                });
            });

            addressObj.district = fullDistrict;

            dbData.push(addressObj)
        });

        for (let i = 0; i < mainResult.length; i++) {
            let findPostcode = dbData.find(function (data) {
                return mainResult[i].postcode == data.postcode
            });

            if (!findPostcode) {
                errorsCollection.push('Postcode is invalid. Line: ' + (i + 2));
            } else {

                let findDistrict = findPostcode.district.find(function (data) {
                    return mainResult[i].district.toLowerCase() == data.name.toLowerCase()
                });

                if (!findDistrict) {
                    errorsCollection.push('District is invalid for this postcode. Line: ' + (i + 2));
                } else {
                    mainResult[i].province = findDistrict.province;
                }
            }
        }

        return {
            mainResult: mainResult,
            errorsCollection: errorsCollection
        }
    }
}

module.exports = BulkOrderHelper;