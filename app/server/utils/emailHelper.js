var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var jwt = require('jsonwebtoken');
var config = require('./../../config');

var crypt = require('./../utils/crypt');
var constants = require('../constants.json');

var transporter = nodemailer.createTransport(smtpTransport({
    service: 'Gmail',
    auth: {
        user: constants.MAIL.AUTH.USER,
        pass: constants.MAIL.AUTH.PASS
    }
}));

function sendNewPassword (options, cb) {

    if (!cb) {
        return;
    }

    var tokenParams = {
        id: options.id,
        email: options.email
    };


    var hash = jwt.sign(tokenParams, config.jwtKey);


    // var secretField = id + '#' + email;
    // var hash = crypt.encrypt(secretField);
    var link = config.server.baseUrl + '/password/confirm?token='+hash;


    var mailOptions = {
        from: constants.MAIL.AUTH.USER, // sender address
        to: options.email,
        subject: 'New password', // Subject line
        //text: 'Dear, ' + login + '\nYour password to access into administrative panel is: ' + '<a href="#">' + password +'</a>\n' +
        //'We recommend you to change password after authorization'
        html: '<p>Dear, ' + options.firstName + ' ' + options.lastName + '</p><p>Your new password to access into your account: ' + '<b>' + options.password +'</b></p>' +
        'Please follow link: ' + '<a href="'+ link +'">Change password</a>'
    };


    transporter.sendMail(mailOptions, function (error, info) {
        console.log(info);
        if (error) {
            return console.log(error);
        }

        cb(hash);
        console.log('Message sent: ' + info.response);
    });
}

module.exports = sendNewPassword;