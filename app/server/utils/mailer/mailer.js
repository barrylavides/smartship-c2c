'use strict';

var Promise = require('bluebird');
var request = require('request');
const constants = require('./../../constants.json');

var Message = require('./message');

const API_URL = constants.ELASTIC_EMAIL.API_URL;
const INVALID_MESSAGE = 'Invalid param, message must be a Message type!';

class Mailer {
    constructor(config) {
        this.config = config;
    }

    send(message) {
        if (!(message instanceof Message)) {
            let error = new TypeError(INVALID_MESSAGE);
            return Promise.reject(error);
        }

        return message
            .build()
            .then(message => _sendRequest({url: API_URL, config: this.config, message}));
    }
}

module.exports = new Mailer({apiKey: constants.ELASTIC_EMAIL.API_KEY});

/**
 * Send request
 * @param options
 * @param options.url
 * @param options.message
 * @param options.config
 * @private
 */
function _sendRequest(options) {
    let formData = Object.assign({}, options.message, options.config);
    return new Promise((resolve, reject) => {
        request.post(options.url, {formData}, (error, response, body) => {
            if (error) return reject(error);
            console.log('sendRequest', body);
            return resolve(body);
        });
    });
}
