'use strict';

var path = require('path');
var Message = require('./message');
const constants = require('./../../constants.json');

class RegistrationMessage extends Message {
    constructor(options, language) {
        super(options);

        if (language === constants.LANGUAGES.ENGLISH) {
            this.subject = constants.ELASTIC_EMAIL.REGISTRATION_SUBJECT_EN;
            this.templateUrl = path.join(__dirname, './../../../views/registrationEnMessage.html');
        }

        if (language === constants.LANGUAGES.THAILAND) {
            this.subject = constants.ELASTIC_EMAIL.REGISTRATION_SUBJECT_TH;
            this.templateUrl = path.join(__dirname, './../../../views/registrationThMessage.html');
        }

    }
}

module.exports = RegistrationMessage;