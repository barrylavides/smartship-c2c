'use strict';

var fs = require('fs');
var Promise = require('bluebird');

const DEFAULT_MESSAGE = {
    from: '',
    sender: '',
    recipient: '',
    subject: '',
    text: '',
    template: '',
    replaces: {}
};
const ATTRIBUTES = ['from', 'fromName', 'to', 'subject'];

/**
 * Class for email message
 */
class Message {
    constructor(options) {
        let properties = Object.assign({}, DEFAULT_MESSAGE, options);
        this._message = {};

        this.from = properties.from;
        this.sender = properties.sender;
        this.subject = properties.subject;
        this.recipient = properties.recipient;
        this.templateUrl = properties.templateUrl;
        this.replaces = properties.replaces;
    }

    build() {
        return _build.call(this);
    }

    set from(from) {
        if (from) this._message.from = from;
    }

    set sender(sender) {
        if (sender) this._message.fromName = sender;
    }

    set subject(subject) {
        if (subject) this._message.subject = subject;
    }

    set recipient(recipient) {
        if (recipient) this._message.to = Array.isArray(recipient) ? recipient : [recipient];
    }

    set text(bodyText) {
        if (bodyText) this._message.bodyText = bodyText;
    }

    set templateUrl(templateUrl) {
        if (templateUrl) this._message.templateUrl = templateUrl;
    }

    set replaces(replaces) {
        if (replaces) this._message.replaces = replaces;
    }
}

module.exports = Message;

/**
 * Build elastic email message
 * @private
 */
function _build() {
    let message = this._message;
    let result = {};
    if (!message.bodyText && !message.templateUrl) return Promise.reject(new Error('Invalid message contents'));
    for (let attribute of ATTRIBUTES) {
        if (message[attribute]) result[attribute] = message[attribute];
    }

    if (!message.templateUrl) {
        result.bodyText = message.bodyText;
        return Promise.resolve(result);
    }

    return _compileTemplate(message)
        .then((html) => {
            result.bodyHtml = html;
            return result;
        });
}

/**
 * Compile template
 * @param message
 * @returns {Promise.<String>}
 * @private
 */
function _compileTemplate(message) {
    let templateUrl = message.templateUrl;
    return new Promise((resolve, reject)=> {
        fs.readFile(templateUrl, 'utf8', (err, template) => {
            if (err) return reject(err);
            let html = _replacer(template, message.replaces);
            resolve(html);
        });
    })
}

/**
 *
 * @param template
 * @param fields
 * @returns {String}
 * @private
 */
function _replacer(template, fields) {
    var result = template;
    Object
        .keys(fields)
        .forEach((field) => {
            result = result.replace(`{{${field}}}`, fields[field])
        });
    return result;
}