'use strict';

var path = require('path');
var Message = require('./message');

class AdminMessage extends Message {
    constructor(options, referenceNo) {
        super(options);
        this.subject = 'Smartship Withdrawal | ' + referenceNo;
        this.templateUrl = path.join(__dirname, './../../../views/adminMessage.html');
    }
}

module.exports = AdminMessage;