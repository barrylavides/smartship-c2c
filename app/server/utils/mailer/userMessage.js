'use strict';

var path = require('path');
var Message = require('./message');

class UserMessage extends Message {
    constructor(options, referenceNo) {
        super(options);
        this.subject = 'Smartship Withdrawal | ' + referenceNo;
        this.templateUrl = path.join(__dirname, './../../../views/userMessage.html');
    }
}

module.exports = UserMessage;