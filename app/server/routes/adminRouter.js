var express = require('express');
var controllers = require('../controllers');
var middlewares = require('../middlewares');
var router = express.Router();
// var isAdmin = require('../middlewares').isAdmin;

module.exports = function () {
    router.post('/:version/auth', [controllers.callAction('admins.auth')]);

    router.use(middlewares.auth.authentication);

    router.get('/:version/orders', [controllers.callAction('orders.getAll')]);

    return router;
};