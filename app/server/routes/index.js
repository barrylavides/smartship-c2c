var controllers = require('../controllers');
var express = require('express');
var app = express.Router();
var middlewares = require('../middlewares');
var adminRouter = require('./adminRouter');

app.get('/:version/common/test', [controllers.callAction('common.test')]);

/**** REGISTER ***/
app.post('/:version/signup/:action', controllers.callAction('signup.signup'));

app.get('/:version/check/pdf', [controllers.callAction('orders.generatePDF')]);

app.get('/:version/postcode/:id', [controllers.callAction('users.getPostcode')]);

app.get('/:version/languages/:type/texts', [controllers.callAction('languages.getTexts')]);

app.route('/:version/drop-off/points')
    .get([controllers.callAction('dropOffPoints.getPoints')]);

app.route('/:version/payment-callback')
    .post([controllers.callAction('packages.getPaymentCallback')]);

/* TEST API */

app.patch('/:version/orders/:id', [controllers.callAction('orders.testUpdateWeight')]);

// ==========

app.post('/:version/orders/update', [controllers.callAction('orders.updateOrders')]);

app.post('/:version/users', [controllers.callAction('users.add')]);

app.patch('/:version/newpassword', [controllers.callAction('emails.newPass')]);

app.post('/:version/auth/users', [controllers.callAction('users.authUser')]);

app.patch('/:version/users/newpassword', [controllers.callAction('users.changePassword')]);

app.use('/adminpanel', adminRouter());

app.use(middlewares.auth.authentication);


/**** USERS ****/
app.patch('/:version/users/registration', [controllers.callAction('users.registration')]);

app.route('/:version/users/update')
    .patch([controllers.callAction('users.updateUser')]);

app.route('/:version/users/:id')
    .patch([controllers.callAction('users.patch')])
    .get([controllers.callAction('users.get')]);

/**** POSTCODES ****/
app.get('/:version/postcodes/soft-search', [controllers.callAction('postcodes.softSearch')]);
app.get('/:version/postcodes/:id', [controllers.callAction('postcodes.getPostcode')]);

/**** ORDERS ****/
app.post('/:version/orders', [controllers.callAction('orders.create')]);
app.get('/:version/orders', [controllers.callAction('orders.getOrders')]);

app.get('/:version/orders/:id', [controllers.callAction('orders.getOneOrder')]);

app.post('/:version/orders/export', [controllers.callAction('orders.getCSVOrders')]);

app.route('/:version/analytics/orders')
    .get([controllers.callAction('analytics.getRectangularAnalytics')]);

app.route('/:version/analytics/orders/graphs')
    .get([controllers.callAction('analytics.getGraphsAnalytics')]);

app.route('/:version/shipping-charges/:type')
    .get([controllers.callAction('charges.getChargesData')]);

app.get('/:version/orders/:id/pdf/tax-invoice', [controllers.callAction('pdf.getTaxInvoicePDF')]);

app.route('/:version/orders/:id/pdf/shipping-card')
    .get([controllers.callAction('pdf.getShippingCardPDF')]);

app.route('/:version/packages')
    .post([controllers.callAction('packages.createPackage')])
    .get([controllers.callAction('packages.getPackages')]);

app.route('/:version/banks')
    .get([controllers.callAction('packages.getBanks')]);

app.route('/:version/transactions')
    .get([controllers.callAction('transactions.getUsersTransactions')]);

app.route('/:version/refunds')
    .post([controllers.callAction('refunds.createRefund')])
    .get([controllers.callAction('refunds.retrieveRefunds')]);

app.use('*', function (req, res, next) {
    var err = new Error();
    err.status = 404;
    return next(err);
});

module.exports = {
    default: app
};