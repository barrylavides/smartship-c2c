'use strict';
var Base = require('./base.js');
var validator = require('./../../utils/bodyValidator');

class SignupValidator extends Base{
    static facebook (req, res, next){
        return validator({
            properties: {
                fbAccessToken: {
                    required: true,
                    allowEmpty: false,
                    type: 'string'
                }
            }
        })(req, res, next);
    };
}

module.exports = SignupValidator;