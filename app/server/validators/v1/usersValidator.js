'use strict';
var Base = require('./base.js');
var validator = require('./../../utils/bodyValidator');

class UsersValidator extends Base{
    static registration (req, res, next){
        return validator({
            properties: {
                phone: {
                    required: true,
                    allowEmpty: false,
                    type: 'string',
                    minLength: 11,
                    maxLength: 11,
                    pattern: /^66\d{9}/
                },
                address: {
                    required: true,
                    allowEmpty: false,
                    type: 'string',
                    minLength: 1,
                    maxLength: 50
                },
                cityId: {
                    required: true,
                    allowEmpty: false,
                    type: 'integer'
                },
                regionId: {
                    required: true,
                    allowEmpty: false,
                    type: 'integer'
                },
                postcodeId: {
                    required: true,
                    allowEmpty: false,
                    type: 'integer'
                },
                // vat: {
                //     required: false,
                //     allowEmpty: true,
                //     type: 'integer'
                // }
            }
        })(req, res, next);
    };
}

module.exports = UsersValidator;