'use strict';
var Base = require('./base.js');
var validator = require('./../../utils/bodyValidator');

class RefundValidator extends Base{
    static create (req, res, next){
        return validator({
            properties: {
                bankName: {
                    required: true,
                    allowEmpty: false,
                    type: 'string'
                },
                cardholderName: {
                    required: true,
                    allowEmpty: false,
                    type: 'string'
                },
                accountNumber: {
                    required: true,
                    allowEmpty: false,
                    type: 'integer'
                }
            }
        })(req, res, next);
    };
}

module.exports = RefundValidator;