'use strict';
var Base = require('./base.js');
var validator = require('./../../utils/bodyValidator');

class AdminsValidator extends Base{
    static auth (req, res, next){
        return validator({
            properties: {
                email: {
                    type: 'string',
                    required: true,
                    allowEmpty: false,
                    unique: true
                },
                password: {
                    type: 'string',
                    required: true,
                    allowEmpty: false
                }
            }
        })(req, res, next);
    };
}

module.exports = AdminsValidator;