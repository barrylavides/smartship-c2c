'use strict';
var Base = require('./base.js');
var validator = require('./../../utils/bodyValidator');

class OrdersValidator extends Base{
    static create (req, res, next){
        return validator({
            properties: {
                postcode: {
                    required: true,
                    allowEmpty: false,
                    type: 'integer',
                    minimum: 10000,
                    maximum: 99999
                },
                receiverName: {
                    required: true,
                    allowEmpty: false,
                    type: 'string',
                    minLength: 1,
                    maxLength: 200
                },
                phone: {
                    required: true,
                    allowEmpty: false,
                    type: 'string',
                    minLength: 11,
                    maxLength: 11,
                    pattern: /^66\d{9}/
                },
                address: {
                    required: true,
                    allowEmpty: false,
                    type: 'string',
                    minLength: 10
                },
                district: {
                    required: true,
                    allowEmpty: false,
                    type: 'object',
                    minLength: 1,
                    maxLength: 100
                },
                province: {
                    required: true,
                    allowEmpty: false,
                    type: 'string',
                    minLength: 1,
                    maxLength: 100
                }
            }
        })(req, res, next);
    };

    static export (req, res, next) {
        return validator({
            properties: {
                orderIDs: {
                    required: true,
                    type: 'array'
                },
                excludedIDs: {
                    required: true,
                    type: 'array'
                }
            }
        }) (req, res, next)
    }

    static updateWeight (req, res, next) {
        return validator({
            properties: {
                weight: {
                    required: true,
                    type: 'string'
                }
            }
        }) (req, res, next)
    }
}

module.exports = OrdersValidator;