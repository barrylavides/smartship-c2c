const Signup = require('./signupValidator');
const Users = require('./usersValidator');
const Orders = require('./ordersValidator');
const Admins = require('./adminsValidator');
const Refunds = require('./refundValidator');


module.exports = {
    signup: Signup,
    users: Users,
    orders: Orders,
    admins: Admins,
    refunds: Refunds
};