'use strict';

const numeral = require('numeral');
const num2Word = require('num2word');
const moment = require('moment');
const changeNumberStingFormat = require('../utils/regeneratorNumToWords');

function generatePdfForTaxInvoice(orderFind, user) {

    let price = +orderFind.rate;

    let priceFormat = numeral(price).format('0,0.00');

    let vat = numeral(price * 0.07).format('0,0.00');

    let totalWithVAT = numeral(price + (price * 0.07)).format('0,0.00');

    let priceNumber = (price + price * 0.07);

    let totalVATRepresentation = changeNumberStingFormat(num2Word(totalWithVAT));

    totalVATRepresentation = totalVATRepresentation.charAt(0).toUpperCase() + totalVATRepresentation.substr(1);


    let resultSTR = "<div style='position: absolute; top: 20px'><img src='http://redmine.cleveroad.com:4011/img/image001.png'></div>" +
            "<div style='position: absolute; top: 5px; right: 20px; font-size: 20px'><b>Original</b></div>"+
        "<div style='position: absolute;" +
        "top: 75px;" +
        "width: 320px; " +
        "height: auto;'>" +
        "<table style='border-spacing: 6px 0px;'>" +
        "<tr> " +
        "<td><b>Acommerce Company Limited</b></td>" +
        " </tr> " +
        "<tr> " +
        " <td>689 Bhiraj Tower, 33rd Floor,</td>" +
        "</tr> " +
        "<tr> " +
        "<td>Sukhumvit Road, Bangkok 10110, Thailand</td>" +
        "</tr>" +
        "<tr>" +
        "<td>" +
        "Tel. +662 636 3138 Fax. +662-236 2805" +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td>" +
        "Head Office" +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td>" +
        "Tax ID.0105556095051" +
        "</td>" +
        "</tr>" +
        "</table>" +
        "</div>" +
        "<div style='position: absolute;" +
        "top: 75px;" +
        "left: 590px;" +
        "width: 300px;'>" +
        "<table style='position: absolute; left: 120px'>" +
        "  <tr>" +
        "   <td><b>Receipt / Tax Invoice</b></td>" +
        "</tr>" +
        "<tr>" +
        "<td><br></td>" +
        "</tr>" +
        "<tr>" +
        "<td>Date : "+moment(orderFind.createdAt).format('DD-MMM-YY')+"</td>" +
        "</tr>" +
        "<tr>" +
        "<td>" +
        "No : " + orderFind.trackingId +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td>" +
        "Ref. Invoice No: 3912048" +
        "</td>" +
        "</tr>" +

        "</table>" +
        "</div> <div style='position: absolute; top: 230px; width: 875px; height: 160px; border-radius: 20px; border: 1px solid black;'>" +
        "<table style='position: absolute; top: 10px; left: 10px'>" +
        "<tr>" +
        "<td><b>To</b></td>" +
        "<td style='padding-left: 20px'>"+user.firstName + ' ' + user.lastName+"</td>" +
        "</tr>" +
        "<tr style='vertical-align: top'>" +
        "<td><b>Address</b></td>" +
        "<td style='padding-left: 20px'>"+user.address+"<br>Tax ID. "+user.nationalId+"</td>" +
        "</tr>" +
        "<tr>" +
        "<td><b>Tel.</b></td>" +
        "<td style='padding-left: 20px'>"+user.phone+"</td>" +
        "</tr>" +
        "<tr>" +
        "<td><b>Email</b></td>" +
        "<td style='padding-left: 20px'>"+user.email+"</td>" +
        "</tr>" +
        "</table>" +
        "</div> <div style='position: absolute; top: 410px; width: 875px; border: 2px solid black'>"+
        "<table style='width: 100%;border-collapse: collapse;'> " +
        "<tr>" +
        "<td style=' border: 1px solid black;" +
        "border-collapse: collapse; padding: 15px; text-align: center'>No.</td>" +
        "<td style=' border: 1px solid black;" +
        "border-collapse: collapse;padding: 15px; text-align: center'>Quantity</td>" +
        "<td style='border: 1px solid black;" +
        "border-collapse: collapse;padding: 15px; text-align: center'>Item Name</td>" +
        "<td colspan='2' style='border: 1px solid black;" +
        "border-collapse: collapse;padding: 15px; text-align: center'>Amount</td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +

        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;" +
        "border-collapse: collapse; text-align:center'>1</td>" +
        "<td style='border: 1px solid black; border-collapse: collapse;text-align:center'>1</td>" +
        "<td style='border: 1px solid black;" +
        "border-collapse: collapse;'>"+orderFind.orderName+"</td>" +
        "<td colspan='2' style='border: 1px solid black;" +
        "border-collapse: collapse; text-align:right'>"+priceFormat+"</td>" +

        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +

        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +

        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +

        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +

        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +

        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +

        "<tr>" +
        "<td style='border: 1px solid black;'><br></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td style='border: 1px solid black;'></td>" +
        "<td colspan='2' style='border: 1px solid black;'></td>" +
        "</tr>" +
        "<tr>" +
        "<td style='border-top: 2px solid black;" +
        "border-collapse: collapse;text-align:center' rowspan='3' colspan='3'><br>"+
        "<td style='border: 1px solid black; border-top: 2px solid black;"+
        "border-collapse: collapse;text-align:right'>Total</td>"+
        "<td style='border: 1px solid black; border-top: 2px solid black;"+
        "border-collapse: collapse;text-align:right'>"+priceFormat+"</td>"+
        "</tr>"+
        "<tr style='border: 1px solid black;"+
        "border-collapse: collapse;text-align:right'>"+
        "<td style='border: 1px solid black;"+
        "border-collapse: collapse; text-align:right'>Discount</td>"+
        "<td style='border: 1px solid black;"+
        "border-collapse: collapse;text-align:right'>-</td>"+
        "</tr>"+
        "<tr>"+
        "<td style='border: 1px solid black;"+
        "border-collapse: collapse;text-align:right'>VAT</td>"+
        "<td style='border: 1px solid black;"+
        "border-collapse: collapse;text-align:right'>"+vat+"</td>" +
        "</tr>" +
        "<td colspan='3' style='border: 1px solid black;" +
        "border-collapse: collapse; padding: 12px;'>"+totalVATRepresentation+"</td>" +
        "<td style='border: 1px solid black;" +
        "border-collapse: collapse; text-align:right'>Grand Total</td>"+
        "<td style='border: 1px solid black;" +
        "border-collapse: collapse; text-align:right'>"+totalWithVAT+"</td>" +
        "</tr>" +
        "</table>" +
        "</div>" +
        "<div  style='position: absolute; top: 970px; width: 875px; height: 150px; border-radius: 20px;'>" +
        "<table>"+
        "<tr>"+
        "<td>Paid by</td>"+
        "<td style='padding-left: 20px'><input type='checkbox'>By Cheque ..........."+
        "&nbsp;&nbsp;&nbsp;"+
        "Chq.No ..........."+
        "&nbsp;&nbsp;&nbsp;"+
        "Chq.Date ..........."+
        "&nbsp;&nbsp;&nbsp;"+
        "Branch ...........</td>"+
        "</tr>"+
        "<tr>"+
        "<td><br></td>"+
        "<td style='padding-left: 20px'><input type='checkbox'>By Cash                 &nbsp;&nbsp;&nbsp;"+
        "<input type='checkbox'>Bank Transfer</td>"+
        "</tr>"+
        "</table>" +
        "</div>" +
        "<div  style='position: absolute; top: 1020px; width: 875px;'>" +
        "<div style='margin-right: 10px; width: 281.5px;  float: left;'>" +
        "<div style='width: 281.5px; height: 20px; text-align:center'></div>" +
        "<div style='height: 60px; border:1px solid black; border-radius: 15px; padding: 15px; width=100%'>" +
        "Prepared By" +
        "<br>"+
        "<br>"+
        "Date "+ moment(orderFind.createdAt).format('DD-MM-YY') +
        "</div>"+
        "</div>"+
        "<div style='margin-right: 10px; width: 281.5px;  float: left;'>"+
        "   <div style='width: 281.5px; height: 20px; text-align:center'><b>ACommerce Company Limited</b></div>"+
        "<div style='height: 60px; border:1px solid black; border-radius: 15px; padding: 15px; width=100%'>"+
        "   Approved By"+
        "<br>"+
        "<br>"+
        "Date"+
        "</div>"+
        "</div>"+
        "<div style='margin-right: 10px; width: 281.5px;  float: left;'>"+
        "<div style='width: 281.5px; height: 20px; text-align:center'><b>ACommerce Company Limited</b></div>"+
        "<div style='height: 60px; border:1px solid black; border-radius: 15px; padding: 15px; width=100%'>"+
        "Recipient"+
        "<br>"+
        "<br>"+
        "Date"+
        "</div>"+
        "</div>" +
        "<div style='position:absolute; top: 130px'><b>This receipt will be valid only when the above cheque has been approved by the bank</b>" +
        "</div>";

    return resultSTR;


}

module.exports = generatePdfForTaxInvoice;