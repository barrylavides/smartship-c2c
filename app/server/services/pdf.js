function generatePdf(doc) {

    doc.save()
        .moveTo(100, 150)
        .lineTo(100, 250)
        .lineTo(200, 250)
        .fill("#FF3300");
}

module.exports = generatePdf;