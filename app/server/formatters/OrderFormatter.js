'use strict';

const constants = require('../constants.json');

class UserFormatters {
    static createOrder (user, body, documentAccess) {

        let createOrder = {};

        createOrder.userId = user.id;
        createOrder.orderName = body.orderName;
        createOrder.receiverName = body.receiver.addressee;
        createOrder.receiverEmail = body.receiver.email;
        createOrder.serviceType = Number(body.service);
        createOrder.phone = body.receiver.phone;
        createOrder.postcode = body.receiver.postcode;
        createOrder.district = body.receiver.district.name;
        createOrder.province = body.receiver.province;
        createOrder.address = body.receiver.address;
        createOrder.price = body.gross;
        createOrder.insurance = +body.insurance;
        createOrder.declareValue = body.declareValue;

        if (documentAccess) {
            createOrder.documentAccess = 0;
        }

        return createOrder;
    }

    static generateAcommerceData (user, city, postcode, body) {
        let options = {
            user: user,
            city: city,
            postcode: postcode,
            body: body
        };

        return options;
    }

    static formatOneOrder (orderVal, user, postcodeFind, cityFind) {
        let result = {
            serviceType: orderVal.serviceType,
            documentAccess: orderVal.documentAccess,
            orderDistrict: orderVal.district,
            packageLabel: orderVal.packageLabel,
            orderPostcode: orderVal.postcode,
            orderProvince: orderVal.province,
            orderName: orderVal.orderName,
            address: orderVal.address,
            price: orderVal.price,
            status: orderVal.status,
            insurance: orderVal.insurance,
            declareValue: orderVal.declareValue,
            rate: orderVal.rate,
            weight: orderVal.weight,
            trackingId: orderVal.trackingId,
            updatedAt: orderVal.updatedAt,
            createdAt: orderVal.createdAt
        };

        result.businessForm = {
            phone: user.phone,
            nationalId: user.nationalId,
            postcode: postcodeFind.code,
            district: cityFind.name,
            province: cityFind.region.name,
            address: user.address,
            vat: user.vat
        };

        return result;
    }

    static getRateValue (orderFind, rateFind) {
        let rate = rateFind.rate;
        if ((orderFind.serviceType === constants.aCommerce.SERVICES.SMARTSHIP_DROPOFF || orderFind.serviceType === constants.aCommerce.SERVICES.SMARTSHIP_KERRY)
            && orderFind.province !== constants.aCommerce.DEFAULT_CITY && rateFind.id) {

            rate = rateFind.provincial;
        }

        return rate;
    }

    static formatACommerceOrderData (data) {
        let result = {};
        if (data.acommerceId) {
            result.trackingId = data.acommerceId;
        }
        if (data.shipPackages && data.shipPackages.length) {
            let packData = data.shipPackages[0];
            if (packData.packageTrackingId) {
                result.trackingId = packData.packageTrackingId;
            }
            if (packData.packageLabel) {
                result.packageLabel = packData.packageLabel;
            }
            result.weight = packData.packegeWeight || 0;
        }
        return result;
    }

    static updateAmountBalance (userFind, orderUpdated, updatedBody) {
        let updateAmountObj = {};
        updateAmountObj.availableAmount = userFind.availableAmount - orderUpdated.rate;

        if (updatedBody.status === constants.ORDER_STATUS.IN_TRANSIT && orderUpdated.insurance) {
            updateAmountObj.availableAmount = updateAmountObj.availableAmount - (orderUpdated.declareValue * constants.DECLARE_VALUE);
        }

        return updateAmountObj;
    }
}

module.exports = UserFormatters;