'use strict';

class AnalyticsFormatter {
    static rectangularValues (orders) {
        let resObj = {
            intransitOrders: {
                count: orders[0].intransit_now_7,
                volume: AnalyticsFormatter.calculateVolume(orders[0].intransit_now_7, orders[0].intransit_7_end)
            },
            deliveredOrders: {
                count: orders[0].delivered_now_7,
                volume: AnalyticsFormatter.calculateVolume(orders[0].delivered_now_7, orders[0].delivered_7_end)
            },
            pendingOrders: {
                count: orders[0].pending_now_7,
                volume: Math.round((orders[0].pending_now_7 / orders[0].total_pending) * 100)
            },
            chargeOrders: {
                count: orders[0].shipping_charge
            }
        };

        return resObj;
    }

    static calculateVolume (firstValue, secondValue) {

        let result = Math.round(((firstValue / secondValue) * 100) - 100);

        if (firstValue === 0 && secondValue === 0) {
            result = 0;
        }
        return result;
    }
}

module.exports = AnalyticsFormatter;