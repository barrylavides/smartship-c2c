'use strict';

class UserFormatters {
    static emailFormatter (user, pass) {

        let emailOptions = {
            id: user.id,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            password: pass
        };

        return emailOptions;
    }

    static synchronizeFormatter (socialRes) {
        let socialData = {
            id: socialRes.id,
            socialId: socialRes.socialId,
            socialEmail: socialRes.socialEmail,
            userId: socialRes.userId,
            socialType: socialRes.socialType,
            createdAt: socialRes.createdAt,
            updatedAt: socialRes.updatedAt
        };

        return socialData;
    }
}

module.exports = UserFormatters;