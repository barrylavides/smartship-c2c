'use strict';
const constants = require('../constants.json');

class RefundsFormatters {
    static createFormatter (user, count) {

        let refundOptions = {
            userId: user.id,
            referenceNo: 'SHIP-' + user.id + '-' + ++count,
            amount: user.availableAmount
        };

        return refundOptions;
    }

    static sendAdmin (user, body, confirm) {
        confirm.from = constants.ELASTIC_EMAIL.FROM_VAL;
        confirm.sender = constants.ELASTIC_EMAIL.SENDER_VAL;
        confirm.recipient = constants.ELASTIC_EMAIL.ACOMMERCE_EMAIL;

        confirm.replaces = {
            bankName: body.bankName,
            cardholderName: body.cardholderName,
            accountNumber: body.accountNumber,
            name: user.firstName + ' ' + user.lastName,
            amount: user.availableAmount
        };

        return confirm;
    }

    static sendUser (user, confirm) {
        confirm.from = constants.ELASTIC_EMAIL.FROM_VAL;
        confirm.sender = constants.ELASTIC_EMAIL.SENDER_VAL;
        confirm.recipient = user.email;

        return confirm;
    }
}

module.exports = RefundsFormatters;