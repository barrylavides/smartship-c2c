'use strict';
const constants = require('./../constants.json');
const languages = require('./../../languages');

class LanguageFormatters {
    static typesHelper (type) {
        let mainResult;

        if (+type === constants.LANGUAGES.ENGLISH) {
            mainResult = languages.english;
        }

        if (+type === constants.LANGUAGES.THAILAND) {
            mainResult = languages.thailand
        }

        return mainResult
    }
}

module.exports = LanguageFormatters;