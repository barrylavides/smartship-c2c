'use strict';

const constants = require('../constants.json');
const config = require('../../config');

class PackageFormatter {
    static finishTransaction (paymentResult, packageData) {

        let transactionObj = {};

        transactionObj.last4 = paymentResult.pan[0].slice(-4);
        transactionObj.amount = packageData.cost;
        transactionObj.invoiceNo = paymentResult.uniqueTransactionCode[0];
        transactionObj.status = config.PAYMENT_2C2P_DATA.TRANSACTION_STATUSES[paymentResult.status[0]];
        transactionObj.updatedAt = Math.floor(Date.now() / 1000);

        return transactionObj;
    }

    static updateUserPackageData (user, packageData) {
        let userObject = {};

        if (user.havePackage === 0) {
            userObject.havePackage = 1;
        }
        
        userObject.availableAmount = user.availableAmount + packageData.cost;
        userObject.availableCount = packageData.offersLimit;
        userObject.currentLowLimit = packageData.lowLimit;
        userObject.packageType = packageData.type;

        return userObject;
    }

    static formatPackage (data) {
        data.unshift(data.pop());
        return data;
    }
}

module.exports = PackageFormatter;