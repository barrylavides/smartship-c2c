var auth = require('./auth');
var admin = require('./isAdmin');

module.exports = {
    auth:  auth,
    isAdmin: admin
};