module.exports = function(req, res, next) {
    if (req.user.role !== 1) { return next() }

    next(new Error('{"status": 401}'));
};