"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let rateValues = sequelize.define("rateValues", {
        minWeight: {
            type: DataTypes.DECIMAL(10, 2),
            field: 'min_weight'
        },
        maxWeight:{
            type: DataTypes.DECIMAL(10, 2),
            field: 'max_weight'
        },
        provincial: {
            type: DataTypes.DECIMAL(10, 2)
        },
        rate: {
            type: DataTypes.DECIMAL(10, 2)
        },
        serviceType: {
            type: DataTypes.INTEGER,
            field: 'service_type'
        }
    }, {
        timestamps: false,
    });

    return rateValues;
};