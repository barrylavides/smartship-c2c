"use strict";

/**
 * postcode model
 *
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let postcodes = sequelize.define("postcodes", {
        code: {
            type: DataTypes.STRING,
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.postcodes.belongsToMany(models.cities, {through: models.postcode2cities});
            }
        }
    });

    return postcodes;
};