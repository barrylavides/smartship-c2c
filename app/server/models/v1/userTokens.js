"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let userTokens = sequelize.define("userTokens", {
        userId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'user_id'
        },
        token: {
            type: DataTypes.STRING,
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return userTokens;
};