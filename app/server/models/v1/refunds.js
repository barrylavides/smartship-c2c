"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let refunds = sequelize.define("refunds", {
        userId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'user_id'
        },
        referenceNo: {
            type: DataTypes.STRING,
            field: 'reference_no'
        },

        amount: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
            hasComment: { type: DataTypes.INTEGER, comment: "Status can be 1 - Pending, 2 - Approved, 3 - rejected" }
        },
        createdAt: {
            type: DataTypes.INTEGER,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.INTEGER,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.refunds.belongsTo(models.users, {foreignKey:'userId'});
            }
        },
        timestamps: false,
    });

    refunds.beforeCreate(function (model, done) {
        model.createdAt = Math.floor(Date.now() / 1000);
        model.updatedAt = Math.floor(Date.now() / 1000);
    });

    return refunds;
};