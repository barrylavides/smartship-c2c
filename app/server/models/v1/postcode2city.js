"use strict";

/**
 *
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let postcode2cities = sequelize.define("postcode2cities", {
        cityId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'city_id'
        },
        postcodeId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'postcode_id'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
            }
        }
    });

    return postcode2cities;
};