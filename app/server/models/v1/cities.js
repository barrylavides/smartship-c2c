"use strict";

/**
 * cities model
 *
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let cities = sequelize.define("cities", {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        regionId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'region_id'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.cities.belongsToMany(models.postcodes, {through: models.postcode2cities});
                models.cities.belongsTo(models.regions, {foreignKey:'regionId'});
            }
        }
    });

    return cities;
};