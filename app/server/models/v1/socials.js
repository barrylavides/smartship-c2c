"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let socials = sequelize.define("socials", {
        socialId: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'social_id'
        },
        userId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'user_id'
        },
        socialType: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'social_type'
        },
        socialEmail: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            field: 'social_email'
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.socials.belongsTo(models.users, {foreignKey:'userId'});
            }
        }
    });

    return socials;
};