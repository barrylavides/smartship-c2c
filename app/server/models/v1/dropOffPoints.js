"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let dropOffPoints = sequelize.define("dropOffPoints", {
        postOffice: {
            type: DataTypes.STRING,
            field: 'post_office'
        },
        phone: {
            type: DataTypes.STRING
        },
        latitude:{
            type: DataTypes.DECIMAL(10, 8)
        },
        longitude: {
            type: DataTypes.DECIMAL(11, 8)
        },
        serviceType: {
            type: DataTypes.INTEGER,
            field: 'service_type'
        },
        postcode: {
            type: DataTypes.INTEGER
        },
        address: {
            type: DataTypes.STRING
        }
    }, {
        timestamps: false
    });

    return dropOffPoints;
};