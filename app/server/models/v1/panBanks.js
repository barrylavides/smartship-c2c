"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let panBanks = sequelize.define("panBanks", {
        name: {
            type: DataTypes.STRING
        },
        value: {
            type: DataTypes.STRING
        }
    }, {
        timestamps: false,
    });

    return panBanks;
};