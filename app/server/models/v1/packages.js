"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let packages = sequelize.define("packages", {
        name: {
            type: DataTypes.STRING
        },
        cost:{
            type: DataTypes.INTEGER
        },
        type: {
            type: DataTypes.INTEGER,
            unique: true
        },
        lowLimit: {
            type: DataTypes.INTEGER,
            field: 'low_limit'
        },
        offersLimit: {
            type: DataTypes.INTEGER,
            field: 'offers_limit'
        }
    }, {
        timestamps: false,
    });

    return packages;
};