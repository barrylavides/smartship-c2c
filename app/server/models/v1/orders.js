"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let orders = sequelize.define("orders", {
        orderName: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'order_name'
        },
        userId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'user_id'
        },
        receiverName: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'receiver_name'
        },
        serviceType: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        insurance: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        declareValue: {
            type: DataTypes.DECIMAL(10, 2),
            field: 'declare_value',
            validate: {
                isDecimal: true
            },
            defaultValue: 0
        },
        documentAccess: {
            type: DataTypes.INTEGER,
            defaultValue: 1,
            field: 'document_access'
        },
        packageLabel: {
            type: DataTypes.STRING,
            field: 'package_label'
        },
        rate: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: true
        },
        receiverEmail: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            },
            field: 'receiver_email'
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: 0
        },
        trackingId: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'tracking_id'
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false
        },
        postcode: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        district: {
            type: DataTypes.STRING,
            allowNull: false
        },
        province: {
            type: DataTypes.STRING,
            allowNull: false
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false
        },
        weight: {
            type: DataTypes.STRING,
            allowNull: true
        },
        createdAt: {
            type: 'DATE',
            field: 'created_at'
        },
        updatedAt: {
            type: 'DATE',
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.orders.belongsTo(models.users, {foreignKey:'userId'});
            }
        },
        timestamps: false,
    });

    return orders;
};