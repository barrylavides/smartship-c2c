"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */
module.exports = function (sequelize, DataTypes) {
    let transactions = sequelize.define("transactions", {
        userId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'user_id'
        },
        packageId:{
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'package_id'
        },
        invoiceNo: {
            type: DataTypes.STRING,
            field: 'invoice_no'
        },
        last4: {
            type: DataTypes.INTEGER
        },
        amount: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        createdAt: {
            type: 'DATE',
            field: 'created_at'
        },
        updatedAt: {
            type: 'DATE',
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.transactions.belongsTo(models.users, {foreignKey:'userId'});
                models.transactions.belongsTo(models.packages, {foreignKey:'packageId'});
            }
        },
        timestamps: false,
    });

    transactions.beforeCreate(function (model, done) {
        model.createdAt = Math.floor(Date.now() / 1000);
        model.updatedAt = Math.floor(Date.now() / 1000);
    });

    return transactions;
};