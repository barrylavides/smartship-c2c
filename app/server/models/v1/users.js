"use strict";
/**
 * user models
 * status [0-created, 1-registered]
 * @param sequelize
 * @param DataTypes
 * @returns {*|{}|Model}
 */

var crypto = require('crypto');
var algorithm = 'aes-256-ctr';

var encrypt = function (str) {
    //return crypto.createHash('md5').update(str).digest("hex");
    var cipher = crypto.createCipher(algorithm, str);
    var crypted = cipher.update(str, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
};

var encryptPass = function (password, salt) {
    var tmpPass = encrypt(password);
    return encrypt(tmpPass + salt);
};

var verifyPassword = function (incomingPassword) {
    return this.password == encryptPass(incomingPassword, this.salt);
};

var generateSalt = function () {
    return crypto.randomBytes(16).toString('base64');
};


var instanceMethods = {
    verifyPassword: verifyPassword,
    generateSalt: generateSalt,
    encryptPass: encryptPass
};


module.exports = function (sequelize, DataTypes) {
    let users = sequelize.define("users", {
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            }
        },
        firstName: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'first_name'
        },
        lastName: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'last_name'
        },
        nationalId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'national_id'
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        },
        regionId:{
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'region_id'
        },
        availableAmount: {
            type: DataTypes.DECIMAL(10, 2),
            defaultValue: 0,
            allowNull: false,
            field: 'available_amount'
        },
        availableCount: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
            field: 'available_count'
        },
        currentLowLimit: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
            field: 'current_low_limit'
        },
        havePackage: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            field: 'have_package'
        },
        packageType: {
            type: DataTypes.INTEGER,
            field: 'package_type'
        },
        salt: {
            type: DataTypes.STRING,
            field: 'salt'
        },
        role: {
            type: DataTypes.INTEGER,
            field: 'role',
            defaultValue: 0
        },
        password: {
            type: DataTypes.STRING,
            field: 'password'
        },
        cityId:{
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'city_id'
        },
        language: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        postcodeId:{
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'postcode_id'
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true
        },
        vat: {
            type: DataTypes.STRING,
            allowNull: true
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }, {
        classMethods: {
            associate: function (models) {
                models.users.hasMany(models.socials, {foreignKey:'userId'});
            }
        },
        instanceMethods: instanceMethods
    });

    users.beforeCreate(function (model, done) {

        if (model.password) {
            model.salt = generateSalt();
            model.password = encryptPass(model.password, model.salt);
        }
    });

    return users;
};