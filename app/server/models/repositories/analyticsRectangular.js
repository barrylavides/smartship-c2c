'use strict';

const sequelize = require('../v1').sequelize;
const constants = require('../../constants.json');

function getAnalytics (userId, cb) {
    let query = sequelize.query('SELECT ' +
        '(SELECT sum(rate) FROM orders WHERE DATEDIFF(NOW(), created_at) <= 13 AND user_id = '+userId+' AND status IN ('
        +constants.ORDER_STATUS.IN_TRANSIT+ ',' + constants.ORDER_STATUS.DELIVERED + ',' + constants.ORDER_STATUS.CANCELLED
        +')) as `shipping_charge`,' +
        '(SELECT count(*) FROM orders WHERE user_id = '+userId+' AND DATEDIFF(NOW(), created_at) <= 6 AND orders.status = 2) as `intransit_now_7`,' +
        '(SELECT count(*) FROM orders WHERE user_id = '+userId+' AND DATEDIFF(DATE_ADD(NOW(), INTERVAL -7 DAY), created_at) between 0 and 7 AND orders.status = 2) as `intransit_7_end`,' +
        '(SELECT count(*) FROM orders WHERE user_id = '+userId+' AND DATEDIFF(NOW(), created_at) <= 6 AND orders.status = 3) as `delivered_now_7`,' +
        '(SELECT count(*) FROM orders WHERE user_id = '+userId+' AND DATEDIFF(DATE_ADD(NOW(), INTERVAL -7 DAY), created_at) between 0 and 7 AND orders.status = 3) as `delivered_7_end`,' +
        '(SELECT count(*) FROM orders WHERE user_id = '+userId+' AND DATEDIFF(NOW(), created_at) > 2 AND (orders.status = 0 OR orders.status = 1)) as `pending_now_7`,' +
        '(SELECT count(*) FROM orders WHERE user_id = '+userId+' AND (orders.status = 0 OR orders.status = 1)) as `total_pending`',
        {type: sequelize.QueryTypes.SELECT})
        .then(function (ordersStatistics) {
            if (!cb) {
                return;
            }
            cb(ordersStatistics)
        });
}

module.exports = {
    orders: getAnalytics
};