'use strict';

const sequelize = require('../v1').sequelize;
const constants = require('../../constants.json');

function getGraphs (userId, status, cb) {
    let query = sequelize.query('SELECT count(*) as `count`,' +
        ' unix_timestamp(created_at) AS `date`' +
        'FROM orders ' +
        'WHERE DATEDIFF(NOW(), created_at) <= 29 AND status = '+status+' AND user_id = ' + userId +
        ' GROUP BY DAY(created_at)',
        {type: sequelize.QueryTypes.SELECT})
        .then(function (orderStatistics) {
            if (!cb) {
                return;
            }
            cb(orderStatistics)
        })
}

module.exports = {
    orders: getGraphs
};