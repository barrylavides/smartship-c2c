"use strict";
const async = require('async');
const getController = function (controller, version) {
    let Controller = require('./' + version + '/' + controller);
    return new Controller(version);
};

const versions = {
    v1: {
        common: getController.apply(this, ['commonController', 'v1']),
        signup: getController.apply(this, ['signUpController', 'v1']),
        users: getController.apply(this, ['usersController', 'v1']),
        postcodes: getController.apply(this, ['postcodesController', 'v1']),
        orders: getController.apply(this, ['ordersController', 'v1']),
        admins: getController.apply(this, ['adminsController', 'v1']),
        pdf: getController.apply(this, ['pdfController', 'v1']),
        emails: getController.apply(this, ['emailsController', 'v1']),
        analytics: getController.apply(this, ['analyticsController', 'v1']),
        charges: getController.apply(this, ['chargesController', 'v1']),
        dropOffPoints: getController.apply(this, ['dropOffPointsController', 'v1']),
        languages: getController.apply(this, ['languagesController', 'v1']),
        packages: getController.apply(this, ['packagesController', 'v1']),
        transactions: getController.apply(this, ['transactionsController', 'v1']),
        refunds: getController.apply(this, ['refundsController', 'v1'])
    }
};

module.exports = {
    static: require('./staticController'),
    callAction: function (route) {
        return function (req, res, next) {
            let error = new Error();

            if (!versions[req.params.version]) {
                error.message = 'Unsupported API version';
                error.status = 403;
                return next(error);
            }

            let controller = route.split('.')[0];
            let action = route.split('.')[1];
            if (!versions[req.params.version][controller]) {
                error.message = 'Controller not implemented';
                error.status = 405;
                return next(error);
            }

            if (!versions[req.params.version][controller][action]) {
                error.message = 'Action not implemented';
                error.status = 405;
                return next(error);
            }

            let operations = Array.isArray(versions[req.params.version][controller][action])
                ? versions[req.params.version][controller][action]
                : [versions[req.params.version][controller][action]];
            operations = operations.map(function (middleware) {
                return middleware.bind(versions[req.params.version][controller], req, res);
            });

            async.series(operations, next);
        }
    }
};
