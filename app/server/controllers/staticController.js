var fs = require('fs');
var config = require('../../config');

function renderStaticHtmlFile (path) {
    return function (req, res, next) {
        fs.createReadStream(__dirname + '/../../frontend/' + path)
            .pipe(res);
    }
}

module.exports = {
    frontendApp: renderStaticHtmlFile('app/index.html'),
    adminPanel: renderStaticHtmlFile('adminpanel/index.html'),
    print: renderStaticHtmlFile('print/index.html'),
    changePasswordPage: renderStaticHtmlFile('app/views/changePass.ejs'),
    changePasswordSuccess: renderStaticHtmlFile('app/views/success.ejs')
};