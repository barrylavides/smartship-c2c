"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const multiparty = require('multiparty');
const fastCSV = require('fast-csv');

class DropOffPointsController extends Controller {
    constructor(version) {
        super(version);

        this.getPoints = [this._getDropOffPoints];
    }

    _getDropOffPoints (req, res, next) {
        let pointTypes = [];

        if (Object.keys(req.query).length !== 0) {
            pointTypes = req.query.types;
        }

        Models.dropOffPoints.findAll({
            where: {
                serviceType: {$in: pointTypes}
            }
        })
            .then(function (pointsFind) {
                return res.send(pointsFind)
            })
            .catch(function (err) {
                return next(err)
            })
    }


}

module.exports = DropOffPointsController;