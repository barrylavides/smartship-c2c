"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');

class CommonController extends Controller {
    constructor(version) {
        super(version);
        this.test = [this._test];
    }

    _test(req, res, next) {
        console.log('test');
        res.send({});

    }
}

module.exports = CommonController;