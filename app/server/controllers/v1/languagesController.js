"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const Format = require('./../../formatters/LanguagesFormatter');

/**
 * Language controller
 */
class LanguageController extends Controller {
    constructor(version) {
        super(version);

        this.getTexts = [this._getLanguagesTexts]
    }

    _getLanguagesTexts (req, res, next) {
        if (!+req.params.type) {
            return next(new HTTPError(400, 'Param "type" must be an integer type'))
        }

        let mainResult = Format.typesHelper(req.params.type);

        return res.send(mainResult)
    }
}

module.exports = LanguageController;