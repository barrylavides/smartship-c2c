"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const Format = require('../../utils/format');
const jwt = require('jsonwebtoken');
const config = require('./../../../config')
const HTTPError = require('node-http-error');

/**
 * Admins controller
 */
class AdminsController extends Controller {

    constructor(version) {
        super(version);
        this.auth = [this.validator.admins.auth, this._authAdmin];
    }

    _authAdmin(req, res, next) {
        Models.users.findOne({where: {email: req.body.email, role: 2}})
            .then(function (admin) {
                if (!admin) {
                    return next(new HTTPError(404, 'Admin not found!'))
                }

                let validPass = admin.verifyPassword(req.body.password);
                if (validPass) {
                    let result = Format.authAdminFormat(admin);

                    let tokenParams = {
                        createTime: Date.now(),
                        id: admin.id
                    };

                    result.token = jwt.sign(tokenParams, config.jwtKey);

                    return res.send(result);
                }

                return next(new HTTPError(400, 'Incorrect email or password!'))
            })
            .catch(function (err) {
                return next(err)
            })
    }
}


module.exports = AdminsController;