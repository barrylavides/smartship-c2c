"use strict";

const Controller = require('./../../utils/controller');
const FB = require('facebook-node');
const Models  = require('../../models/v1');
const Format = require('./../../utils/format');
const usersFormatter = require('../../formatters/UserFormatter');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const HTTPError = require('node-http-error');

class SignUpController extends Controller {
    constructor (version) {
        super(version);
        this.signup = this._chooseStrategy;
    }

    /**
     * choose signup strategy
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @returns {*}
     *
     * @private
     */
    _chooseStrategy(req, res, next) {
        var self = this;
        var strategies = {
            facebook: [this.validator.signup.facebook, self._fetchFBData, self._checkForExistFbUser,
                self._createNewFbUser, self._createNewFbSocial, self._checkForSynchronizeSocials,
                self._synchronizeSocials, self._authUser]
        };

        if (!strategies[req.params.action]) {
            var error = new Error();
            error.status = 404;
            return next(error);
        }

        var operations = strategies[req.params.action].map(function (middleware) {
            return middleware.bind(self, req, res);
        });

        require('async').series(operations, next);
    }

    _fetchFBData (req, res, next) {
        FB.setAccessToken(req.body.fbAccessToken);
        FB.api('/me', {fields: ['email', 'first_name', 'last_name', 'id']}, function (fbUser) {
            if (!fbUser) {
                let err = new Error();
                err.status = 400;
                err.message = 'Can\'t get response from facebook';
                return next(err);
            }

            req.fbUser = fbUser;

            return next();
        })
    }

    _checkForExistFbUser (req, res, next) {
        Models.socials.findOne({
            where: {socialId: req.fbUser.id},
            include: [{model: Models.users}]
        })
            .then(function (user) {
                req.user = user;

                if (user && req.body.syncFB) {
                    return next(new HTTPError(400, 'User with such FB account already exists'))
                }

                if (user) {
                    req.userFind = true;
                    return next();
                }

                let format = Format.registerUserFromFacebook(req.fbUser);

                if (req.body.userID) {

                    req.userIdFromBody = true;
                    req.formatFB = format;
                    return next()
                }

                let newUser = format.user;

                req.newUser = newUser;
                req.userFormat = format;

                return next();
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _createNewFbUser (req, res, next) {
        if (req.userFind) {
            return next()
        }

        if (req.userIdFromBody) {
            return next()
        }

        Models.users.create(req.newUser)
            .then(function (userCreate) {
                let newSocialUser = req.userFormat.social;
                newSocialUser.userId = userCreate.id;

                req.userCreate = userCreate;
                req.newSocialUser = newSocialUser;

                return next();
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _createNewFbSocial (req, res, next) {
        if (req.userFind) {
            return next()
        }

        if (req.userIdFromBody) {
            return next()
        }

        Models.socials.create(req.newSocialUser)
            .then(function (socialUserCreate) {
                req.user = Format.formatNewUser(req.userCreate, socialUserCreate);

                req.newUser = true;

                return next();
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _synchronizeSocials (req, res, next) {

        if (!req.userIdFromBody) {
            return next();
        }

        let socialData = req.formatFB.social;
        socialData.userId = req.body.userID;
        
        Models.socials.create(socialData)
            .then(function (socialCreate) {
                return res.send(socialCreate)
            })
            .catch(function (err) {
                return next(err)
            });
    }

    _checkForSynchronizeSocials (req, res, next) {
        if (req.userIdFromBody) {
            return next();
        }

        if (req.newUser) {
            return next();
        }

        if (req.body.userID) {
            req.user.update({userId: req.body.userID})
                .then(function (socialRes) {

                    let socialData = usersFormatter.synchronizeFormatter(socialRes);

                    return res.send(socialData)
                })
                .catch(function (err) {
                    return next(err)
                });
            return;
        }

        req.user = Format.dataForSynchronize(req.user);

        return next();
    }

    /**
     * create auth token
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     * @private
     */
    _authUser(req, res, next){
        // let user = Format.user(req.user);

        if (!req.user.email) {
            req.user.withoutEmail = true;
        }

        let user = req.user;

        let tokenParams = {
            createTime: Date.now(),
            id: user.id
        };
        
        user.token = jwt.sign(tokenParams, config.jwtKey);


        return res.send(user);
    }
}

module.exports = SignUpController;