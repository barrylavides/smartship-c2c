"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const _ = require('lodash');

class PostcodesController extends Controller {
    constructor(version) {
        super(version);
        this.getPostcode = [this._getPostcodeById];
        this.getPostcodes = [this._getPostcodes];
        this.softSearch = [this._softSearch];
    }

    /**
     * get postcode by id
     * @param req
     * @param res
     * @param next
     * @private
     */
    _getPostcodeById(req, res, next) {
        Models.postcodes.findById(req.params.id, {
            "include": [
                Models.cities
            ]
        }).then(function(postcode){
            if(postcode){
                res.send(postcode);
            }else{
                res.send({});
            }
        })
    }

    _getPostcodes(req, res, next) {
        res.send('!!!!');
    }

    /**
     * soft search on loke condition
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     * @private
     */
    _softSearch(req, res, next) {
        let searchConditions = {
            code: {
                $like: '%'+req.query.code+'%'
            }
        };

        let limit = _.isNaN(parseInt(req.query.limit)) ? 10 : parseInt(req.query.limit);
        let offset = _.isNaN(parseInt(req.query.offset)) ? 0 : parseInt(req.query.offset);

        Models.postcodes.findAndCountAll({
            include: [
                {
                    model: Models.cities,
                    include: [
                        {
                            model: Models.regions,
                            where: {country: 'TH'}
                        }
                    ]
                }
            ],
            where: searchConditions,
            limit: limit,
            offset: offset
        }).then(function(postcodes) {
            res.send(postcodes);
        });
    }
}

module.exports = PostcodesController;