
"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const Format = require('../../utils/format');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const constants = require('./../../constants.json');
const jsreport = require('jsreport');
const docHelperPdfTaxInvoice = require('../../services/pdfTax');
const aCommerceWrapper = require('./../../utils/aCommerceWrapper');
const request = require('request');
const http = require('http');


/**
 * PDF controller
 */
class PdfController extends Controller {
    constructor(version) {
        super(version);

        this.getTaxInvoicePDF = [this._generateTaxInvoicePDF];
        this.getShippingCardPDF = [this._checkOrder, this._getPackageOrdersLabel, this._savePackageOrdersLabel, this._getPDFBodyFromLabel]
    }

    _generateTaxInvoicePDF(req, res, next) {

        Models.orders.findById(req.params.id)
            .then(function (orderFind) {
                if (orderFind.status !== constants.ORDER_STATUS.DELIVERED) {
                    return {err: 'Order has not been delivered yet'}
                }
                return jsreport.render(docHelperPdfTaxInvoice(orderFind, req.user))
            })
            .then(function (out) {
                if (out.err) {
                    return next(new HTTPError(400, 'Order has not been delivered yet'))
                }

                res.writeHead(200, {
                    'Content-Type': 'application/pdf',
                    'Access-Control-Allow-Origin': '*',
                    'Content-Disposition': 'attachment; filename=' + req.params.id + '.pdf'
                });

                out.stream.pipe(res)
            })
            .catch(function (err) {
                return next(err)
            });
    }

    _checkOrder (req, res, next) {
        Models.orders.findById(req.params.id)
            .then(function (orderFind) {

                if (!orderFind) {
                    return next(new HTTPError(404, 'Order not found'))
                }

                if (orderFind.status === constants.ORDER_STATUS.CANCELLED) {
                    return next(new HTTPError(400, 'Order has been cancelled'))
                }

                req.packageOrderlabel = true;
                req.orderFind = orderFind;

                if (!orderFind.packageLabel) {
                    req.packageOrderlabel = false;
                    return next()
                }


                return next()
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _getPDFBodyFromLabel (req, res, next) {
        if (!req.packageOrderlabel) {
            return next(new HTTPError(400, 'Package label is incorrect'));
        }

        res.writeHead(200, {
            'Content-Type': 'application/pdf',
            'Access-Control-Allow-Origin': '*',
            'Content-Disposition': 'attachment; filename='+req.user.id + '-' +req.orderFind.id+'.pdf'
        });


        http.get(req.orderFind.packageLabel, function(response) {
            response.pipe(res)
        });
    }

    _getPackageOrdersLabel (req, res, next) {

        if (req.packageOrderlabel) {
            return next()
        }

        aCommerceWrapper.ordersAPIServices('/order/', 'get', false, req.orderFind.id, false, function (result) {
            result = JSON.parse(result);

            if (result.error) {
                let strErr = JSON.stringify(result.error);

                return next(new HTTPError(400, strErr));

            }

            if (!result.shipPackages[0].packageLabel) {
                return next(new HTTPError(400, 'Generating Shipping Label.... Try again later.'))
            }

            req.orderData = result;

            return next()

        })
    }

    _savePackageOrdersLabel (req, res, next) {

        if (req.packageOrderlabel) {
            return next()
        }

        req.orderFind.update({packageLabel: req.orderData.shipPackages[0].packageLabel})
            .then(function (updatedOrder) {
                req.packageOrderlabel = true;
                req.orderFind = updatedOrder;

                return next()
            })
            .catch(function (err) {
                return next(err)
            })
    }
}

module.exports = PdfController;