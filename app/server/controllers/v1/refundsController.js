"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const refundsFormat = require('./../../formatters/RefundsFormatter');
const filtersHelper = require('./../../utils/filtersHelper');
const _ = require('lodash');
const adminMessage = require('./../../utils/mailer/adminMessage');
const userMessage = require('./../../utils/mailer/userMessage');
const mailer = require('./../../utils/mailer/mailer');


/**
 * Refund controller
 */
class RefundsController extends Controller {

    constructor(version) {
        super(version);

        this.createRefund = [this.validator.refunds.create, this._addRefund];
        this.retrieveRefunds = [this._createRefundsCondition, this._getRefunds]
    }

    _addRefund (req, res, next) {

        let refundObj = {};

        if (req.user.availableAmount <= 0) {
            return next(new HTTPError(400, 'You can not refund if your balance equal or less than 0'));
        }

        Models.refunds.count({
            where:
            {
                userId: req.user.id
            }
        })
            .then(function (count) {

                let refundData = refundsFormat.createFormatter(req.user, count);

                return Models.refunds.create(refundData)
            })
            .then(function (refundCreated) {
                let confirmUser = new userMessage({}, refundCreated.referenceNo);
                refundObj.createdRefund = refundCreated;

                confirmUser = refundsFormat.sendUser(req.user, confirmUser);

                return mailer.send(confirmUser)
            })
            .then(function (userEmailSent) {
                console.log('User message', userEmailSent);
                let confirmAdmin = new adminMessage({}, refundObj.createdRefund.referenceNo);

                confirmAdmin = refundsFormat.sendAdmin(req.user, req.body, confirmAdmin);

                return mailer.send(confirmAdmin)
            })
            .then(function (adminEmailSent) {
                console.log('Admin message', adminEmailSent);

                return req.user.update({availableAmount: 0});
            })
            .then(function (userUpdated) {
                return res.send(refundObj.createdRefund)
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _createRefundsCondition (req, res, next) {
        let order = [];
        let orderCondition = ['id'];

        if(req.query.sort){
            orderCondition = [req.query.sort];
        }

        if(req.query.direction){
            orderCondition.push(req.query.direction);
        };

        order.push(orderCondition);
        console.log(order);
        req.order = order;
        next();
    }

    _getRefunds (req, res, next) {
        req.searchConditions = {
            userId: req.user.id
        };

        let limit = _.isNaN(parseInt(req.query.limit)) ? 10 : parseInt(req.query.limit);
        let offset = _.isNaN(parseInt(req.query.offset)) ? 0 : parseInt(req.query.offset);

        req.searchConditions = filtersHelper.refundsFilters(req.query, req.searchConditions);

        Models.refunds.findAndCountAll({
            where: req.searchConditions,
            order: req.order,
            limit: limit,
            offset: offset
        })
            .then(function (transactionFind) {
                return res.send(transactionFind)
            })
            .catch(function (err) {
                return next(err)
            })
    }

}

module.exports = RefundsController;