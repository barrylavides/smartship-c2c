"use strict";

const Controller = require('./../../utils/controller');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const constants = require('./../../constants.json');
const HTTPError = require('node-http-error');
const Format = require('../../formatters/PackageFormatter');
const request = require('request');
const paymentHelper = require('./../../utils/paymentHelper');
const Models = require('../../models/v1');
const _ = require('lodash');


/**
 * Package controller
 */
class PackagesController extends Controller {
    constructor(version) {
        super(version);

        this.createPackage = [this._buyNewPackage, this._startPaymentTransaction];
        this.getPackages = [this._getPackagesInfo];
        this.getPaymentCallback = [this._parsePaymentResponse, this._checkPaymentTransaction,
            this._finishPaymentTransaction, this._allowDocumentAccess];
        this.getBanks = [this._findPanBanks]
    }

    _buyNewPackage (req, res, next) {
        Models.packages.findById(req.body.packageId)
            .then(function (packageFind) {
                if (!packageFind) {
                    return next(new HTTPError(404, "Backend, it's not shown for now"));
                }

                req.packageFind = packageFind;
                return next();
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _startPaymentTransaction (req, res, next) {

        Models.transactions.create({userId: req.user.id, packageId: req.packageFind.id})
            .then(function (transactionBody) {
                let payload = paymentHelper.generatePaymentRequest(req.body, req.packageFind, req.user, transactionBody);

                return res.send({payload: payload});
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _parsePaymentResponse (req, res, next) {
        paymentHelper.decryptPaymentResponse(req.body.paymentResponse, function (err, result) {
            if (err) {
                console.log(err);
            }

            let redirectParams = "/index/shipping";

            if (result.PaymentResponse.userDefined1[0] !== '0') {
                redirectParams = result.PaymentResponse.userDefined1[0];
            }

            if (result.PaymentResponse.respCode[0] !== '00') {
                redirectParams = redirectParams + "?errors=" + result.PaymentResponse.failReason[0];

                // return res.redirect(redirectParams);
            }

            req.redirectParams = redirectParams;
            req.paymentResult = result.PaymentResponse;
            req.packageData = JSON.parse(result.PaymentResponse.userDefined3[0]);
            return next();
        })
    }

    _checkPaymentTransaction (req, res, next) {
        Models.transactions.findById(+req.paymentResult.userDefined2[0])
            .then(function (transactionFind) {
                if (!transactionFind) {
                    return next(new HTTPError(404, 'Transaction not found'));
                }

                req.transactionFind = transactionFind;
                return next();
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _finishPaymentTransaction (req, res, next) {
        let paymentResult = Format.finishTransaction(req.paymentResult, req.packageData);

        req.transactionFind.update(paymentResult)
            .then(function (transactionUpdated) {
                if (transactionUpdated.status !== config.PAYMENT_2C2P_DATA.TRANSACTION_STATUSES.A) {
                    return {err: true}
                }
                return Models.users.findById(req.transactionFind.userId);
            })
            .then(function (userFind) {
                if (userFind.err) {
                    return userFind;
                }
                let userData = Format.updateUserPackageData(userFind, req.packageData);

                return userFind.update(userData);
            })
            .then(function (userUpdated) {
                if (userUpdated.err) {
                    return res.redirect(req.redirectParams);
                }
                req.userUpdated = userUpdated;
                return next()
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _allowDocumentAccess (req, res, next) {
        Models.orders.update({documentAccess: 1},
            {
                where: {userId: req.userUpdated.id, documentAccess: 0}
            })
            .then(function (orderUpdated) {
                return res.redirect(req.redirectParams);
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _getPackagesInfo (req, res, next) {
        Models.packages.findAll()
            .then(function (packages) {
                let packageResult = Format.formatPackage(packages);
                return res.send(packageResult)
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _findPanBanks (req, res, next) {

        let limit = _.isNaN(parseInt(req.query.limit)) ? 10 : parseInt(req.query.limit);
        let offset = _.isNaN(parseInt(req.query.offset)) ? 0 : parseInt(req.query.offset);

        Models.panBanks.findAll({
            where:
            {
                name: {
                    $like: '%' + req.query.name + '%'
                }
            },
            limit: limit,
            offset: offset
        })
            .then(function (bankFind) {
                return res.send(bankFind)
            })
            .catch(function (err) {
                return next(err)
            })
    }
}

module.exports = PackagesController;