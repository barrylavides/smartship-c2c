"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const config = require('./../../../config');
const HTTPError = require('node-http-error');

/**
 * User controller
 */
class ChargesController extends Controller {
    constructor(version) {
        super(version);

        this.getChargesData = [this._getCharges]
    }

    _getCharges (req, res, next) {
        Models.rateValues.findAll({
            where: {
                serviceType: req.params.type
            }
        })
            .then(function (rates) {
                return res.send(rates)
            })
            .catch(function (err) {
                return next(err)
            })
    }
}

module.exports = ChargesController;