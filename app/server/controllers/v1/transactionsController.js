"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const filtersHelper = require('./../../utils/filtersHelper');
const crypto = require('crypto');
const request = require('request');
const _ = require('lodash');

/**
 * User controller
 */
class TransactionsController extends Controller {
    constructor(version) {
        super(version);

        this.getUsersTransactions = [this._createTransactionsCondition, this._getTransactionsData];
    }

    _createTransactionsCondition (req, res, next) {
        let order = [];
        let orderCondition = ['id'];

        if(req.query.sort){
            orderCondition = [req.query.sort];
        }

        if(req.query.direction){
            orderCondition.push(req.query.direction);
        };

        order.push(orderCondition);
        console.log(order);
        req.order = order;
        next();
    }

    _getTransactionsData (req, res, next) {

        req.searchConditions = {
            userId: req.user.id
        };

        let limit = _.isNaN(parseInt(req.query.limit)) ? 10 : parseInt(req.query.limit);
        let offset = _.isNaN(parseInt(req.query.offset)) ? 0 : parseInt(req.query.offset);

        req.searchConditions = filtersHelper.transactionsFilters(req.query, req.searchConditions);

        Models.transactions.findAndCountAll({
            where: req.searchConditions,
            order: req.order,
            limit: limit,
            offset: offset,
            include: [{model: Models.packages}]
        })
            .then(function (transactionFind) {
                return res.send(transactionFind)
            })
            .catch(function (err) {
                return next(err)
            })
    }
}

module.exports = TransactionsController;