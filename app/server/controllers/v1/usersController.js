"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const Format = require('./../../utils/format');
var reCaptcha = require('./../../utils/reCaptcha');
const crypto = require('crypto');
const request = require('request');
const mailer = require('./../../utils/mailer/mailer');
const registerMessage = require('./../../utils/mailer/registrationMessage');

/**
 * User controller
 */
class UsersController extends Controller {

    constructor(version) {
        super(version);
        this.patch = [this._checkUserExist, this._patchUser];
        this.get = [this._getOneUser];
        this.add = [reCaptcha, this._checkUser, this._createUser];
        this.registration = [this.validator.users.registration, this._checkPostcodeExist, this._checkCityExist,
            this._checkRegionExist, this._checkEmailUser, this._registerUser, this._sendEmailToUsers];
        this.authUser = [this._mainAuthUsers];
        this.changePassword = [this._checkValidToken, this._changeUserPass];
        this.getPostcode = [this._getPostcodeName];
        this.updateUser = [this.validator.users.registration, this._checkPostcodeExist, this._checkCityExist,
            this._checkRegionExist, this._updateUser, this._updateSocialEmail]
    }

    _getPostcodeName (req, res, next) {
        Models.postcodes.findById(req.params.id)
            .then(function (postcode) {
                return res.send(postcode)
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _checkValidToken (req, res, next) {
        Models.userTokens.findOne({
            where: {token: req.headers.authorization}
        })
            .then(function (tokenFind) {
                if (tokenFind) {
                    req.tokenFind = tokenFind;
                    return next()
                }

                return next(new HTTPError(400, 'Token not valid!'))
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _changeUserPass (req, res, next) {
        var tokenData = jwt.verify(req.headers.authorization, config.jwtKey);

        Models.users.findById(tokenData.id)
            .then(function (userFind) {

                let salt = userFind.generateSalt();

                let hashPass = userFind.encryptPass(req.body.password, salt);
                return userFind.update({password: hashPass, salt: salt});
            })
            .then(function (updated) {
                req.tokenFind.destroy()
                    .then(function (success) {
                        res.status(200);
                        return res.send({message: "Success"});
                    })
            })
            .catch(function (userFindErr) {
                return next(userFindErr)
            });
    }

    _mainAuthUsers (req, res, next) {
        Models.users.findOne({
            where: {
                email: req.body.email,
                role: 1
            },
            include:
                [{
                model: Models.socials
            }]})
            .then(function (user) {
                if (!user) {
                    return next(new HTTPError(404, 'User not found!'))
                }

                let validPass = user.verifyPassword(req.body.password);
                if (validPass) {

                    let result = Format.mainAuthUser(user);

                    let tokenParams = {
                        createTime: Date.now(),
                        id: user.id
                    };

                    result.token = jwt.sign(tokenParams, config.jwtKey);

                    return res.send(result);
                }

                return next(new HTTPError(400, 'Incorrect email or password!'))
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _checkUser (req, res, next) {
        Models.users.findOne({where: {email: req.body.email}})
            .then(function (userFind) {
                if (!userFind) {
                    return next()
                }

                return next(new HTTPError(400, 'This email already in use! Try another!'));
            })
            .catch(function (userError) {
                return next(userError)
            });
    }

    _createUser(req, res, next) {

        let createBody = Format.createUser(req.body);

        Models.users.create(createBody)
            .then(function (userCreate) {

                let result = Format.resAfterRegister(userCreate);

                let tokenParams = {
                    createTime: Date.now(),
                    id: userCreate.id
                };

                result.token = jwt.sign(tokenParams, config.jwtKey);

                return res.send(result)
            })
            .catch(function (userCreateError) {
                return next(userCreateError)
            });

    }


    /**
     * check city exist
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _checkCityExist(req, res, next){
        let cityId = req.body.cityId;
        Models.cities.findOne({
            where: {
                id: cityId
            }
        }).then(function (city) {
            if (!city) {
                let error = new Error();
                error.status = 422;
                error.message = 'city does not exist';
                return next(error);
            }
            next();
        }).catch(function (err) {
            next(err);
        })
    }

    /**
     * Get one user
     *
     * @param req
     * @param res
     * @param next
     *
     * @private
     */
    _getOneUser (req, res, next) {

        if (req.user.password) {
            req.user.dataValues.password = true
        }
        return res.send(req.user);
    }

    /**
     * check postcode exist
     *
     * @param {object}req
     * @param {object} res
     * @param {object} next
     *
     * @private
     */
    _checkPostcodeExist(req, res, next){

        req.body = Format.validateVatForRegister(req.body);

        if (req.body.err) {
            return next(new HTTPError(req.body.status, req.body.message));
        }

        let postcodeId = req.body.postcodeId;
        Models.postcodes.findOne({
            where: {
                id: postcodeId
            }
        }).then(function (postcode) {
            if (!postcode) {
                let error = new Error();
                error.status = 422;
                error.message = 'postcode does not exist';
                return next(error);
            }
            next();
        }).catch(function (err) {
            next(err);
        })
    }

    /**
     * check region exist
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _checkRegionExist(req, res, next){
        let regionId = req.body.regionId;
        Models.regions.findOne({
            where: {
                id: regionId
            }
        }).then(function (region) {
            if (!region) {
                let error = new Error();
                error.status = 422;
                error.message = 'region does not exist';
                return next(error);
            }
            next();
        }).catch(function (err) {
            next(err);
        })
    }

    /**
     * check user exist
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     */
    _checkUserExist(req, res, next) {
        let userId = req.params.id;
        Models.users.findOne({
            where: {
                id: userId
            },
            include: [{
                model: Models.socials
            }]
        }).then(function (user) {
            if (!user) {
                let error = new Error();
                error.status = 422;
                error.message = 'user does not exist';
                return next(error);
            }
            req.existUser = user;
            next();
        }).catch(function (err) {
            next(err);
        })
    }

    /**
     * update user
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _patchUser(req, res, next) {

        let updateBody = Format.patchUser(req.body, req.existUser);

        req.existUser.update(updateBody)
            .then(function (updatedUser) {
                let result = Format.resForPatch(updatedUser, req.existUser, updateBody);
                return res.send(result)
            })
            .catch(function (err) {
                return next(err)
            });
    }

    /**
     * update user profile and status
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _registerUser(req, res, next) {

        let user = Format.userSaveOptions(req.user, req.body);

        user.save().then(function () {
            if (user.password) {
                user.password = true;
            }

            user = Format.registerFormatter(user);

            req.registerUser = user;

            return next();

        }).catch(function (err) {
            next(err);
        });
    }

    _checkEmailUser (req, res, next) {
        if (!req.body.email) {
            return next()
        }

        if (req.body.email === req.user.email) {
            return next()
        }

        Models.users.findOne({where: {email: req.body.email} })
            .then(function (userFind) {
                if (!userFind) {
                    return next()
                }

                return next(new HTTPError(400, 'This email already in use! Try other!'));
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _updateUser(req, res, next) {

        delete req.body.email;
        let user = Format.userSaveOptions(req.user, req.body);

        user.save().then(function () {
            if (user.password) {
                user.password = true;
            }

            if (user.withoutEmail) {
                req.resUser = user;
                return next();
            }

            user = Format.registerFormatter(user);

            return res.send(user)

        }).catch(function (err) {
            next(err);
        });
    }

    _updateSocialEmail (req, res, next) {
        Models.socials.update({socialEmail: req.resUser.email}, {where: {userId: req.resUser.id}})
            .then(function (success) {
                return res.send(req.resUser)
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _sendEmailToUsers (req, res, next) {

        if (!req.registerUser) {
            return next()
        }

        let emailUsers = new registerMessage({}, req.registerUser.language);

        emailUsers = Format.sendUser(req.registerUser, emailUsers);

        mailer.send(emailUsers)
            .then(function (messageSent) {
                console.log(messageSent);
                return res.send(req.registerUser)
            })
            .catch(function (err) {
                return next(err)
            })

    }

}

module.exports = UsersController;
