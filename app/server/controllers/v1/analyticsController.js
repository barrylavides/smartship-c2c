'use strict';

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const rectangularAnalytics = require('../../models/repositories/analyticsRectangular');
const graphsAnalytics = require('../../models/repositories/analyticsGraphs');
const analyticsFormatter = require('../../formatters/AnalyticsFormatter');
const constants = require('../../constants.json');

class AnalyticsController extends Controller {
    constructor(version) {
        super(version);

        this.getRectangularAnalytics = [this._getOrdersStatistics];
        this.getGraphsAnalytics = [this._getDroppedOrdersGraphs, this._getDeliveredOrdersGraphs];
    }

    _getOrdersStatistics (req, res, next) {
        rectangularAnalytics.orders(req.user.id, function (result) {
            return res.send(analyticsFormatter.rectangularValues(result))
        })
    }

    _getDroppedOrdersGraphs (req, res, next) {
        graphsAnalytics.orders(req.user.id, constants.ORDER_STATUS.IN_TRANSIT, function (result) {
            req.droppedOrders = result;
            return next()
        })
    }

    _getDeliveredOrdersGraphs (req, res, next) {
        let mainResult = {};
        graphsAnalytics.orders(req.user.id, constants.ORDER_STATUS.DELIVERED, function (result) {
            mainResult.droppedOrders = req.droppedOrders;
            mainResult.deliveredOrders = result;

            return res.send(mainResult);
        })
    }
}




module.exports = AnalyticsController;