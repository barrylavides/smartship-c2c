"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const jwt = require('jsonwebtoken');
const config = require('./../../../config');
const HTTPError = require('node-http-error');
const Format = require('../../formatters/UserFormatter');
const uid = require('uid');
const emailHelper = require('./../../utils/emailHelper');

/**
 * Email controller
 */
class AdminsController extends Controller {

    constructor(version) {
        super(version);
        this.newPass = [this._preparingForSending, this._sendNewPassword];
    }

    _preparingForSending (req, res, next) {
        var password = uid(5);

        let details = {};

        Models.users.findOne({
            where: {email: req.body.email}
        })
            .then(function (user) {
                if (!user) {
                    return next(new HTTPError(404, 'User not found!'))
                }

                details.salt = user.generateSalt();

                details.hashPassword = user.encryptPass(password, details.salt);

                details.user = user;

                return user.update({password: details.hashPassword, salt: details.salt});
            })

            .then(function (success) {
                let emailOptions = Format.emailFormatter(details.user, password);

                emailHelper(emailOptions, function (token) {

                    req.emailToken = token;
                    req.emailUser = details.user;

                    return next();
                });

            })
            .catch(function (err) {
                return next(err)
            })

    }

    _sendNewPassword (req, res, next) {
        Models.userTokens.create({userId: req.emailUser.id, token: req.emailToken})
            .then(function (success) {
                return res.send({
                    message: "We've sent you a link to restore password! Please check your inbox!"
                })
            })
            .catch(function (err) {
                return next(err)
            })
    }


}

module.exports = AdminsController;
