"use strict";

const Controller = require('./../../utils/controller');
const Models = require('../../models/v1');
const _ = require('lodash');
const aCommerceHelper = require('./../../utils/aCommerceHelper');
const aCommerceWrapper = require('./../../utils/aCommerceWrapper');
const Format = require('../../formatters/OrderFormatter');
const PDFDocument = require ('pdfkit');
const webhookParser = require('./../../utils/webhookParser');
const docHelper = require('../../services/pdf');
const fs = require('fs');
const HTTPError = require('node-http-error');
const autoLogger = require('./../../utils/autoLogger');
const filtersHelper = require('./../../utils/filtersHelper');
const constants = require('./../../constants.json');
const json2csv = require('json2csv');
const csvHelper = require('./../../utils/csvHelper');
const multiparty = require('multiparty');
const fastCSV = require('fast-csv');
const bulkOrderHelper = require('./../../utils/bulkOrderHelper');

/**
 * Order class
 */
class OrdersController extends Controller {
    constructor(version) {
        super(version);
        this.create = [/*this.validator.orders.create,*/ this._createOrder, this._createOrderViaAcommerce, this._getTrackingIdForDropoff,
            this._giveOrderResponse, this._parseOrdersFile, this._checkPostcodeAndDistrict, this._bulkCreateOrders];
        this.getOrders = [this._createOrderCondition, this._getOrders, this._getChargeStatistics];
        this.getOneOrder = [this._getOneOrder];
        this.getAll = [this._createOrderCondition, this._getAllOrders];
        this.generatePDF = [this._generatePDF];
        this.getCSVOrders = [this.validator.orders.export, this._findFiltersOrders, this._findOrders, this._exportCSV];
        this.updateOrders = [this._updateOrders, this._getOrderWeight, this._getOrderData, this._getRateValue, this._decreaseUserBalance];
    }

    _findFiltersOrders (req, res, next) {
        if (Object.keys(req.query).length === 0) {
            return next()
        }
        let searchConditions = {
            userId: req.user.id
        };
        if (req.body.excludedIDs.length) {
            searchConditions.id = {$notIn: req.body.excludedIDs};
        }

        searchConditions = filtersHelper.ordersFilters(req.query, searchConditions);
        Models.orders.count({
            where: searchConditions
        })
            .then(function (count) {
                if (count > 10000) {
                    return next(new HTTPError(400, "There are too much orders in the table." +
                        " You can not download more than 10000 orders at once"))
                }

                return count;
            })
            .then(function (count) {
                return Models.orders.findAll({

                    attributes: constants.ORDER_ATTRIBUTES,
                    where: searchConditions,
                    order: [['createdAt', 'DESC']]

                })
            })
            .then(function (orders) {
                req.orderResult = orders;
                return next();
            }).catch(next);
    }

    _getOrderData (req, res, next) {
        let orderId = +req.body.shipOrderId || req.params.id;

        Models.orders.findById(orderId)
            .then(function (orderFind) {
                if (!orderFind) {
                    return next(new HTTPError(404, 'Order not found'))
                }

                req.orderFind = orderFind;
                return next()
            })
            .catch(next)
    }

    _getRateValue (req, res, next) {
        Models.rateValues.findOne({
            where: {
                minWeight: {
                    $lte: req.aCommerceOrderData.weight
                },
                maxWeight: {
                    $or: {
                        $gt: req.aCommerceOrderData.weight,
                        $eq: null
                    }
                },
                serviceType: req.orderFind.serviceType
            }
        })
            .then(function (rateFind) {
                if (!rateFind) {
                    return next(new HTTPError(404, 'Rate not found'))
                }

                req.rateFind = rateFind;
                return next();
            })
            .catch(next)
    }

    _decreaseUserBalance (req, res, next) {
        req.aCommerceOrderData.rate = Format.getRateValue(req.orderFind, req.rateFind);

        Models.users.findById(req.orderFind.userId)
            .then(function (userFind) {
                req.userFind = userFind;

                return req.orderFind.update(req.aCommerceOrderData)
            })
            .then(function (orderUpdated) {
                req.updateAmountObj = Format.updateAmountBalance(req.userFind, orderUpdated, req.orderUpdateBody);
                return req.userFind.update(req.updateAmountObj);
            })
            .then(function (userUpdated) {
                return res.send({message: "Success!"})
            })
            .catch(next)
    }


    _updateOrders (req, res, next) {

       let orderUpdateBody = webhookParser.acommerceWebhook(req.body);

        if (orderUpdateBody.status === constants.ORDER_STATUS.IN_TRANSIT
            || orderUpdateBody.status === constants.ORDER_STATUS.DELIVERED
            || orderUpdateBody.status === constants.ORDER_STATUS.READY_TO_SHIP) {

            req.orderUpdateBody = orderUpdateBody;
            return next()
        }

        Models.orders.update(orderUpdateBody, {where: {id: req.body.shipOrderId}})
            .then(function (success) {
                return res.send({message: "Success!"})
            })
            .catch(function (err) {
                return next(err)
            });
    }

    _getOrderWeight(req, res, next) {
        aCommerceWrapper.ordersAPIServices('/order/', 'get', false, req.body.shipOrderId,
            false, function (result) {
                let parseResult = {
                    result: result
                };

                if (result instanceof Array) {
                    parseResult.error = result[0].error || null;
                } else {
                    parseResult.error = result.error || null;
                }

                if (parseResult.error) {
                    let strErr = JSON.stringify(parseResult.error);
                    return next(new HTTPError(400, strErr));
                }

                if (typeof result !== 'object') {
                    result = JSON.parse(result);
                }
                req.aCommerceOrderData = Format.formatACommerceOrderData(result);
                req.aCommerceOrderData.status = req.orderUpdateBody.status;
                req.aCommerceOrderData.updatedAt = req.orderUpdateBody.updatedAt;
                return next();
            });
    }

    _generatePDF (req, res, next) {
        let doc = new PDFDocument();

        res.writeHead(200, {
            'Content-Type': 'application/pdf',
            'Access-Control-Allow-Origin': '*',
            'Content-Disposition': 'attachment; filename=' + 51 + '.pdf'
        });

        doc.pipe(res);

        docHelper(doc);

        doc.end();
    }

    _findOrders (req, res, next) {
        if (!req.body.orderIDs.length) {
            return next();
        }

        let where = {
            userId: req.user.id
        };
        if (req.body.orderIDs.length >= constants.MAX_ORDER_LENGTH) {
            return next(new HTTPError(400, `There are too much orders in the table.
                 You can not download more than ${constants.MAX_ORDER_LENGTH} orders at once`))
        }
        if (req.body.orderIDs.length) {
            where.id = {$in: req.body.orderIDs};
        }

        Models.orders.findAll({
            where: where,
            attributes: constants.ORDER_ATTRIBUTES
        })
            .then(function (ordersFind) {
                req.orderResult = ordersFind;
                return next()
            })
            .catch(next)
    }

    _exportCSV (req, res, next) {

        let dateNow = Math.floor(Date.now() / 1000);

        res.writeHead(200, {
            'Content-Type': 'application/csv',
            'Access-Control-Allow-Origin': '*',
            'Content-Disposition': 'attachment; filename='+dateNow+'.csv'
        });

        let csvData = csvHelper.resultFormatter(req.orderResult);

        let fields = csvData.fields;
        let mainResult = csvData.result;

        json2csv({data: mainResult, fields: fields}, function (err, csv) {
            if (err) return next(err);
            res.write(csv);

            res.end();
        })

    }



    /**
     * create sort for query
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _createOrderCondition(req, res, next) {
        let order = [];
        let orderCondition = ['id'];

        if(req.query.sort){
            orderCondition = [req.query.sort];
        }

        if(req.query.direction){
            orderCondition.push(req.query.direction);
        };

        order.push(orderCondition);
        console.log(order);
        req.order = order;
        next();
    }

    /**
     * create order api
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _createOrder(req, res, next) {

        let documentChange = false;

        if (req.user.availableCount === 0 || req.user.availableAmount <= 0) {
            return next(new HTTPError(400, 'You do not have shipping package for this action'));
        }

        if (req.user.availableAmount <= req.user.currentLowLimit) {
            documentChange = true;
        }

        if (req.query.fileOrders) {
            return next();
        }

        let orderOption = {};

        Models.cities.findOne(
            {
                where: {
                    id: req.user.cityId
                },
                include: [{model: Models.regions}]
            })
            .then(function (cityFind) {
                orderOption.city = cityFind;
                return Models.postcodes.findById(req.user.postcodeId)
            })
            .then(function (postcodeFind) {

                orderOption.postcode = postcodeFind;

                let createOrder = Format.createOrder(req.user, req.body, documentChange);
                
                return Models.orders.create(createOrder)
            })
            .then(function (orderCreate) {

                let options = Format.generateAcommerceData(req.user, orderOption.city, orderOption.postcode, req.body);

                let shippingData = aCommerceHelper.generateSaveOrderBody(options);

                req.shippingData = shippingData;
                req.orderData = orderCreate;

                return next();


            })
            .catch(function (cityErr) {
                return next(cityErr)
            })
    }

    _parseOrdersFile (req, res, next) {
        var form = new multiparty.Form();
        var mainResult = [];
        let errorsCollection = ['All fields are empty'];
        let errors = true;
        let count = 1;

        form.parse(req);

        form.on('close', function () {
            console.log('Upload completed!');
            console.log(errorsCollection)
        });

        form.on('error', function(err) {
            console.log('ERROR uploading the file:',err.message);
        });

        form.on('part', function(part) {

            if (!part.filename) {
                console.log('got field named ' + part.name);
                part.resume();
            }

            if (part.filename) {

                fastCSV
                    .fromStream(part, {headers: true})

                    .on('data', function (data) {
                        let bulkOrder = bulkOrderHelper.formatterBulkUpload(data, req.user, ++count);

                        bulkOrder.errors.forEach(function (errorField) {
                            errorsCollection.push(errorField)
                        });

                        errors = false;

                        mainResult.push(bulkOrder.order);
                    })
                    .on('end', function () {

                        if (mainResult.length > 20) {
                            errorsCollection.push('You can upload maximum 20 orders at once. Make your file shorter');
                        }

                        if (errorsCollection.length >= 1 && !errors) {
                            errorsCollection = errorsCollection.splice(1)
                        }

                        req.errorsCollection = errorsCollection;
                        req.mainResult = mainResult;

                        return next()
                    })
            }

            part.on('error', function(err) {
                return next(err)
            });
        })
    }

    _checkPostcodeAndDistrict (req, res, next) {

        let inputPostcodes = [];

        let postcodeFlag = false;

        req.mainResult.forEach(function (orderObj) {

            if (!orderObj.postcode) {
                postcodeFlag = true;
                req.errorsCollection.push('Postcode is invalid. Line: ' + orderObj.count)
            }
            inputPostcodes.push(orderObj.postcode);
        });

        if (postcodeFlag) {
            return next()
        }

        Models.postcodes.findAll({
            where: {
                code: {$in: inputPostcodes}
            },
            include: [
                {
                    model: Models.cities,
                    include: [
                        {
                            model: Models.regions,
                            where: {country: 'TH'}
                        }
                        ]
                }],
            attributes: ['id', 'code']
        })
            .then(function (postcodesFind) {

                let bulkOrder = bulkOrderHelper.postcodeAndDistrictValidation(postcodesFind, req.mainResult, req.errorsCollection);
                req.errorsCollection = bulkOrder.errorsCollection;

                if (req.errorsCollection.length !== 0) {
                    return next(new HTTPError(400, req.errorsCollection));
                }

                req.bulkOrders = bulkOrder.mainResult;
                return next()

            })
            .catch(function (err) {
                return next(err)
            });
    }

    _bulkCreateOrders (req, res, next) {

        if (req.errorsCollection.length !== 0) {
            return next(new HTTPError(400, req.errorsCollection));
        }

        Models.orders.bulkCreate(req.bulkOrders)
            .then(function (ordersCreate) {
                return res.send(ordersCreate)
            })
            .catch(function (err) {
                return next(err)
            })
    }

    _createOrderViaAcommerce (req, res, next) {

        if (req.query.fileOrders) {
            return next();
        }

        aCommerceWrapper.ordersAPIServices('/order/', 'put', req.shippingData, req.orderData.id, false, function (result) {
            let parseResult = {
                result: result
            };

            if (result instanceof Array) {
                parseResult.error = result[0].error || null;
            } else {
                parseResult.error = result.error || null
            }

            if (parseResult.error) {
                let strErr = JSON.stringify(parseResult.error);
                return next(new HTTPError(400, strErr));
            }

            if (req.user.availableAmount <= req.user.currentLowLimit) {
                req.changeCount = true;
                return next()
            }

            if (req.orderData.serviceType === constants.aCommerce.SERVICES.SMARTSHIP_DROPOFF) {
                req.serviceDropOff = true;
                return next()
            }

            return res.send(req.orderData)
        })
    }

    _getTrackingIdForDropoff (req, res, next) {

        if (!req.serviceDropOff) {
            return next();
        }

        aCommerceWrapper.ordersAPIServices('/order/', 'get', false, req.orderData.id,
            false, function (result) {
                let parseResult = {
                    result: result
                };

                if (result instanceof Array) {
                    parseResult.error = result[0].error || null;
                } else {
                    parseResult.error = result.error || null
                }

                if (parseResult.error) {
                    let strErr = JSON.stringify(parseResult.error);
                    return next(new HTTPError(400, strErr));

                }

                Models.orders.update({trackingId: result.acommerceId},
                    {
                        where:
                        {
                            id: req.orderData.id
                        }
                    })
                    .then(function (orderUpdated) {
                        return res.send(orderUpdated);
                    })
                    .catch(next)
            })
    }


    _giveOrderResponse (req, res, next) {
        if (!req.changeCount) {
            return next();
        }

        let currentAvailableCount = {};

        currentAvailableCount.availableCount = req.user.availableCount - 1;

        req.user.update(currentAvailableCount)
            .then(function (userUpdated) {
                return res.send(req.orderData)
            })
            .catch(function (err) {
                return next(err)
            })
    }

    /**
     * get orders
     *
     * @param {object} req
     * @param {object} res
     * @param {function} next
     *
     * @private
     */
    _getOrders(req, res, next){

        req.searchConditions = {
            userId: req.user.id
        };

        let limit = _.isNaN(parseInt(req.query.limit)) ? 10 : parseInt(req.query.limit);
        let offset = _.isNaN(parseInt(req.query.offset)) ? 0 : parseInt(req.query.offset);

        req.limitOrder = limit;
        req.offsetOrder = offset;

        if (req.query.charge) {
            return next()
        }

        req.searchConditions = filtersHelper.ordersFilters(req.query, req.searchConditions);

        Models.orders.findAndCountAll({
            where: req.searchConditions,
            order: req.order,
            limit: limit,
            offset: offset
        }).then(function(orders){
            res.send(orders);
        }).catch(function (err) {
            next(err);
        });
    }

    _getChargeStatistics (req, res, next) {

        req.searchConditions.status = {$in: [constants.ORDER_STATUS.IN_TRANSIT, constants.ORDER_STATUS.DELIVERED,
            constants.ORDER_STATUS.CANCELLED]
        };

        req.searchConditions = filtersHelper.chargesFilters(req.query, req.searchConditions);

        console.log(req.searchConditions);

        Models.orders.findAndCountAll({
            where: req.searchConditions,
            order: req.order,
            limit: req.limitOrder,
            offset: req.offsetOrder
        }).then(function(orders){
            res.send(orders);
        }).catch(function (err) {
            next(err);
        });
    }

    _getOneOrder(req, res, next) {
        let orderOptions = {};

        Models.orders.findOne({where: {id: req.params.id} })
            .then(function (orderVal) {
                if (!orderVal) {
                    res.status(404);
                    return res.send({message: "Order not found!"})
                }
                orderOptions.orderVal = orderVal;
                return Models.cities.findOne(
                    {
                        where: {
                            id: req.user.cityId
                        },
                        include: [{model: Models.regions}]
                    })
            })
            .then(function (cityFind) {
                orderOptions.cityFind = cityFind;
                return Models.postcodes.findById(req.user.postcodeId)
            })
            .then(function (postcodeFind) {
                let result = Format.formatOneOrder(orderOptions.orderVal, req.user, postcodeFind,
                    orderOptions.cityFind);

                return res.send(result);
            })
            .catch(function (errOrder) {
                return next(errOrder)
            })
    }

    _getAllOrders(req, res, next) {

        let limit = _.isNaN(parseInt(req.query.limit)) ? 10 : parseInt(req.query.limit);
        let offset = _.isNaN(parseInt(req.query.offset)) ? 0 : parseInt(req.query.offset);

        Models.orders.findAll({
            limit: limit,
            offset: offset,
            order: req.order
        })
            .then(function (ordersFind) {
                Models.orders.count()
                    .then(function (count) {
                        res.setHeader('X-Count', count);
                        return res.send(ordersFind)
                    })
            })
            .catch(function (err) {
                return next(err)
            })
    }
}

module.exports = OrdersController;