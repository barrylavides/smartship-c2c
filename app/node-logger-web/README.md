# Node-Logger-Web

This library adds a middleware Logger for express 4.x storing data in the database (or custom storage).

[TOC]

# Install
Clone package
```bash
cd $project-folder
git clone git@bitbucket.org:idsoutsource/node-logger-web.git
```

Add dependency to package.json for our project
```json
    "node-logger-web": "file:./node-logger-web"
```

Install
```bash
npm install
```

# Usage

Example use logger.
```js
var express = require('express');
var Logger = require('node-logger-web');
var app = express();

var storage = new Logger.MongooseStorage({ 
    url   : 'mongodb://localhost:27017/my-favorite-db', // database url,
    model : 'log',
    export: { 
        path: __dirname + '/logs/export/',  // url to export dir.
        time: '00 00 00 * * 1-5',           // export log information every day at 00:00 am o'clock.
        enableLogExport: true
    } 
});
var logger = Logger({ 
    fileName: 'logger.log', // path to log file (where logger save him error). 
    storage : storage,      
    enable  : true          // if true logger is enable, default value false.
}); 

app.use(logger.requestHandler);

app.get('/', function (req, res, next) {
  logger.info('Get root').then(function(){
    res.send('Hello World');
  });
});

app.use(logger.errorHandler);

app.listen(3000);
```


MySQL storage use mysql module.
```js
var dbConfig = { 
    host    : 'localhost',      // db host
    user    : USERNAME, 
    password: PASSWORD,  
    name    : "my-favorite-db"  // db name
};

var storage = new logger.MySqlStorage({ 
    config: dbConfig, 
    export: { 
        path: __dirname + '/../../logs/export/', 
        time: '00 00 00 * * 1-5'
    } 
});
```

MySQL storage use sequelize.
```js
var Sequelize = require('sequelize');
var sequelize = new Sequelize('mysql://user:pass@example.com:5432/dbname');

//some code...

var storage = new logger.SequelizeMySqlStorage({ 
    config: dbConfig,  // Config to db or
    //db: sequelize,   // sequelize instance
    export: { 
        path: __dirname + '/logs/export/', 
        time: '00 00 00 * * 1-5'
    } 
}); 
```

Mongo db storage use mongodb module.
```js
var storage = new logger.MongoStorage({ 
    url   : 'mongodb://localhost:27017/my-favorite-db', 
    model : 'log',
    export: { 
        path: __dirname + '/logs/export/',  // url to export dir.
        time: '00 00 00 * * 1-5'            // export log information every day at 00:00 am o'clock.
    }  
}); 
```

# Log Record Fields
Default logger record all field. 
Request path and the status of the response always saved. Record other field you can configure:
```js
var logger = Logger({ 
    properties: {
        type : true,
        start: true,    
        time : true
    },
    //...
});
```
|field    | Description               |
|---------|---------------------------|
|type     | type request (HTTP method)| 
|start    | timestamp create request  |    
|time     | request processing        |
|body     | request body              |
|response | response body             |
|error    | error stack trace         |
|headers  | request headers           |


# Documentation

## Logger([options]) ⇒ logger
Create Logger with options

**Kind**: global function  
**Returns**: `Object` logger - logger object. 

* logger - `Object`
    * [logger.errorHandler]()
    * [logger.requestHandler]()
    * [logger.log]()
    * [logger.info]()
    * [logger.warn]()
    * [logger.error]()
    
**Access:** public  

| Param | Type | Description |
| ----- | ---- | ----------- |
| [options] | `Object` |  |
| [options.storage] | `Object` | Log storage. |
| [options.fileName] | `Object` | The path to the log file (for logging logger exceptions). |
| [options.properties] | `Object` | Map with properties for logging. |
| [options.enable] | `Boolean` | Enable/disable value. |
| [options.isActive] | `Boolean` | Enable/disable value. |

* [Logger([options])](#markdown-header-loggeroptions) ⇒ `Object`
    * [Logger.SequelizeMySqlStorage](#Logger.SequelizeMySqlStorage)
    * [Logger.MySqlStorage](#Logger.MySqlStorage)
    * [Logger.MongoStorage](#Logger.MongoStorage)
    * [Logger.MongooseStorage](#Logger.MongooseStorage)


### logger.log(level, body) ⇒ promise

Manual logger

**Kind**: method of [logger](#markdown-header-logger)  
**Access:** public  

| Param | Type | Description |
| --- | --- | --- |
| [level]| `String` | Log level.|
| [body] | `String` | Log body. |

### logger.info(body) ⇒ promise

Info level manual log

**Kind**: method of [logger](#markdown-header-logger)  
**Access:** public  

| Param | Type | Description |
| --- | --- | --- |
| [body] | `String` | Log body. |

### logger.warn(body) ⇒ promise

See [logger.info]()


### logger.error(body) ⇒ promise

See [logger.info]()


### logger.requestHandler(req, res, next)

Request handling middleware for express.

**Kind**: method of [logger](#markdown-header-logger)  
**Access:** public  

### logger.errorHandler(req, res, next)

Error handling middleware for express.

**Kind**: method of [logger](#markdown-header-logger)  
**Access:** public  


### Logger.SequelizeMySqlStorage
Static property
SequelizeMySqlStorage - Logger adapter for sequelize

**Kind**: static property of [logger](#markdown-header-loggeroptions)  
**Access:** public  


### Logger.MySqlStorage
Static property
MySqlStorage - Logger adapter for mysql

**Kind**: static property of [logger](#markdown-header-loggeroptions)  
**Access:** public  


### Logger.MongoStorage
Static property
MongoStorage - Logger adapter for mongodb

**Kind**: static property of [logger](#markdown-header-loggeroptions)  
**Access:** public  


### Logger.MongooseStorage
Static property
MongooseStorage - Logger adapter for mongoose

**Kind**: static property of [logger](#markdown-header-loggeroptions)  
**Access:** public  


## Classes

### Storage ⇐ `EventEmitter`
**Kind**: global class  
**Extends:** `EventEmitter`  
**Access:** public  

* [Storage](#markdown-header-storage-eventemitter) ⇐ `EventEmitter`
    * [new Storage(options)](#new_Storage_new)
    * [.export(date)](#Storage+export)
    * [.throwIfError([err])](#Storage+throwIfError)

#### new Storage(options)
Storage interface.

| Param                                | Type               | Description   |
| ------------------------------------ | ------------------ | ------------- |
| options                              | `Object`           | options       |
| [options.export]                     | `Object`           | cron settings |
| [options.export.path]                | `String`           | export folder |
| [options.export.time]                | `String` or `Date` |  |
| [options.export.timeZone]            | `String`           |  |
| [options.export.enableLogExport] | `Boolean`          |  |

#### storage.export(date)
Export logs to a file and delete them from the table

**Kind**: instance method of [Storage](#markdown-header-storage-eventemitter)  
**this**: [Storage](#markdown-header-storage-eventemitter)  
**Access:** public  

| Param | Type |
| --- | --- |
| date | `Number` or `Date` | 

#### storage.throwIfError([err])
Triggering Event "error" if passed object exist.

**Kind**: instance method of [Storage](#markdown-header-storage-eventemitter)  

| Param | Type |
| --- | --- |
| [err] | `Error` or `String` or `Object` | 


### MongooseStorage ⇐ [Storage](#markdown-header-storage-eventemitter)
**Kind**: global class  
**Extends:** [Storage](#markdown-header-storage-eventemitter)  
**Access:** public  

* [MongooseStorage](#MongooseStorage) ⇐ [Storage](#markdown-header-storage-eventemitter)
    * [new MongooseStorage(options)](#new_MongooseStorage_new)
    * [.log(type, info)](#MongooseStorage+log)
    * [.export(date)](#MongooseStorage+export)
    * [.throwIfError([err])](#Storage+throwIfError)

#### new MongooseStorage(options)
Logger to the database by means of Mongodb

**Throws**:

- Will throw an error if the arguments `options.connection` and `options.url` is null.


| Param | Type | Description |
| --- | --- | --- |
| options | `Object` |  |
| [options.url] | `String` |  |
| [options.model] | `String` | table name |
| [options.export] | `Object` | cron settings |
| [options.export.path] | `String` | backup folder |
| [options.export.time] | `String` or `Date` | cron time property |
| [options.export.timeZone] | `String` | cron time zone property |
| [options.export.enableLogExport] | `Boolean`          |  |

#### mongooseStorage.log(type, info)
Logging information into a database by means of MySQL

**Kind**: instance method of [MongooseStorage](#MongooseStorage)  
**this**: [MongooseStorage](#MongooseStorage)  
**Access:** public  

| Param | Type | Description |
| --- | --- | --- |
| type | `Object` | type log information |
| info | `Object` | log information |

#### mongooseStorage.export(date)
Export logs to a file and delete them from the table

**Kind**: instance method of [MongooseStorage](#MongooseStorage)  
**this**: [MongooseStorage](#MongooseStorage)  
**Access:** public  

| Param | Type |
| --- | --- |
| date | `Number` or `Date` | 

#### mongooseStorage.throwIfError([err])
Triggering Event "error" if passed object exist.

**Kind**: instance method of [MongooseStorage](#MongooseStorage)  

| Param | Type |
| --- | --- |
| [err] | `Error` or `String` or `Object` | 

### MongoStorage ⇐ [Storage](#markdown-header-storage-eventemitter)
**Kind**: global class  
**Extends:** [Storage](#markdown-header-storage-eventemitter)  
**Access:** public  
	
* [MongoStorage](#MongoStorage) ⇐ [Storage](#markdown-header-storage-eventemitter)
	* [new MongoStorage(options)](#new_MongoStorage_new)
    * [.log(type, info)](#MongoStorage+log)
    * [.export(date)](#MongoStorage+export)
    * [.throwIfError([err])](#Storage+throwIfError)

####  new MongoStorage(options)
Logger to the database by means of Mongodb

**Throws**:

- Will throw an error if the arguments `options.url` is null.


| Param | Type | Description |
| --- | --- | --- |
| options | `Object` |  |
| options.url | `String` |  |
| [options.model] | `String` | table name |
| [options.export] | `Object` | cron settings |
| [options.export.path] | `String` | backup folder |
| [options.export.time] | `String` or `Date` | cron time property |
| [options.export.timeZone] | `String` | cron time zone property |
| [options.export.enableLogExport] | `Boolean`          |  |


#### mongoStorage.addLog(type, info)
Logging information into a database by means of MySQL

**Kind**: instance method of [MongoStorage](#MongoStorage)  
**this**: [MongoStorage](#MongoStorage)  
**Access:** public  

| Param | Type | Description |
| --- | --- | --- |
| type | `Object` | type log information |
| info | `Object` | log information |

#### mongoStorage.export(date)
Export logs to a file and delete them from the table

**Kind**: instance method of [MongoStorage](#MongoStorage)  
**this**: [MongoStorage](#MongoStorage)  
**Access:** public  

| Param | Type |
| --- | --- |
| date | `Number` or `Date` | 

#### mongoStorage.throwIfError([err])
Triggering Event "error" if passed object exist.

**Kind**: instance method of [MongoStorage](#MongoStorage)  

| Param | Type |
| --- | --- |
| [err] | `Error` or `String` or `Object` | 

### MySqlStorage ⇐ [Storage](#markdown-header-storage-eventemitter)
**Kind**: global class  
**Extends:** [Storage](#markdown-header-storage-eventemitter)  
**Access:** public  

* [MySqlStorage](#MySqlStorage) ⇐ [Storage](#markdown-header-storage-eventemitter)
    * [new MySqlStorage(options)](#new_MySqlStorage_new)
    * [.addLog(type, info)](#MySqlStorage+addLog)
    * [.export(date)](#MySqlStorage+export)
    * [.throwIfError([err])](#Storage+throwIfError)

#### new MySqlStorage(options)
Logger to the database by means of MySQL

**Throws**:

- Will throw an error if the arguments `options.config` is null.


| Param | Type | Description |
| --- | --- | --- |
| options | `Object` |  |
| options.config | `Object` | database config |
| [options.model] | `String` | table name |
| [options.export] | `Object` | cron settings |
| [options.export.path] | `String` | backup folder |
| [options.export.time] | `String` or `Date` | cron time property |
| [options.export.timeZone] | `String` | cron time zone property |
| [options.export.enableLogExport] | `Boolean`          |  |

#### mySqlStorage.addLog(type, info)
Logging information into a database by means of MySQL

**Kind**: instance method of [MySqlStorage](#MySqlStorage)  
**this**: [MySqlStorage](#MySqlStorage)  
**Access:** public  

| Param | Type | Description |
| --- | --- | --- |
| type | `Object` | type log information |
| info | `Object` | log information |

#### mySqlStorage.export(date)
Export logs to a file and delete them from the table

**Kind**: instance method of [MySqlStorage](#MySqlStorage)  
**this**: [MySqlStorage](#MySqlStorage)  
**Access:** public  

| Param | Type |
| --- | --- |
| date | `Number` or `Date` | 

#### mySqlStorage.throwIfError([err])
Triggering Event "error" if passed object exist.

**Kind**: instance method of [MySqlStorage](#MySqlStorage)  

| Param | Type |
| --- | --- |
| [err] | `Error` or `String` or `Object` | 

### SequelizeMySqlStorage ⇐ [Storage](#markdown-header-storage-eventemitter)
**Kind**: global class  
**Extends:** [Storage](#markdown-header-storage-eventemitter)  
**Access:** public  

* [SequelizeMySqlStorage](#SequelizeMySqlStorage) ⇐ [Storage](#markdown-header-storage-eventemitter)
    * [new SequelizeMySqlStorage(options)](#new_SequelizeMySqlStorage_new)
    * [.addLog(type, info)](#SequelizeMySqlStorage+addLog)
    * [.export(date)](#SequelizeMySqlStorage+export)
    * [.throwIfError([err])](#Storage+throwIfError)

#### new SequelizeMySqlStorage(options)
Logger to the database by means of Sequelize (MySQL)

**Throws**:

- Will throw an error if the arguments `options.db` and `options.config` is null.


| Param | Type | Description |
| --- | --- | --- |
| options | `Object` |  |
| [options.db] | `Object` | sequelize instance |
| [options.config] | `Object` | database config |
| [options.model] | `String` | table name |
| [options.export] | `Object` | cron settings |
| [options.export.path] | `String` | backup folder |
| [options.export.time] | `String` or `Date` | cron time property |
| [options.export.timeZone] | `String` | cron time zone property |
| [options.export.enableLogExport] | `Boolean`          |  |

#### sequelizeMySqlStorage.addLog(type, info)
Logging information into a database by means of Sequelize (MySQL)

**Kind**: instance method of [SequelizeMySqlStorage](#SequelizeMySqlStorage)  
**this**: [SequelizeMySqlStorage](#SequelizeMySqlStorage)  
**Access:** public  

| Param | Type | Description |
| --- | --- | --- |
| type | `Object` | type log information |
| info | `Object` | log information |

#### sequelizeMySqlStorage.export(date)
Export logs to a file and delete them from the table

**Kind**: instance method of [SequelizeMySqlStorage](#SequelizeMySqlStorage)  
**this**: [SequelizeMySqlStorage](#SequelizeMySqlStorage)  
**Access:** public  

| Param | Type |
| --- | --- |
| date | `Number` or `Date` | 

#### sequelizeMySqlStorage.throwIfError([err])
Triggering Event "error" if passed object exist.

**Kind**: instance method of [SequelizeMySqlStorage](#SequelizeMySqlStorage)  

| Param | Type |
| --- | --- |
| [err] | `Error` or `String` or `Object` | 
