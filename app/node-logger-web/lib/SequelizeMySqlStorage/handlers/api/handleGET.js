var MODEL_ATTRIBUTES = require('./../../../constants.json').MODEL_ATTRIBUTES;


module.exports = function (storage, req, res, next) {
    var sequelizeQuery = formatQuery(req.query);

    storage._logModel.findAndCountAll(sequelizeQuery)
        .then(function(result) {
            res.setHeader('X-Count', result.count);
            res.send(result.rows);
        })
        .catch(function(err) {
            err.status = 400;
            next(err);
        });
}

function formatQuery(queryBody) {
    var title;

    var result = {
        limit: parseInt(queryBody.limit, 10) || 100,
        offset: parseInt(queryBody.offset, 10) || 0,
        order: [[(queryBody.sort || 'date'), (queryBody.dir || 'DESC')]],
        where: {}
    }

    for (var i = 0; i < MODEL_ATTRIBUTES.length; i++) {
        title = MODEL_ATTRIBUTES[i];

        if (queryBody[title] !== undefined && queryBody[title] !== '') {
            result.where[title] = {
                $like: queryBody[title]
            }
        }
    }

    return result;
}