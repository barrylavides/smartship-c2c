
module.exports = function (storage, req, res, next) {
    var query = { where: { id: req.params.id }};

    storage._logModel.destroy(query)
        .then(function(result) {
            res.send({result: result});
        })
        .catch(function(err) {
            err.status = 400;
            next(err);
        });

}
