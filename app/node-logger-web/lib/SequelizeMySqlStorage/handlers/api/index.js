var handleGET = require('./handleGET');
var handleDELETE = require('./handleDELETE');

module.exports = {
    handleGET: handleGET,
    handleDELETE: handleDELETE
}