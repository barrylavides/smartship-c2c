'use strict';

/**
 * Module dependencies.
 * @private
 */
var MongoClient = require('mongodb').MongoClient;
var uuid = require('uuid');
var util = require('util');
var async = require('async');
var fs = require('fs');
var Promise = require('bluebird');
var Storage = require('./Storage');
var storageUtil = require('./utils/storageUtil');
var csvExport = require('./utils/csvExport');
var constants = require('./constants.json');


/**
 * Logger storage by means of Mongodb
 *
 * @constructor MongoStorage
 * @extends Storage
 *
 * @public
 *
 * @param {Object} options
 * @param {String} options.url
 * @param {String} [options.model] table name
 * @param {Object} [options.export] cron settings
 * @param {String} [options.export.path] backup folder
 * @param {String|Date} [options.export.time] cron time property
 * @param {String} [options.export.timeZone] cron time zone property
 * @param {Boolean} [options.export.enableAutoLogExport]
 * @param {Boolean} [options.export.enableHandLogExport]
 *
 * @throws  Will throw an error if the arguments `options.url` is null.
 */
function MongoStorage(options) {
    if (!options) {
        throw new Error('Invalid options');
    }

    Storage.apply(this, arguments);
    if (options.url) {
        this.url = options.url;
    } else {
        throw new Error('Invalid URL value');
    }

    this._logsModelName = options.model || constants.DEFAULT_TABLE_NAME;
    this._db = null;
    var self = this;
    MongoClient.connect(this.url, function (err, db) {
        if (err) return self.throwIfError(err);
        self._db = db;
    });
}

util.inherits(MongoStorage, Storage);

/**
 * Logging request information into a database by means of mongodb
 *
 * @public
 *
 * @this MongoStorage
 *
 * @param {Object} level - level log information
 * @param {Object} info - log information
 */
MongoStorage.prototype.addRequestLog = function (level, info) {
    info.level = level;
    info.date = Date.now();
    info.logType = constants.logtypes.AUTOLOG;
    var self = this;

    write(info, self);
    function write(info, self) {
        if (self._db) {
            return self._db.collection(self._logsModelName)
                .insert(info, {w: 1}, function (err, result) {
                    self.throwIfError(err);
                });
        }

        process.nextTick(() => {
            write(info, self);
        });
    }
};

MongoStorage.prototype.updateRequestLog = function (level, info) {
    info.level = level;
    info.date = Date.now();
    var self = this;

    write(info, self);
    function write(info, self) {
        if (self._db) {
            return self._db.collection(self._logsModelName)
                .updateOne({id: info.id}, info, function (err, result) {
                    self.throwIfError(err);
                });
        }

        process.nextTick(() => {
            write(info, self);
        });
    }
};

/**
 * Hand-log in database by means of mongodb
 *
 * @param {String} level
 * @param {String} value
 * @returns {Promise}
 */
MongoStorage.prototype.log = function (level, value) {
    var self = this;
    value = storageUtil.manualLogFormatter.apply(null, arguments);

    return new Promise(function (fulfill, reject) {
        var date = new Date();
        var log = {
            id    : uuid.v1(),
            level  : level,
            date   : date.getTime(),
            value  : value,
            logType: constants.logtypes.HANDLOG
        };

        write(log, self);
        function write(log, self) {
            if (self._db) {
                return self._db.collection(self._logsModelName)
                    .insert(log, {w: 1}, function (err, result) {
                        if (err) {
                            return reject(err);
                        }

                        fulfill(result.ops[0]);
                    });
            }

            setTimeout(() => {
                console.warn('nextTick');
                write(log, self);
            }, 10);
        }
    });
};

/**
 * Export logs to a file and delete them from the table
 *
 * @private
 *
 * @this MongoStorage
 *
 * @param {Number|Date} date
 */
MongoStorage.prototype.export = function (date) {
    var self = this;
    if (date instanceof Date) {
        date = date.getTime();
    }

    if (typeof date != 'number') {
        throw new TypeError('The expected value of Number');
    }

    if (!this.exportPath) {
        throw new Error('Invalid export path');
    }

    if(!this._db) {
        return setTimeout(() => {
            self.export(date);
        }, 10)
    }

    var name = this._logsModelName;
    var db = this._db;

    var where = {
        date: {$lte: date}
    };

    db
        .collection(name)
        .count(where, function (err, count) {
            if (err) {
                return self.throwIfError(err);
            }
            if (!count) return;

            var filename = `${self.exportPath}${name}_${date}.${csvExport.type}`;
            var stream = fs.createWriteStream(filename, {
                flags: 'a'
            });
            var iterationCount = Math.ceil(count / constants.DEFAULT_EXPORT_LIMIT);
            var exportAdapter = csvExport.exportAsync(stream);
            async.timesSeries(iterationCount, exportData(db, name, where, exportAdapter), function (err) {
                self.throwIfError(err);
                stream.close();
            });
        });
};

function exportData(db, name, where, exportAdapter) {
    return function (iteration, next) {
        var ids = [];
        async.waterfall([
            _getLegacyLog,
            _saveLogInFile,
            _deleteLegacyLog
        ], next);

        function _getLegacyLog(cb) {
            db.collection(name)
                .find(where)
                .limit(constants.DEFAULT_EXPORT_LIMIT)
                .sort([['date', 1]])
                .toArray(cb);
        }

        function _saveLogInFile(logs, cb) {
            ids = storageUtil.logsToIds(logs);
            exportAdapter(logs, cb);
        }

        function _deleteLegacyLog(cb) {
            db.collection(name)
                .remove({id: {$in: ids}}, {w: 1}, cb);
        }
    }
}

/**
 * Module exports.
 * @public
 */
module.exports = MongoStorage;
