'use strict';

/**
 * Module dependencies.
 * @private
 */
var CronJob = require('cron').CronJob;
var util = require('util');
var async = require('async');
var EventEmitter = require('events');

/**
 * Storage interface.
 *
 * @constructor Storage
 * @extends EventEmitter
 *
 * @public
 *
 * @param {Object} options
 * @param {Object} [options.export] cron settings
 * @param {String} [options.export.path] backup folder
 * @param {String|Date} [options.export.time]
 * @param {String} [options.export.timeZone]
 * @param {Boolean} [options.export.enableLogExport]
 */
function Storage(options) {
    if (!options) {
        throw new Error('Invalid options');
    }

    if (options.export) {
        var cron = {};

        this.exportPath = options.export.path || '/logs/export/';
        this._enableExport = options.export.enableLogExport || false;

        cron.time = options.export.time || '00 00 00 * * *';
        cron.timeZone = options.export.timeZone;
        cronSetup.call(this, cron);
    }
}
util.inherits(Storage, EventEmitter);

/**
 * Triggering Event "error" if passed object exist.
 *
 * @param {Error|String|Object} [err]
 */
Storage.prototype.throwIfError = function (err) {
    if (err) {
        this.emit('error', err);
    }
};

/**
 * Export logs to a file and delete them from the table
 *
 * @public
 *
 * @this Storage
 *
 * @param {Number|Date} date
 */
Storage.prototype._export = function (date) {
    if (date instanceof Date) {
        date = date.getTime();
    }

    if (typeof date != 'number') {
        throw new TypeError('The expected value of Number');
    }

    if (!this.exportPath) {
        throw new Error('Invalid export path');
    }

    if (this._enableExport) {
        console.warn('export');
        this.export(date);
    }
};

Storage.prototype.export = function (date) {
    throw new Error('Not implements!');
};

/**
 * Setup cron
 *
 * @private
 *
 * @param {Object} settings logger
 * @param {String|Date} [settings.time] cron settings
 * @param {String} [settings.timeZone] cron settings
 */
var cronSetup = function (settings) {
    var self = this;

    this.cron = new CronJob({
        cronTime: settings.time,
        onTick  : function () {
            try {
                if (self._export && typeof self._export == 'function') {
                    self._export(new Date().getTime());
                }
            } catch (err) {
                self.emit('error', err);
            }
        },
        timeZone: settings.timeZone
    });
    this.cron.start();
};

module.exports = Storage;