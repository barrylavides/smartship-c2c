var json2csv = require('json2csv');
var constants = require('../constants.json');

function exportAdapter(stream) {
    var column = 0;
    return function (logs, cb) {
        json2csv({data: logs, fields: constants.MODEL_ATTRIBUTES, hasCSVColumnTitle: column < 1}, function (err, csv) {
            if (err) return cb(err);
            column++;
            csv += '\r\n';
            stream.write(csv, 'utf8', cb);
        });
    }
}

module.exports = {
    exportAsync: exportAdapter,
    type       : 'csv'
};