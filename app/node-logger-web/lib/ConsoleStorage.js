'use strict';

/**
 * Module dependencies.
 * @private
 */
var Promise = require('bluebird');
var storageUtil = require('./utils/storageUtil');
var constants = require('./constants.json');

/**
 * Logger storage by means of console
 *
 * @constructor ConsoleStorage
 *
 * @public
 *
 * @param {Object} options
 */
function ConsoleStorage() {}

/**
 * Logging request information into a database by means of console
 *
 * @public
 *
 * @this SequelizeMySqlStorage
 *
 * @param {Object} level - type log information
 * @param {Object} requestLog - log information
 */
ConsoleStorage.prototype.addRequestLog = function (level, requestLog) {
    requestLog = storageUtil.logToString(requestLog);
    level = level == 'info'  ? level : 'error';
    console[level](new Date(), level, requestLog);
};

ConsoleStorage.prototype.updateRequestLog = function (level, responseLog) {
    responseLog = storageUtil.logToString(responseLog);
    level = level == 'info'  ? level : 'error';
    console[level](new Date(), level, responseLog);
};

/**
 * Hand-log in database by means of console
 *
 * @param {String} level
 * @param {String} value
 * @returns {Promise}
 */
ConsoleStorage.prototype.log = function (level, value) {
    var message = new Date() + ' ' + level + ': ';
    var log = storageUtil.manualLogFormatter.apply(null, arguments);
    return new Promise(function (resolve, reject) {
        if(console[level]) {
            console[level](message, log);
            return resolve(log);
        }

        console.log(message, log);
        resolve(log);
    });
};

/**
 * Module exports.
 * @public
 */
module.exports = ConsoleStorage;