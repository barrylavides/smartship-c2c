/**
 * Levels list.
 */
var levels = require('./constants.json').LOG_LEVELS;

/**
 * Mapping log level methods in manual logging
 * @param target
 */
function setLevels (target){
    levels.forEach(function (level){
        target[level] = function () {
            var args = [level].concat(Array.prototype.slice.call(arguments));
            return target.log.apply(target, args);
        }
    })
}

module.exports = setLevels;