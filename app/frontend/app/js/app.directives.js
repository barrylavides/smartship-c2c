/**
 *
 * Pass all functions into module
 */
angular
    .module('smartShip')
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    .directive('onlyNumbers', onlyNumbers)
    .directive('onlyDecimalNumbers', onlyDecimalNumbers)
    .directive('onlyText', onlyText)
    .directive('formEvents', formEvents)
    .directive('affixHeader', affixHeader)
    .directive('limitTo', limitTo)
    .directive('backTo', backTo)
    .directive('datePicker', datePicker)
    .directive('labelDegree', labelDegree)
    .directive('monthYear', monthYear)
    .directive('expirationDate', expirationDate)
    .directive('hoverIsActive', hoverIsActive)
    .directive('historyStepBack', historyStepBack);

/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function(scope, element) {
            var listener = function(event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'smartShip';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'smartShip | ' + toState.data.pageTitle;
                $timeout(function() {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
}

function onlyNumbers() {
    return {
        require: "ngModel",
        restrict: "A",
        link: function(scope, elem, attrs, ngModelCtrl) {
            angular.element(elem).bind("keyup keypress input paste change", function(e) {
                var charCode = (e.which) ? e.which : e.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    // e.preventDefault();
                    return false;
                }
            });
        }
    }
}

function onlyDecimalNumbers() {
    return {
        require: "ngModel",
        restrict: "A",
        scope: {
            onlyDecimalNumbers: '='
        },
        link: function(scope, elem, attrs, ngModelCtrl) {

            var regExp = /[^[0-9\. +]]*/gi;

            angular.element(elem).bind("keydown input", function(e) {
                var transformedInput;

                var maxlength = Number(attrs.maxLeng);
                function fromUser(text) {
                    if(regExp.test(text)) {
                        transformedInput = text.replace(regExp, '');
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }

                    if(/\./.test(text) && text.length >= maxlength) {
                        transformedInput = text.substring(0, maxlength + 1);
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                        return transformedInput;
                    } else if (text.length > maxlength) {
                        transformedInput = text.substring(0, maxlength);
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                        return transformedInput;
                    }
                    return text;
                }
                ngModelCtrl.$parsers.push(fromUser);

                // var val = elem.val();
                // var regex = /[^[0-9\. +]]*/gi;
                // var regex = /^\d+(\.\d{1,2})?$/;
                // var regex = /^[\d+(\.\d{1,2})?$]*/gi;

                // var newval = val.replace(regex, '');
                // elem.val(newval)
                // console.log(regex.test(val));
                // if (/^\d+(\.\d{1,2})?$/.test(val)) {
                //
                //     // console.log('newval', newval);
                //     // elem.val(newval);
                //     e.preventDefault();
                // }
            });
        }
    }
}

function limitTo() {
    return {
        require: "ngModel",
        restrict: "A",
        link: function(scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).bind("input change paste keyup mouseup", function(e) {
                if (this.value.length >= limit) {
                    this.value = this.value.substr(0, limit);
                    return false;
                }
            });
        }
    }
}

function onlyText() {
    return {
        require: "ngModel",
        restrict: "A",
        link: function(scope, elem, attrs) {
            angular.element(elem).bind("keypress input", function(e) {
                var charCode = (e.charCode) ? e.charCode : ((e.keyCode) ? e.keyCode :
                    ((e.which) ? e.which : 0));
                if (charCode > 31 && (charCode < 65 || charCode > 90) &&
                    (charCode < 97 || charCode > 122)) {
                    e.preventDefault();
                }
            });
        }
    }
}



/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function(){
                element.metisMenu();
            });
        }
    };
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            }
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function() {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

function formEvents() {
    return {
        restrict: 'A',
        link: function(scope, el, attr) {
            angular.element(document).on('blur', '#postcode', function () {
                scope.$apply(scope.postcodeSelected(scope.postcode));
            });

            angular.element(document).on('keypress', '#editForm, #registrationForm', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

            angular.element(document).on('blur', '#phone', function () {
                if (scope.registerUser.phone) {
                    var correctPhone = scope.registerUser.phone.replace(/[^0-9.]/g, "");
                    scope.registerUser.phone = correctPhone;
                }
            });
        }
    }
}

function affixHeader($window) {
    return {
        restrict: 'A',
        link: function(scope, el, attr) {
            var nav = angular.element('#mainNav'),
                navHeight = nav[0].offsetHeight,
                affixTop = 'affix-top',
                affix = 'affix';

            angular.element($window).bind('scroll', function() {
                if(angular.element($window)[0].pageYOffset >= navHeight) {
                    nav.removeClass(affixTop).addClass(affix);
                } else {
                    nav.removeClass(affix).addClass(affixTop);
                }
            });
        }
    }
}

function backTo() {
    return {
        restrict: 'A',
        scope: {
            backTo: '@'
        },
        link: function(scope, el) {
            var anchorElement = angular.element(document.getElementById(scope.backTo));
            var anchorOffsetTop = anchorElement[0].offsetTop;

            angular.element(el).on('click', function(e){
                e.preventDefault();
                angular.element('hrml, body').animate({scrollTop: anchorOffsetTop}, 600);
            });
        }
    }
}

function datePicker($filter) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            datePicker: '@'
        },
        link: function (scope, element, attrs, ngModelCtrl) {
            angular.element(element).datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: function (date) {
                    var ar = date.split("/");
                    date = new Date(ar[2] + "-" + ar[1] + "-" + ar[0]);

                    if(scope.datePicker) {
                        if(scope.datePicker === 'endDate') {
                            date = new Date(date.getTime());
                            date.setDate(date.getDate() + 1);
                        } else {
                            date = new Date(date.getUTCFullYear(), date.getUTCMonth(),
                                date.getUTCDate(),  date.getUTCHours(),
                                date.getUTCMinutes(), date.getUTCSeconds(),
                                date.getUTCMilliseconds());
                        }
                    }

                    ngModelCtrl.$setViewValue(date.getTime()/1000);
                    scope.$apply();
                }
            });
            ngModelCtrl.$formatters.unshift(function (v) {
                return $filter('date')(v, 'dd/MM/yyyy');
            });

        }
    }
}

function labelDegree() {
    return {
        restrict: 'A',
        scope: {
            labelDegree: '&'
        },
        link: function(scope) {
            var winWidth;
            var defWindth = 1855;

            scope.labelDegree({
                value: getDegreePercent(window.innerWidth)
            });

            angular.element(window).bind('resize', function() {
                winWidth = window.innerWidth;
                scope.$apply(function() {
                    scope.labelDegree({
                        value: getDegreePercent(winWidth)
                    });
                });
            });

            function getDegreePercent(width) {
                return (Math.abs(width/defWindth) * 100);
            }
        }

    }
}

function monthYear() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, el, attrs, ngModelCtrl) {
            angular.element(el).bind("keyup keypress", function(e) {

                var backspace = e.which;
                var val = ngModelCtrl.$viewValue;

                var regExp = /(\d{2})(\d{4})/;
                // while (regExp.test(val)) val = val.replace(regExp, '$1'+'/'+'$2');

                if(/[\/]/.test(val)){
                    return val;
                } else if (!/[\/]/.test(val) && backspace === 8) {
                    return val;
                } else {
                    if(!val) return;
                    val = val.replace(/\d{2}/, function(match, i, s){
                        return match + '/' + s.substr(2);
                    });
                }

                ngModelCtrl.$setViewValue(val);
                ngModelCtrl.$render();
            })
        }
    }
}

function expirationDate() {
    return {
        restrict: 'AC',
        link: function (scope, el) {
            var expDate = angular.element(el).find('#expiration_data');
            var expDateVal, fullDate, month, year, encryptBox;

            expDate.bind('input', function() {
                expDateVal = expDate.val();
                fullDate = expDateVal.split('/');
                encryptBox = angular.element(el)[0].getElementsByClassName('encrypt-box')[0];

                if(expDateVal.length === 7) {
                    if(!encryptBox) {
                        month = angular.element('<input type="text" class="encrypt-box hide" name="month" data-encrypt="month" />');
                        year = angular.element('<input type="text" class="encrypt-box hide" name="year" data-encrypt="year" />');
                    }

                    angular.element(el).append(month, year);

                    month.val(fullDate[0]);
                    year.val(fullDate[1]);
                }

            });
        }
    }
}

function hoverIsActive() {
    return {
        restrict: 'AC',
        scope: {
            hoverIsActive: '@',
            targetHover: '@'
        },
        link: function(scope, el) {

            var className = 'active';
            var classPaused = 'paused';
            var hoverIs = angular.element(el).find('.' + scope.hoverIsActive);
            var targetIs = angular.element(el).find('.' + scope.targetHover);

            hoverIs.bind('mouseenter', function() {
                targetIs.addClass(className);
                targetIs.removeClass(classPaused);
            });

            hoverIs.bind('mouseleave', function() {
                targetIs.removeClass(className);
                targetIs.addClass(classPaused);
                setTimeout(function(){
                    targetIs.removeClass(classPaused);
                }, 500);
            });
        }
    }
}

function historyStepBack() {
    return {
        scope: {
            historyStepBack: '@'
        },
        link: function(scope, el) {

            var winReload;

            angular.element(window)[0].addEventListener('load', function() {
                console.log('statechange');
                winReload = true;
            });
            angular.element(el).bind('click', function() {
                if(winReload && scope.historyStepBack) {
                    window.location.href = scope.historyStepBack;
                    return
                }
                history.go(-1);
            });
        }
    }
}