angular
    .module('smartShip')
    .config(appConfig);

/** @ngInject */
function appConfig($stateProvider, $urlRouterProvider, $sceDelegateProvider, $ocLazyLoadProvider, $facebookProvider, $locationProvider, $logProvider, ROUTES) {

    // $urlRouterProvider.otherwise("/promo");

    /*$urlRouterProvider.otherwise(function($injector) {
        var stateGo = $injector.get('$state');
        stateGo.go(ROUTES.promo);
    });*/

    $logProvider.debugEnabled(false);

    $locationProvider.html5Mode({
        enabled: true
    });

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    // $sceDelegateProvider.resourceUrlWhitelist(['**']);

    $stateProvider
        .state(ROUTES.index, {
            url: "/index",
            templateUrl: "views/common/content.html",
            controller: function ($scope, $rootScope, $state, $location) {

                if (!$rootScope.profile.user) {
                    // $location.path('/');
                    $state.go(ROUTES.login);
                    return;
                }

                if ($rootScope.profile.user.status == 0) {
                    // console.log('$rootScope.profile.user', $rootScope.profile.user);
                    $location.path('/user/registration');
                    return;
                    // $state.go('user.registration');
                }

                $scope.currentUser = $rootScope.profile.user;
                // $location.path('/main');

                if ($state.current.url === '/' + ROUTES.index) {
                    $state.go(ROUTES.index + '.main')
                }

            }
        })
        .state(ROUTES.password, {
            url: '/password/confirm',
            templateUrl: "js/components/users/changePass.html",
            controller: "UsersController"
        })
        .state(ROUTES.info, {
            url: "/info",
            templateUrl: "js/components/info/howTo.html",
            controller: 'InfoController'
        })
        .state(ROUTES.user, {
            abstract: true,
            url: "/user",
            templateUrl: "views/common/content.html"
        })
        //auth
        .state(ROUTES.login, {
            url: "/home?:modal",
            templateUrl: "js/components/login/login.html",
            controller: 'LoginController',
            data: {
                pageTitle: 'Login two columns',
                specialClass: 'gray-bg',
                openedPage: true
            }
        })
        .state(ROUTES.user + '.registration', {
            url: "/registration",
            templateUrl: "js/components/registration/registrationForm.html",
            controller: 'RegistrationFormController',
            data: {
                pageTitle: 'Business form'
            }
        })
        .state(ROUTES.index + '.password', {
            url: "/password/add",
            templateUrl: "js/components/registration/addPassword.html",
            controller: "UsersController"
        })
        .state(ROUTES.index + '.email', {
            url: "/email/edit",
            templateUrl: "js/components/registration/editEmail.html",
            controller: "UsersController"
        })

        .state(ROUTES.index + '.edit', {
            url: "/edit",
            templateUrl: "js/components/users/edit.html",
            controller: "UsersController"
        })
        .state(ROUTES.index + '.main', {
            url: "/main",
            templateUrl: "js/components/statistics/statistics.html",
            controller: 'StatisticController',
            data: {pageTitle: 'Statistics'}
        })
        .state(ROUTES.index + '.orders', {
            url: "/orders",
            templateUrl: "js/components/orders/orders.html",
            controller: 'OrdersController',
            data: {pageTitle: 'orders'},
            resolve: {
                loadPlugin: loadPlugins
            }
        })

        .state(ROUTES.index + '.orders.add', {
            parent: ROUTES.index,
            url: "/orders/add",
            templateUrl: "js/components/orders/create.html",
            controller: 'CreateOrderController'
        })

        .state(ROUTES.index + '.orders.view', {
            parent: ROUTES.index,
            url: "/orders/:id",
            templateUrl: "js/components/orders/view.html",
            controller: "InfoOrderController"
        })

        .state(ROUTES.index + '.shipping', {
            url: "/" + ROUTES.shipping + '?:params',
            templateUrl: "js/components/shipping/shipping.html",
            controller: "ShippingController"
        })

        .state(ROUTES.index + '.transaction', {
            parent: ROUTES.index,
            url: "/" + ROUTES.transaction,
            templateUrl: "js/components/transaction/transaction.html",
            controller: "TransactionController",
            resolve: {
                loadPlugin: loadPlugins
            }
        })

        .state(ROUTES.index + '.card', {
            url: "/" + ROUTES.card + '/:id?:params',
            templateUrl: "js/components/card/card.html",
            controller: "InfoCardController",
            resolve: {
                loadPlugin: loadPlugins
            }
        })

        .state(ROUTES.privacy_policy, {
            url: "/" + ROUTES.privacy_policy,
            templateUrl: "views/common/privacy-policy.html"
        })

        .state(ROUTES.conditions, {
            url: "/" + ROUTES.conditions,
            templateUrl: "views/common/conditions.html"
        })

        .state(ROUTES.promo, {
            url: "/promo",
            templateUrl: "js/components/promo/promo.html"
        });


    //init facebook
    $facebookProvider.setAppId('1058219174214128');
}


function loadPlugins($ocLazyLoad) {
    return $ocLazyLoad.load([
        {
            serie: true,
            files: ['../bower_components/jqueryui-datepicker/datepicker.js', '../bower_components/jqueryui-datepicker/datepicker.css']
        }
    ]);
}