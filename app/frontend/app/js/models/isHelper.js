"use strict";

angular.module('smartShip')
    .service('isHelpers', isHelpers);

/** @ngInject */
function isHelpers() {

    function Model() {}

    angular.extend(Model.prototype, {
        parseLocalStorage: parseLocalStorage
    });

    return new Model();

    function parseLocalStorage(key, prop) {
        if(prop) return JSON.parse(localStorage.getItem(key))[prop];
        return JSON.parse(localStorage.getItem(key));
    }
}