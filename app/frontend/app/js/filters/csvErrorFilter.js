angular
    .module('smartShip')
    .filter('csvErrors', function (CSV_ERRORS) {
        return function (input, language) {
            var obj = CSV_ERRORS;
            var res = [];
            var match;
            if(Array.isArray(input)) {
                // while(input.length > 0){
                input.forEach(function(item){
                    for(var key in obj) {
                        if(item && item.indexOf(obj[key]) !== -1) {
                            match = item.match(/Line(.*)/i);

                            if(match) {
                                res.push(language[key] + '. Line' + match[1]);
                                break;
                            }
                            res.push(language[key]);
                            break;

                        }
                    }
                // }
                });
                return res;
            }
            return input;

        }
    });
