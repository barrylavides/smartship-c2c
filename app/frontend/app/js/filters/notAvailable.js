angular
    .module('smartShip')
    .filter('notAvailable', function () {
        return function (input) {
            if(input) {
                return input;
            }
            input = 'Not available';
            return input;
        }
    });
