angular
    .module('smartShip')
    .filter('rateCardFilter', function () {
        return function (input) {
            if(input.minWeight == 0) {
                return '< - ' + input.maxWeight + ' KG';
            } else if (!input.maxWeight) {
                return input.minWeight + '+' +' KG';
            } else {
                return input.minWeight + ' - ' + input.maxWeight + ' KG';
            }
        }

    });