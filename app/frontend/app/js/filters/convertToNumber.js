angular
    .module('smartShip')
    .filter('convertToNumber', function () {
        return function (input) {
            if(input) {
                input = 1;
                return input;
            }
            input = 0;
            return input;
        }
    });