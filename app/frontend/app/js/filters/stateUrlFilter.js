angular
    .module('smartShip')
    .filter('stateUrlFilter', function () {
        return function (input) {
            if(input) {
                return decodeURIComponent(input);
            }
            input = 0;
            return input;
        }
    });