angular
    .module('smartShip')
    .filter('serviceType', ['appConstants', function (appConstants) {
        return function (input, fieldName) {
            var type = appConstants.SERVICE_TYPE.find(function (type) {
                return type.field == input;
            });
            return type ? type[fieldName] : '';
        }

    }]);