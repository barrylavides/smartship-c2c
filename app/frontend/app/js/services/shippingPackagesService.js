"use strict";

angular.module('smartShip')
    .service('shippingPackagesService', shippingPackagesService);

/** @ngInject */
function shippingPackagesService (
    $q,
    $http,
    appConstants
){
    return {
        getPackages: function () {
            var deferred = $q.defer();

            $http.get(appConstants.API_URL + 'packages')
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function(err){
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        setPackage: function (data) {
            var deferred = $q.defer();

            $http.post(appConstants.API_URL + 'packages', data)
                .then(function(response) {
                    deferred.resolve(response);
                })
                .catch(function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        getBanksName: function(name) {
            var deferred = $q.defer();

            if(!name) name = '';

            $http.get(appConstants.API_URL + 'banks?name=' + name)
                .then(function(response) {
                    deferred.resolve(response);
                })
                .catch(function(err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    }
}