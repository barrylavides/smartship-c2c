"use strict";

angular.module('smartShip')
    .service('fileUploadService', fileUploadService);

/** @ngInject */
function fileUploadService ($q, appConstants, Upload){


    function Model() {}

    angular.extend(Model.prototype, {
        fileUpload: fileUpload
    });

    return new Model();

    function fileUpload(file, options) {
        var deferred = $q.defer();

        var defOptions = {
            url: appConstants.API_URL + 'orders?fileOrders=1' || options.url,
            data: {file: file} || options.data
        };
        if(options) angular.extend(defOptions, options);

        file.upload = Upload.upload(defOptions);

        file.upload
            .then(function(res) {
                deferred.resolve(res);
            })
            .catch(function(err) {
                deferred.reject(err);
            });

        return deferred.promise;
    }
}