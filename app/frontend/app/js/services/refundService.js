"use strict";

angular.module('smartShip')
    .service('refundService', refundService);

/** @ngInject */
function refundService(
    $q,
    $http,
    appConstants
){
    return {
        setRefundPayment: function(data) {
            var defer = $q.defer();

            $http.post(appConstants.API_URL + 'refunds', data)
                .then(function(response) {
                    defer.resolve(response)
                })
                .catch(function(error) {
                    defer.reject(error);
                });

            return defer.promise;
        }
    }
}
