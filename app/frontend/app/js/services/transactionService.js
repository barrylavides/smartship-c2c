"use strict";

angular.module('smartShip')
    .service('transactionService', transactionService);

/** @ngInject */
function transactionService (
    $q,
    $http,
    appConstants
){
    return {
        getShippingCharge: function(params) {
            var defer = $q.defer();

            $http.get(appConstants.API_URL + 'orders?charge=1&' + transformParams(params), {cache: false})
                .then(function(resolve) {
                    defer.resolve(resolve.data);
                })
                .catch(function(err) {
                    defer.reject(err);
                });

            return defer.promise;
        },
        getTransaction: function(params) {
            var defer = $q.defer();

            $http.get(appConstants.API_URL + 'transactions?' + transformParams(params), {cache: false})
                .then(function(resolve) {
                    defer.resolve(resolve.data);
                })
                .catch(function(err) {
                    defer.reject(err);
                });

            return defer.promise;
        },
        getWithdrawal: function(params) {
            var defer = $q.defer();

            $http.get(appConstants.API_URL + 'refunds?' + transformParams(params), {cache: false})
                .then(function(resolve) {
                    defer.resolve(resolve.data);
                })
                .catch(function(err) {
                    defer.reject(err);
                });

            return defer.promise;
        }
    };

    function transformParams(params) {
        var conditions = [];
        for(var key in params) {
            if(params[key]) conditions.push(key + '=' + params[key]);
        }
        conditions = conditions.join('&');
        return conditions;
    }
}
