"use strict";

angular.module('smartShip')
    .service('statisticService', statisticService);

/** @ngInject */
function statisticService (
    $q,
    $http,
    appConstants
){
    return {
        getRegAnalytics: function () {
            var deferred = $q.defer();

            $http.get(appConstants.API_URL + 'analytics/orders')
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function(err){
                    deferred.reject(err);
                });

            return deferred.promise;
        },
        getGraphs: function () {
            var deferred = $q.defer();

            $http.get(appConstants.API_URL + 'analytics/orders/graphs')
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function(err){
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    }
}