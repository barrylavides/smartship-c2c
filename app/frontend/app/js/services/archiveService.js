"use strict";

angular.module('smartShip')
    .service('archiveService', archiveService);

/** @ngInject */
function archiveService(
      $q,
      $http,
      appConstants
    ) {
    return {
        checkPDF: function () {
            var defer = $q.defer();

            $http.get(appConstants.API_URL + 'check/pdf', {cache: false, responseType: 'arraybuffer'})
                .then(function (result) {
                    defer.resolve(result.data)
                })
                .catch(function (err) {
                    defer.reject(err)
                });

            return defer.promise;
        },

        generateTaxInvoicePDF: function (order) {
            var defer = $q.defer();

            var tabWindowId = window.open('about:blank', '_blank');
            var file, fileURL;
            var Backhystory = history.length;
            history.go(-Backhystory);

            $http.get(
                appConstants.API_URL + 'orders/' + order.id + '/pdf/tax-invoice',
                {cache: false, responseType: 'arraybuffer'}
            )
                .then(function (result) {
                    file = new Blob([result.data], {type: 'application/pdf'});
                    fileURL = URL.createObjectURL(file);
                    // tabWindowId.location.href = fileURL;
                    tabWindowId.location.replace(fileURL);

                    defer.resolve(result.data)
                })
                .catch(function (err) {
                    defer.reject(err)
                });

            return defer.promise;
        },

        getShippingCardPDF: function (id) {
            var defer = $q.defer();

            var tabWindowId = window.open('about:blank', '_blank');
            var file, fileURL, URL;

            $http.get(appConstants.API_URL + 'orders/' + id + '/pdf/shipping-card',
                {cache: false, responseType: 'arraybuffer'})
                .then(function (result) {

                    URL = window.URL || window.webkitURL;
                    file = new Blob([result.data], {type: 'application/pdf;base64'});
                    fileURL = URL.createObjectURL(file);

                    tabWindowId.location.replace(fileURL);
                    defer.resolve(result.data);
                })
                .catch(function (err) {
                    console.log('ERROR', err);
                    defer.reject(err);
                });

            return defer.promise;
        },

        getLinkS3Pdf: function (link) {
            var defer = $q.defer();

            var altLink = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/149125/relativity.pdf';

            var req = {
                method: 'GET',
                url: link,
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, DELETE',
                    'Access-Control-Max-Age': '3600',
                    'Access-Control-Allow-Headers': 'x-requested-with',
                    'Authorization': undefined
                }
            };

            $http(req)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        }
    }
}