"use strict";

angular.module('smartShip')
    .service('cvsFileService', cvsFileService);

/** @ngInject */
function cvsFileService (
    $q,
    $http,
    appConstants
){

    return {
        getCSV: function (params, checkList, excludes) {
            var deferred = $q.defer();

            var conditions = [];

            var data = {
                orderIDs: checkList,
                excludedIDs: excludes
            };

            if(checkList.length) {
                data = {
                    orderIDs: checkList,
                    excludedIDs: excludes
                }
            }

            _.forEach(params, function(value, key) {
                if(value) {
                    if(['limit', 'offset', 'sort', 'direction'].indexOf(key) !== -1) return true;
                    conditions.push(key + '=' + value);
                }
            });
            conditions = conditions.join('&');

            $http.post(appConstants.API_URL + 'orders/export?' + conditions, data)
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function(err){
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    }
}