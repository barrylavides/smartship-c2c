"use strict";

angular.module('smartShip')
    .service('userService', [
        '$q',
        '$http',
        'appConstants',
        '$rootScope',
        userService
    ]);

/** @ngInject */
function userService(
    $q,
    $http,
    appConstants,
    $rootScope
){
    var f = {
        editUserField: function (data, id) {
            var deferred = $q.defer();

            $http.patch(appConstants.API_URL + 'users/' + id, data)
                .then(function (response) {
                    deferred.resolve(response.data)
                })
                .catch(function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        },

        mainAuthUsers: function (data) {
            var deferred = $q.defer();

            $http.post(appConstants.API_URL + 'auth/users', data)
                .then(function (response) {
                    deferred.resolve(response.data)
                })
                .catch(function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        },

        changePass: function (data) {
            var deferred = $q.defer();

            $http.patch(appConstants.API_URL + 'newpassword', data)
                .then(function (response) {
                    deferred.resolve(response.data)
                })
                .catch(function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        },

        mainRegisterUsers: function (data) {
            var deferred = $q.defer();

            $http.post(appConstants.API_URL + 'users', data)
                .then(function (response) {
                    deferred.resolve(response.data)
                })
                .catch(function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        },

        singUpFacebook: function (data, userID, syncFB) {

            if (!userID) userID = false;

            var dataSignObj = {};
            var deferred = $q.defer();

            if (data.authResponse) {
                var tokenFB = data.authResponse.accessToken;
                dataSignObj.fbAccessToken = tokenFB;
                dataSignObj.userID = userID;
                if(syncFB) dataSignObj.syncFB = true;
                $http.post(appConstants.API_URL + 'signup/facebook', dataSignObj)
                    .then(function (response) {
                        var user = response.data;
                        deferred.resolve(user);
                    })
                    .catch(function (err) {
                        deferred.reject(err);
                    });
            } else {
                deferred.reject({'data': {'message': 'fb error'}});
            }

            return deferred.promise;
        },

        /**
         * set user as current
         * @param user
         * @returns {Promise}
         */
        setAsCurrent: function (user) {
            var deferred = $q.defer();

            /** model attributes **/

            var userData = localStorage.getItem('currentUser');


            var localAttributes = [
                'id', 'email', 'status', 'token', 'password', 'vat', 'facebookId', 'socials', 'mainEmail',
                'authType', 'postcodeId', 'phone', 'cityId', 'nationalId', 'address', 'firstName', 'lastName',
                'vat', 'language', 'availableAmount', 'availableCount', 'currentLowLimit', 'havePackage', 'packageType'
            ];
            var data = {};
            for (var i in  localAttributes) {
                var name = localAttributes[i];
                data[name] = user[name];
            }

            if (userData) {
                userData = JSON.parse(userData);
                data.authType = userData.authType;
            }

            $http.defaults.headers.common.Authorization = user.token;
            $http.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';
            $http.defaults.headers.common['Cache-Control'] = 'no-cache, no-store, must-revalidate';
            $http.defaults.headers.common['Expires'] = 0;

            if (!$rootScope.profile) {
                $rootScope.profile = {};
            }


            $rootScope.currentUser = data;
            $rootScope.profile.user = data;
            $rootScope.profile.isAuthenticated = true;


            localStorage.setItem('currentUser', JSON.stringify(data));
            localStorage.setItem('isAuthenticated', true);

            // deferred.resolve(user);
            deferred.resolve(data);

            return deferred.promise;
        },

        /**
         * get current user
         *
         * @returns {Promise}
         */
        getCurrent: function (isReplace) {
            var data = {};
            var deferred = $q.defer();

            try {
                data = localStorage.getItem('currentUser');
                data = JSON.parse(data);
            } catch (e) {
                data = {};
            }

            data = data || {};

            if(isReplace && data.availableAmount) {
                return deferred.promise;
            } else if (data.id) {
                $http.defaults.headers.common.Authorization = data.token;
                $http.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';
                $http.get(appConstants.API_URL + 'users/' + data.id)
                    .then(function (user) {
                        if (!user) {
                            f.logout();
                            deferred.reject();
                        } else {
                            user.data.token = data.token;
                            user.data.authType = data.authType;
                            deferred.resolve(user.data);
                            // console.log('getCurrent data', data);
                            f.setAsCurrent(user.data);
                        }
                    })
                    .catch(function (err) {
                        f.logout();
                        deferred.reject();
                    });
            } else {
                f.logout();
                deferred.reject();
            }

            return deferred.promise;
        },

        /**
         * logout user
         */
        logout: function (cb) {
            var data = {};
            // localStorage.clear();
            localStorage.removeItem('isAuthenticated');
            localStorage.removeItem('currentUser');
            $rootScope.profile = {};
            $http.defaults.headers.common.Authorization = '';
            if (cb) {
                cb();
            }
        },

        /**
         * register user
         * @param userId
         * @param data
         */
        registerUser: function (userId, data) {
            var deferred = $q.defer();

            $http.patch(appConstants.API_URL + 'users/registration', data)
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        updateUser: function (userId, data) {
            var deferred = $q.defer();

            $http.patch(appConstants.API_URL + 'users/update', data)
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (err) {
                    deferred.reject(err);
                });

            return deferred.promise;
        }
    };

    return f;
}