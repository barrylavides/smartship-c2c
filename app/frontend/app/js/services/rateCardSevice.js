"use strict";

angular.module('smartShip')
    .service('rateCardService', rateCardService);

/** @ngInject */
function rateCardService (
    $q,
    $http,
    appConstants
){
    return {
        getRateCard: function (type) {
            var deferred = $q.defer();

            $http.get(appConstants.API_URL + 'shipping-charges/' + type)
                .then(function (response) {
                    deferred.resolve(response)
                }, function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        }
    }
}