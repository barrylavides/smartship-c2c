"use strict";

angular.module('smartShip')
    .service('postcodeService', [
        '$q',
        '$http',
        'appConstants',
        postcodeService
        ]);

/** @ngInject */
function postcodeService(
        $q,
        $http,
        appConstants
    ){
    var f = {
        softSearch: function (code) {
            var deferred = $q.defer();
            var url = appConstants.API_URL + 'postcodes/soft-search?code=' + code;

            $http.get(url)
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function(err){
                    deferred.reject(err);
                });

            return deferred.promise;
        },

        getPostcode: function (id) {
            var deferred = $q.defer();

            $http.get(appConstants.API_URL + 'postcode/' + id)
                .then(function (response) {
                    deferred.resolve(response.data)
                })
                .catch(function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        }
    };

    return f;
}