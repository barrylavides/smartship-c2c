"use strict";

angular.module('smartShip')
    .service('languageService', [
        '$q',
        '$http',
        'appConstants',
        languageService
    ]);

/** @ngInject */
function languageService (
    $q,
    $http,
    appConstants
){
    return {
        getCurrentLanguage: function (type) {
            var deferred = $q.defer();

            $http.get(appConstants.API_URL + 'languages/' + type + '/texts')
                .then(function (response) {
                    deferred.resolve(response)
                }, function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        }
    };
}