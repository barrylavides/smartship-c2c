"use strict";

angular.module('smartShip')
    .service('modalBoxService', modalBoxService);

/** @ngInject */
function modalBoxService ($uibModal){

    return {
        modalInstance: null,

        openLoginDialog: function(options) {
            this.modalInstance = $uibModal.open(options);
        },

        closeLoginDialog: function () {
            if(this.modalInstance !== null) this.modalInstance.close();
        }
    }
}