"use strict";

angular.module('smartShip')
    .service('socialNetworkService', [
        '$q',
        '$facebook',
        'userService',
        socialNetworkService
        ]);

/** @ngInject */
function socialNetworkService(
        $q,
        $facebook,
        userService
    ){
    var f = {
        /**
         * login val facebook
         *
         * @returns {Promise}
         */
        facebookSignIn: function () {
            var deferred = $q.defer();
            $facebook.login()
                .then(function (response){
                    return userService.singUpFacebook(response);
                })
                .then(function(user){
                    return userService.setAsCurrent(user);
                })
                .then(function(user){
                    deferred.resolve(user);
                })
                .catch(function (r) {
                    deferred.reject(r);
                });

            return deferred.promise;
        },

        mergeFB: function (userID, syncFB) {
            var deferred = $q.defer();

            $facebook.login()
                .then(function (response){
                    return userService.singUpFacebook(response, userID, syncFB);
                })
                .then(function(user){
                    deferred.resolve(user);
                })
                .catch(function (r) {
                    deferred.reject(r);
                });

            return deferred.promise;
        }
    };

    return f;
}