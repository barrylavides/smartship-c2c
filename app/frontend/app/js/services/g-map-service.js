"use strict";

angular.module('smartShip')
    .service('gMapService', [
        '$q',
        '$http',
        'appConstants',
        gMapService
    ]);

/** @ngInject */
function gMapService (
    $q,
    $http,
    appConstants
){
    return {
        getCoordsList: function (types) {
            var deferred = $q.defer();
            var query = 'drop-off/points?';
            if(Array.isArray(types)) {
                types = types.map(function(item) {
                    if(item) return item = 'types[]=' + item;
                }).join('&');
            } else {
                query+='types[]=';
            }



            $http.get(appConstants.API_URL + query + types)
                .then(function (response) {
                    deferred.resolve(response)
                }, function (err) {
                    deferred.reject(err)
                });

            return deferred.promise;
        }
    };
}