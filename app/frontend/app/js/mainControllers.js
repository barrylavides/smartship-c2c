angular
    .module('smartShip')
    .controller('MainCtrl', MainCtrl);

/** @ngInject */
function MainCtrl($scope, $state, $rootScope, $log, userService, $location,
                  socialNetworkService, notifyService, languageService, appConstants, COUNTRIES) {

    $log.debug('MainCtrl');

    var text, type;
    $scope.languages = appConstants.LANGUAGES;


    $scope.authViaFacebook = authViaFacebook;
    $scope.init = init;
    $scope.logOut = logOut;
    $scope.currentLanguage = currentLanguage;
    $scope.getCurrentContent = getCurrentContent;

    $scope.init();

    currentLanguage($scope.languages.thai, true);

    function init(isReplace) {
        userService
            .getCurrent(isReplace)
            .catch(function (err) {
                // $state.go('login');
                if(!isReplace) {
                    $location.path('/');
                }
            });
    }

    function authViaFacebook(syncFB) {
        var currentUser = localStorage.getItem('currentUser');
        var userID = JSON.parse(currentUser).id;
        //try to login via fb
        socialNetworkService
            .mergeFB(userID, syncFB)
            .then(function (result) {
                var user = angular.extend({}, $rootScope.profile.user, {socials: [result]});
                return userService.setAsCurrent(user);
            })
            .then(function () {
                notifyService({
                    status: 200,
                    message: $scope.curLanguage.SYNCHRONIZED
                });
            })
            .catch(function (err) {
                notifyService({
                    data: {message: err.data.message}
                });
                if (err.status === 401) $scope.logOut();
            });
    }


    function logOut(err) {
        if(err && err.status === 401) {
            userService
                .logout(function () {
                    $state.go('index', {}, {reload: true});
                });
            return true;
        }
        userService
            .logout(function () {
                $state.go('index', {}, {reload: true});
            });
    }


    function currentLanguage(language, flag) {
        type = JSON.parse(localStorage.getItem('language'));
        if(type === null) {
            getCurrentContent(language.type);
        } else if (type && type.LANG !== language.type && !flag) {
            getCurrentContent(language.type);
        } else {
            $scope.curLanguage = JSON.parse(localStorage.getItem('language'));
            refreshData([refreshCarusel, refreshCountries, refreshPackagesDesc]);
        }

    }

    function getCurrentContent(type) {
        languageService.getCurrentLanguage(type)
            .then(function(res) {
                text = angular.extend({}, res.data, {LANG: type});
                localStorage.setItem('language', JSON.stringify(text));
                $scope.curLanguage = JSON.parse(localStorage.getItem('language')) || {};
                refreshData([refreshCarusel, refreshCountries, refreshPackagesDesc]);
            })
            .catch(function(err) {
                console.log('currentLanguage err', err);
            });
    }

    function refreshCarusel() {
        return $scope.carouselItems = [
            {
                title: $scope.curLanguage.LANDING_INTRODUCING,
                image: appConstants.CARUSEL_IMGS.main,
                mask: false
            },
            {
                title: $scope.curLanguage.LANDING_CHEAPER,
                image: appConstants.CARUSEL_IMGS.rates,
                mask: true
            },
            {
                title: $scope.curLanguage.LANDING_THE_WIDEST,
                image: appConstants.CARUSEL_IMGS.widest,
                mask: false
            },
            {
                title: $scope.curLanguage.LANDING_TECHNOLOGY,
                image: appConstants.CARUSEL_IMGS.technology,
                mask: true
            }
        ];
    }


    function refreshCountries() {
        return $scope.countries = [
            {
                name: $scope.curLanguage.PROMO_THAILAND,
                flag: COUNTRIES[0].flag,
                available: true
            },
            {
                name: $scope.curLanguage.PROMO_SINGAPORE,
                flag: COUNTRIES[1].flag,
                available: false
            },
            {
                name: $scope.curLanguage.PHILIPPINES,
                flag: COUNTRIES[2].flag,
                available: false
            },
            {
                name: $scope.curLanguage.INDONESIA,
                flag: COUNTRIES[3].flag,
                available: false
            },
            {
                name: $scope.curLanguage.MALAYSIA,
                flag: COUNTRIES[4].flag,
                available: false
            },
            {
                name: $scope.curLanguage.VIETNAM,
                flag: COUNTRIES[5].flag,
                available: false
            }

        ];
    }

    function refreshPackagesDesc() {
        return $scope.packagesDesc = [
            {
                logo: appConstants.LOGOS.red,
                description: $scope.curLanguage.FOR_CURIOUS
            },
            {
                logo: appConstants.LOGOS.green,
                description: $scope.curLanguage.FOR_SMALL_SELLERS
            },
            {
                logo: appConstants.LOGOS.blue,
                description: $scope.curLanguage.FOR_SMALL_TO_MEDIUM
            },
            {
                logo: appConstants.LOGOS.gold,
                description: $scope.curLanguage.FOR_MEDIUM_TO_LARGE
            }
        ];
    }

    function refreshData(data) {
        data.forEach(function(item){
            item();
        })
    }


}