'use strict';

angular.module('smartShip')
    .controller('TransactionController', TransactionController);

/** @ngInject */
function TransactionController($scope, $log, $timeout, $controller, transactionService, notifyService,
                               TRANS_STATUSES, TYPING_INTERVAL, modalBoxService, shippingPackagesService,
                               refundService) {

    $log.debug('TransactionController');

    $controller('AmountBalanceController', {$scope: $scope});
    $controller('statusOrderController', {$scope: $scope});
    // $controller('filterController', {$scope: $scope});

    $scope.shippingCharge = [];
    $scope.transactions = [];
    $scope.withdrawals = [];
    $scope.transactionsStatuses = TRANS_STATUSES;
    $scope.refundStatuses = {
        pending: $scope.curLanguage.PENDING,
        approved: $scope.curLanguage.APPROVED,
        rejected: $scope.curLanguage.REJECTED
    };
    $scope.pagination = {
        pageLimit: 10,
        currentPage: 1
    };
    $scope.sort = 'id';
    $scope.direction = 'DESC';
    $scope.defaultParams = {
        limit: $scope.pagination.pageLimit,
        sort: $scope.sort,
        direction: $scope.direction
    };
    $scope.defArr = ['trackingId', 'invoiceNo', 'fromDate', 'toDate'];
    $scope.typingInterval = TYPING_INTERVAL;
    $scope.filterParams = $scope.defaultParams;
    $scope.oldFilterParams = {};
    $scope.refund = {};
    $scope.bank = {};

    $scope.modalOptions = {
        animation: true,
        windowClass: 'middle-modal',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/modals/modalRefund.html',
        controller: 'TransactionController',
        scope: $scope
    };

    $scope.pageChanged = pageChanged;
    $scope.getShippingCharge = getShippingCharge;
    $scope.getTransaction = getTransaction;
    $scope.keyUp = keyUp;
    $scope.keyDown = keyDown;
    $scope.orderSubmit = orderSubmit;
    $scope.refundPayment = refundPayment;
    $scope.openDialog = openDialog;
    $scope.closeDialog = closeDialog;
    $scope.getBanksName = getBanksName;
    $scope.getWithdrawal = getWithdrawal;

    $scope.init();

    function pageChanged(params) {
        if($scope.hideBlock == 1) {
            getShippingCharge(params);
            return;
        } else if ($scope.hideBlock == 2) {
            getTransaction(params);
            return;
        }
        getWithdrawal(params);
    }

    function getShippingCharge(params, restartPagin) {
        if(restartPagin) {
            resetPaginationParams(1);
            params = {};
        }

        params.offset = (($scope.pagination.currentPage - 1) * $scope.pagination.pageLimit);

        transactionService.getShippingCharge(params)
            .then(function(res) {
                $scope.shippingCharge = res.rows;
                $scope.pagination.count = res.count;

                if(Array.isArray($scope.shippingCharge)) {
                    $scope.shippingCharge.forEach(function(item) {
                        $scope.statusConverter(item.status, $scope.statuses, item);
                    });
                }

                $scope.pagination.pages = Math.ceil($scope.pagination.count / $scope.pagination.pageLimit);

            })
            .catch(function(err) {
                console.log('Error', err);
            })
    }

    function getTransaction(params, restartPagin) {
        if(restartPagin) {
            resetPaginationParams(2);
            params = {};
        }

        params.offset = (($scope.pagination.currentPage - 1) * $scope.pagination.pageLimit);

        transactionService.getTransaction(params)
            .then(function(res) {
                $scope.transactions = res.rows;
                $scope.pagination.count = res.count;

                if(Array.isArray($scope.transactions)) {
                    $scope.transactions.forEach(function(item) {
                        $scope.statusConverter(item.status, $scope.transactionsStatuses, item, true);
                    });
                }

                console.log('getTransaction');
                $scope.pagination.pages = Math.ceil($scope.pagination.count / $scope.pagination.pageLimit);

            })
            .catch(function(err) {
                console.log('Error', err);
            })
    }

    function getWithdrawal(params, restartPagin) {
        if(restartPagin) {
            resetPaginationParams(3);
            params = {};
        }

        params.offset = (($scope.pagination.currentPage - 1) * $scope.pagination.pageLimit);

        transactionService.getWithdrawal(params)
            .then(function(res) {
                $scope.withdrawals = res.rows;
                $scope.pagination.count = res.count;

                if(Array.isArray($scope.withdrawals)) {
                    $scope.withdrawals.forEach(function(item) {
                        $scope.statusConverter(item.status, $scope.refundStatuses, item, true);
                    });
                }

                $scope.pagination.pages = Math.ceil($scope.pagination.count / $scope.pagination.pageLimit);

            })
            .catch(function(err) {
                console.log('Error', err);
            })
    }

    function resetPaginationParams(flag) {
        $scope.pagination.pageLimit = 10;
        $scope.pagination.currentPage = 1;
        $scope.filterParams = {};
        $scope.hideBlock = flag;
    }

    function keyUp() {
        $timeout.cancel($scope.promiseTimeout);
        if($scope.hideBlock == 1) {
            $scope.promiseTimeout = $timeout(orderSubmit, $scope.typingInterval, true, 'getShippingCharge');
            return true;
        } else if ($scope.hideBlock == 2) {
            $scope.promiseTimeout = $timeout(orderSubmit, $scope.typingInterval, true, 'getTransaction');
            return true;
        }
        $scope.promiseTimeout = $timeout(orderSubmit, $scope.typingInterval, true, 'getWithdrawal');
    }

    function keyDown() {
        $timeout.cancel($scope.promiseTimeout);
    }

    function orderSubmit(invokeApply) {
        for (var key in $scope.filterParams) {
            if ($scope.filterParams[key] != $scope.oldFilterParams[key]) {
                if ($scope.defArr.indexOf(key) !== -1 || $scope.oldFilterParams[key] === undefined) {
                    $scope.hasUpdate = $scope.filterParams[key] !== '';
                }
                $scope[invokeApply]($scope.filterParams);
                $scope.oldFilterParams = angular.copy($scope.filterParams);
                break;
            }
        }
        return $scope.hasUpdate;
    }

    function openDialog() {
        modalBoxService.openLoginDialog($scope.modalOptions);
    }

    function closeDialog() {
        modalBoxService.closeLoginDialog();
    }

    function getBanksName(name) {
        shippingPackagesService.getBanksName(name)
            .then(function(res) {
                $scope.banks = res.data;
            })
            .catch(function(err) {
                console.log('getBanksName error is', err);
            })
    }

    function refundPayment(isValid, data) {
        $scope.submitted = true;

        if(isValid) {
            $scope.sent = true;
            data.bankName = $scope.bank.info.name;
            data.accountNumber = parseInt(data.accountNumber, 10);
            refundService
                .setRefundPayment(data)
                .then(function(res) {
                    closeDialog();
                    notifyService({
                        status: 200,
                        message: $scope.curLanguage.LETTER_SENT
                    });
                    $scope.sent = false;
                    $scope.$parent.availableAmount = 0;
                    $scope.$parent.init();
                })
                .catch(function(err) {
                    console.log('RefundPayment Error ', err);
                });
        }
    }
}