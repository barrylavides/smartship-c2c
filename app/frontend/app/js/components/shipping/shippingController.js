'use strict';

angular.module('smartShip')
    .controller('ShippingController', ShippingController);

/** @ngInject */
function ShippingController($scope, $location, $controller, $stateParams, notifyService, $log, shippingPackagesService) {

    $log.debug('ShippingController');

    $controller('AmountBalanceController', {$scope: $scope});

    $scope.goTo = '' || $stateParams.params;
    $scope.queryParams = $location.search() || {};

    getParams($scope.queryParams);
    getPackages();
    $scope.init();

    function getPackages() {
        shippingPackagesService.getPackages()
            .then(function(res) {
                $scope.packages = res.data;
                $scope.packages.forEach(function(item, i) {
                    item.logo = $scope.packagesDesc[i].logo;
                    item.description = $scope.packagesDesc[i].description;
                });
            })
            .catch(function(err){
                console.log('Err', + err)
            })
    }

    function getParams(arg) {
        var params = arg;

        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                if(key === 'errors') {
                    var msg = decodeURIComponent(params[key]);
                    notifyService({
                        data: {message: msg}
                    });
                }
            }

        }
    }

    function emptyObj(obj) {
        for(var key in obj) {
            return false;
        }
        return true;
    }
}