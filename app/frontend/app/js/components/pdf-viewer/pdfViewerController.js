angular.module('smartShip')
    .controller('PdfViewerController', PdfViewerController);

/** @ngInject */
function PdfViewerController($scope, $log, $stateParams, $sce) {

    $log.debug('PdfViewerController');

    $scope.trustSrc = trustSrc;

    $scope.pdf = {};
    $scope.pdf.src = $stateParams.link || localStorage.getItem('pdfUrl');

    var MyHistory = history.length;
    console.log('MyHistory', MyHistory);
    console.log($scope.pdf.src);

    function trustSrc(src) {
        return $sce.trustAsResourceUrl(src);
    }
}