'use strict';

angular.module('smartShip')
    .controller('RegistrationFormController', RegistrationFormController);

/** @ngInject */
function RegistrationFormController($scope, $state, $rootScope, $log, userService, notifyService, $controller, appConstants) {

    $log.debug('RegistrationFormController');

    $controller('getPostcodeController', {$scope: $scope});

    //variables
    $scope.postcodeList = [];
    $scope.autocomplitePostcodeList = [];
    $scope.postcode = null;
    $scope.languages = appConstants.LANGUAGES;
    $scope.cityList = [{'name': $scope.curLanguage.SELECT + ' ' + $scope.curLanguage.DISTRICT}];

    $scope.registerUser = {
        region: {
            name: ''
        }
    };

    $scope.registerUser.language = $scope.curLanguage.LANG;
    $scope.validationErrors = {};

    //Functions
    $scope.clearForm = clearForm;
    $scope.postcodeSelected = postcodeSelected;
    $scope.regUser = regUser;


    /**
     * clear cityList region and postcode function
     */
    function clearForm() {
        $scope.cityList = [{'name': "Select City"}];
        $scope.registerUser.city = null;
        $scope.registerUser.region = null;
        $scope.registerUser.postcode = null;
    }

    /**
     * function call after postcode was selected
     * update city list and regions
     *
     * @param code
     */
    function postcodeSelected(code) {
        $scope.clearForm();

        var postcodeExist = 0;

        $scope.openDistrict = true;
        _.forEach($scope.postcodeList, function (postcode) {
            if (postcode.code == code) {
                /**** update city list ****/
                $scope.cityList = postcode.cities;
                /** update user postcode **/
                $scope.registerUser.postcode = postcode;
                postcodeExist = 1;
            }

        });
        if (postcodeExist) {
            $scope.validationErrors.postcode = null;
            /** set region **/
            if ($scope.cityList[0].region) {
                $scope.registerUser.region = $scope.cityList[0].region;
            }
            /** set city as default **/
            $scope.registerUser.city = $scope.cityList[$scope.cityList.length - 1]
        } else {
            $scope.validationErrors.postcode = $scope.curLanguage.INCORRECT_POSTCODE;
        }

    }

    //helpers
    /**
     * registration user profile
     */
    function regUser(isValid) {
        $scope.submitted = true;

        if(isValid) {
            userService.getCurrent().then(function (user) {
                var registerUser = _.clone($scope.registerUser);
                if ($scope.registerUser.city) {
                    registerUser.cityId = $scope.registerUser.city.id;
                }
                if ($scope.registerUser.region) {
                    registerUser.regionId = $scope.registerUser.region.id;
                }
                if ($scope.registerUser.postcode) {
                    registerUser.postcodeId = $scope.registerUser.postcode.id;
                }
                registerUser.phone = '66' + registerUser.phone;
                registerUser.language = +registerUser.language;
                userService.registerUser(user.id, registerUser).then(function (response) {

                    response.data.token = user.token;
                    $rootScope.profile.user = response.data;

                    userService.setAsCurrent(response.data).then(function (user) {

                        $rootScope.profile.user.authType = user.authType;
                        $state.go('index', {}, {reload: true});

                    });
                }).catch(function (err) {
                    notifyService({
                        data: {message: err.data.message}
                    });
                    if (err.status === 401) {
                        $scope.logOut();
                    }
                });
            })
        }
    }

    notifyService({
        message: $scope.curLanguage.FILL_OUT_FORM,
        status: 200
    });
}