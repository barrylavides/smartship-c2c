'use strict';

angular.module('smartShip')
    .controller('LoginController', LoginController);

/** @ngInject */
function LoginController($scope, $state, $stateParams, $rootScope, socialNetworkService, $log, $location, $uibModal,
                         userService, notifyService, appConstants, vcRecaptchaService, INTERVAL, modalBoxService, ROUTES) {

    $log.debug('LoginController');

    //variables
    $scope.reCaptchaKey = appConstants.RE_CAPTCHA_KEY;
    $scope.galleryInterval = INTERVAL;
    $scope.modalTemplate = 'views/modals/modalLoginUsers.html';
    $scope.modalOptions = {
        animation: true,
        windowClass: 'middle-modal',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: $scope.modalTemplate,
        controller: 'LoginController',
        scope: $scope
    };

    //Function
    $scope.activeLogin = activeLogin;
    $scope.activeSignUp = activeSignUp;
    $scope.auth = auth;
    $scope.authViaFacebook = authViaFacebook;
    $scope.checkForgotPassword = checkForgotPassword;
    $scope.checkPasswords = checkPasswords;
    $scope.checkUserAccess = checkUserAccess;
    $scope.openLoginDialog = openLoginDialog;
    $scope.closeLoginDialog = closeLoginDialog;
    $scope.register = register;
    $scope.resetData = resetData;
    $scope.sendEmail = sendEmail;
    $scope.setWidgetId = setWidgetId;
    $scope.open = open;

    $scope.resetData();
    $scope.checkUserAccess();
    getParams();

    /**
     * Check user access level
     */
    function checkUserAccess() {
        if ($rootScope.profile.user || $rootScope.profile.isAuthenticated) $state.go(ROUTES.index);
    }

    /**
     * Open SignIn/SignOut modal
     */
    function openLoginDialog() {
        $scope.openModal = true;
        modalBoxService.openLoginDialog($scope.modalOptions);
    }

    /**
     * Close SignIn/SignOut modal
     */
    function closeLoginDialog() {
        $scope.submitted = false;
        modalBoxService.closeLoginDialog();
    }

    /**
     * Go to forgot password state
     */
    function checkForgotPassword() {
        $scope.forgotPassword = true;
    }

    /**
     * Check passwords equal
     */
    function checkPasswords() {
        $scope.notMatchedPassword = false;

        if ($scope.user.password !== $scope.user.passwordRepeat) {
            $scope.notMatchedPassword = true;
            $scope.validationErrors.password = $scope.curLanguage.PASSWORD_DOES_NOT_MATCH;
        }
    }

    /**
     * Local signIn
     * @param isValid
     * @param userVal
     */
    function auth(isValid, userVal) {
        $scope.submitted = true;

        if (isValid) {
            userService.mainAuthUsers(userVal)
                .then(function (result) {
                    return userService.setAsCurrent(result);
                })
                .then(function () {
                    modalBoxService.closeLoginDialog();
                    $state.go('index', {}, {reload: true});
                })
                .catch(function (err) {
                    var msg;
                    if(err.data.message === 'Incorrect email or password!') {
                        msg = $scope.curLanguage.INCORRECT_EMAIL_OR_PASSWORD;
                    } else {
                        msg = $scope.curLanguage.USER_NOT_FOUND;
                    }

                    notifyService({
                        data: {message: msg}
                    });
                })
        }
    }

    /**
     * Local signUp
     * @param isValid
     * @param userVal
     */
    function register(isValid, userVal) {
        $scope.submitted = true;

        if ($scope.notMatchedPassword) {
            isValid = false;
        }

        if (isValid) {
            userVal['g-recaptcha-response'] = userVal.recaptchaResponse;
            userService
                .mainRegisterUsers(userVal)
                .then(function (result) {
                    return userService.setAsCurrent(result);
                })
                .then(function () {
                    modalBoxService.closeLoginDialog();
                    $state.go('index', {}, {reload: true});

                })
                .catch(function (err) {
                    vcRecaptchaService.reload($scope.widgetId);
                    notifyService({
                        data: {message: $scope.curLanguage.EMAIL_ALREADY_IN_USE}
                    });
                })
        }
    }

    /**
     * Send password recovery email
     * @param isValid
     * @param dataUser
     */
    function sendEmail(isValid, dataUser) {
        $scope.submitted = true;

        if (isValid) {
            userService
                .changePass(dataUser)
                .then(function (success) {
                    notifyService({
                        status: 200,
                        message: $scope.curLanguage.SENT_NEW_PASSWORD
                    });
                    modalBoxService.closeLoginDialog();
                })
                .catch(function (err) {
                    var msg = $scope.curLanguage.USER_NOT_FOUND;
                    notifyService({
                        data: {message: msg}
                    });
                })
        }
    }

    /**
     * Go to sign in state
     */
    function activeLogin() {
        $scope.signUp = false;
        $scope.forgotPassword = false;
    }

    /**
     * Go to sign up state
     */
    function activeSignUp() {
        $scope.signUp = true;
        $scope.forgotPassword = false;
    }

    /**
     * Facebook auth
     */
    function authViaFacebook() {
        //try to login via fb
        socialNetworkService
            .facebookSignIn()
            .then(function (result) {
                modalBoxService.closeLoginDialog();
                $state.go('index', {}, {reload: true});
            })
            .catch(function (err) {
                console.log('Error', err);
                $location.path('/');
            });
    }

    /**
     * Set reCaptcha widget id
     * @param widgetId
     */
    function setWidgetId(widgetId) {
        $scope.widgetId = widgetId;
    }

    /**
     * Reset model
     */
    function resetData() {
        $scope.signUp = false;
        $scope.forgotPassword = false;
        $scope.user = {};
        $scope.registerForm = {
            $valid: ''
        };
        $scope.validationErrors = {};
        modalBoxService.modalCreateOrder = null;
    }

    function getParams() {
        if($stateParams.modal === 'true' && !$scope.openModal) openLoginDialog();
    }

    function open(size) {
        var modalInstance = $uibModal.open($scope.modalOptions);

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    }

}