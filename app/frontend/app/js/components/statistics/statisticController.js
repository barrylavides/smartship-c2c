'use strict';

angular.module('smartShip')
    .controller('StatisticController', StatisticController);

/** @ngInject */
function StatisticController($scope, $log, statisticService) {

    $log.debug('StatisticController');

    //variables
    $scope.dates = [];
    $scope.analyticsTitles = [
        $scope.curLanguage.ORDERS_DROPPED_OFF,
        $scope.curLanguage.ORDERS_DELIVERED,
        $scope.curLanguage.ORDERS_PENDING,
        $scope.curLanguage.SHIPPING_CHARGE
    ];
    $scope.graphsData = {};

    // default options of charts
    $scope.datasetOverride = [
        {
            borderWidth: 2,
            lineTension: "0.01",
            borderColor: "#a3e0d4",
            label: $scope.analyticsTitles[1],
            backgroundColor: "rgba(163, 224, 221, .1)",
            pointBorderColor: "rgba(163, 224, 221, .5)",
            pointBackgroundColor: "rgba(163, 224, 221, .5)",
            pointBorderWidth: 1,
            borderDashOffset: 0.0
        },
        {
            borderWidth: 2,
            lineTension: "0.01",
            borderColor: "#60a8d6",
            label: $scope.analyticsTitles[2],
            backgroundColor: "rgba(96, 168, 214, .1)",
            pointBorderColor: "rgba(96, 168, 214, .5)",
            pointBackgroundColor: "rgba(96, 168, 214, .5)",
            pointBorderWidth: 1,
            borderDashOffset: 0.0
        }
    ];

    $scope.options = {
        responsive: true,
        maintainAspectRatio: true,
        scaleShowGridLines: true,
        pointDotStrokeWidth: 1,
        elements: {
            point: {
                radius: 2,
                hitRadius: 2,
                hoverRadius: 2
            }
        },
        legend: {
            display: true,
            labels: {
                fontColor: 'rgba(103, 106, 108, 1)',
                fontSize: 14
            }
        },
        scales: {
            xAxes: [
                {
                    afterTickToLabelConversion: function(data) {
                        var xLabels = data.ticks;

                        xLabels.forEach(function (labels, i) {
                            if (i % 3 !== 0){
                                xLabels[i] = '';
                            }
                        });
                    },
                    afterCalculateTickRotation: function(scale) {
                        // $scope.degreeValue
                        scale.labelRotation = 1;
                    }
                }
            ],
            yAxes: [
                {
                    position: "left",
                    scaleLabel: {
                        display: true,
                        fontFamily: "Montserrat",
                        fontColor: "black"
                    },
                    ticks: {
                        min: 0,
                        beginAtZero: true,
                        stepSize: 1
                    }
                }
            ]
        }
    };


    $scope.getRegAnalytics = getRegAnalytics;
    $scope.isHelper = isHelper;
    $scope.isDegree = isDegree;

    $scope.getRegAnalytics();
    $scope.init(true);
    getGraphs();
    getCurrentLabels();

    function getRegAnalytics() {
        statisticService.getRegAnalytics()
            .then(function(res) {
                var i = 0;
                $scope.analytics = res.data;

                _.forEach($scope.analytics, function(value) {
                    i++;
                    value.title = $scope.analyticsTitles[i-1];
                    value.subTitle = 'New ' + $scope.analyticsTitles[i-1].toLowerCase();
                    if($scope.analyticsTitles[i-1].toLowerCase().match(/pending|กำลังดำเนินการส่ง/))
                        value.subTitle = $scope.analyticsTitles[i-1]
                });
            })
            .catch(function(err) {
                console.log('getRegAnalytics Error', err);
            });
    }

    function getGraphs() {
        statisticService.getGraphs()
            .then(function(res) {
                $scope.graphsData = res.data;
                addProperties($scope.graphsData);
            })
            .catch(function(err) {
                console.log('getGraphs Error', err);
            });
    }

    function isHelper(value) {
        if(value >= 0) return true;
        return false;
    }

    function addProperties(obj) {
        var count = -1;
        for(var key in obj) {
            count++;
            if(obj.hasOwnProperty(key)) {
                if(obj[key].length > 0) {
                    obj[key].forEach(function(item) {
                        item.date = item.date * 1000;
                        item.fullDate = new Date(item.date).toString().split(/\s+/).slice(1, 3).join(' ');
                    });
                    getDataGraphs(obj[key], count);
                }
            }
        }
        return obj;
    }

    function getCurrentLabels() {
        var limit = 30;
        var date;
        var newDate;
        var prevDate;
        var fullDate;
        for(var i = 0; i <= limit; i++){
            date = new Date();
            newDate = date.setDate(date.getDate() - i);
            prevDate = new Date(newDate);
            fullDate = prevDate.toString().split(/\s+/).slice(1, 3).join(' ');
            $scope.dates.push(fullDate);
        }
        $scope.data = [dataLimit(), dataLimit()];
        $scope.labels = $scope.dates.reverse();
        return null;
    }



    function getDataGraphs(arr, count) {
        arr.forEach(function(item) {
            var position = $scope.dates.indexOf(item.fullDate);
            $scope.data[count][position] = item.count;
        });
    }

    function dataLimit() {
        var i = 31;
        var arr = [];
        while(i) {
            i--;
            arr.push(0);
        }
        return arr;
    }

    function isDegree(value) {
        $scope.degreeValue = value;
    }


}