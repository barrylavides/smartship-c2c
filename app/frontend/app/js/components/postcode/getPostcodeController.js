'use strict';

angular.module('smartShip')
    .controller('getPostcodeController', getPostcodeController);

/** @ngInject */
function getPostcodeController($scope, $log, postcodeService) {

    $log.debug('getPostcodeController');

    $scope.postcodeList = [];
    $scope.updatePostcodes = updatePostcodes;

    function updatePostcodes(code) {
        $scope.autocomplitePostcodeList = [];
        postcodeService.softSearch(code).then(function (postcodeList) {
            $scope.postcodeList = postcodeList.data.rows;
            _.forEach(postcodeList.data.rows, function (postcode) {
                $scope.autocomplitePostcodeList.push(postcode.code.toString());
            });

        }).catch(function (err) {
            console.log(err);
            if (err.status === 401) {
                $scope.logOut();
            }
        });
    }
}