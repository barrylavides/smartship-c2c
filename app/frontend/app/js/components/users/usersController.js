'use strict';

angular.module('smartShip')
    .controller('UsersController', UsersController);

/** @ngInject */
function UsersController($scope, $rootScope, $controller, $log, ngDialog, $state, userService, notifyService, postcodeService, appConstants) {

    $log.debug('UsersController');

    $controller('getPostcodeController', {$scope: $scope});

    //variables
    var defaultCities = [{'name': "Select District"}];

    $scope.user = {};
    $scope.forgotPassword = false;

    $scope.registerUser = {
        region: {
            name: ''
        }
    };

    $scope.languages = appConstants.LANGUAGES;

    $scope.addPasswordForm = {
        $valid: ''
    };

    $scope.rootUser = $rootScope.profile.user;

    $scope.registerUser.phone = $rootScope.profile.user.phone;
    $scope.registerUser.nationalId = $rootScope.profile.user.nationalId;
    $scope.registerUser.address = $rootScope.profile.user.address;
    $scope.registerUser.vat = $rootScope.profile.user.vat;
    $scope.registerUser.email = $rootScope.profile.user.email;
    $scope.registerUser.language = $scope.curLanguage.LANG;

    $scope.registerUser.phone = $scope.registerUser.phone.slice(2);

    //edit profile
    $scope.postcodeList = [];
    $scope.autocomplitePostcodeList = [];
    $scope.postcode = null;
    $scope.cityList = defaultCities;

    $scope.validationErrors = {};

    //Functions
    $scope.getLocationInfo = getLocationInfo;
    $scope.checkPasswords = checkPasswords;
    $scope.editUser = editUser;
    $scope.clearForm = clearForm;
    $scope.postcodeSelected = postcodeSelected;
    $scope.userEdit = userEdit;

    /**
     * load location info (postcode, district and region) for user
     * @param options
     */
    function getLocationInfo(options) {
        postcodeService.getPostcode($rootScope.profile.user.postcodeId)
            .then(function (postcode) {
                $scope.postcode = postcode.code;
                return postcodeService.softSearch(postcode.code);
            })
            .then(function (info) {
                var postcodeList = info.data.rows;
                if (postcodeList.length) {
                    var postcode = postcodeList[0];
                    var cityList = postcode.cities || [];
                    $scope.cityList = cityList;
                    $scope.registerUser.postcode = postcode;
                    $scope.validationErrors.postcode = null;

                    var city = cityList.find(function (city) {
                        return city.id == options.cityId;
                    });

                    $scope.registerUser.city = city;

                    /** set region **/
                    if (city) {
                        $scope.registerUser.region = city.region;
                    }
                }
            })
            .catch(function (err) {
                console.log(err);
                if (err.status === 401) {
                    $scope.logOut();
                }
            })
    }

    /**
     * Cheak password equel
     */
    function checkPasswords() {

        $scope.notMatchedPassword = false;

        if ($scope.user.password !== $scope.user.passwordRepeat) {
            $scope.notMatchedPassword = true;
            $scope.addPasswordForm = {
                $valid: false
            };
            $scope.validationErrors.password = $scope.curLanguage.PASSWORD_DOES_NOT_MATCH;
        } else {
            $scope.addPasswordForm = {
                $valid: true
            };
        }
    }

    /**
     * Save user model changing
     * @param isValid
     * @param data
     */
    function editUser(isValid, data) {
        $scope.submitted = true;

        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // var auth = {};

        // auth.type = currentUser.authType;
        var userID = currentUser.id;

        if (isValid) {
            userService.editUserField(data, userID)
                .then(function (res) {
                    notifyService({
                        status: 200,
                        message: res.message
                    });

                    var user = angular.extend({}, currentUser, res);
                    return userService.setAsCurrent(user)
                })
                .then(function () {
                    $state.go('index', {}, {reload: true});
                })
                .catch(function (err) {
                    notifyService({
                        data: {message: err.data.message}
                    });
                    if (err.status === 401) {
                        $scope.logOut();
                    }
                })
        }
    }

    /**
     * clear cityList region and postcode function
     */
    function clearForm() {
        $scope.cityList = [{'name': "Select City"}];
        $scope.registerUser.region = null;
        $scope.registerUser.postcode = null;
    }

    /**
     * function call after postcode was selected
     * update city list and regions
     *
     * @param code
     */
    function postcodeSelected(code) {
        $scope.clearForm();
        var postcodeExist = 0;
        _.forEach($scope.postcodeList, function (postcode) {
            if (postcode.code == code) {
                /**** update city list ****/
                $scope.cityList = postcode.cities;
                /** update user postcode **/
                $scope.registerUser.postcode = postcode;
                postcodeExist = 1;
            }

        });
        if (postcodeExist) {
            $scope.validationErrors.postcode = null;
            /** set region **/
            if ($scope.cityList[0].region) {
                $scope.registerUser.region = $scope.cityList[0].region;
            }
            /** set city as default **/
            $scope.registerUser.city = $scope.cityList[$scope.cityList.length - 1]
        } else {
            $scope.validationErrors.postcode = $scope.curLanguage.INCORRECT_POSTCODE;
        }

    }


    /**
     * update user profile
     */

    function userEdit(isValid, orderVal) {
        $scope.submitted = true;

        if (isValid) {
            userService.getCurrent().then(function (user) {
                var registerUser = _.clone($scope.registerUser);
                if ($scope.registerUser.city) {
                    registerUser.cityId = $scope.registerUser.city.id;
                }
                if ($scope.registerUser.region) {
                    registerUser.regionId = $scope.registerUser.region.id;
                }
                if ($scope.registerUser.postcode) {
                    registerUser.postcodeId = $scope.registerUser.postcode.id;
                }
                registerUser.phone = '66' + registerUser.phone;
                userService.updateUser(user.id, registerUser).then(function (response) {

                    response.data.token = user.token;
                    response.data.authType = user.authType;

                    userService.setAsCurrent(response.data).then(function () {
                        notifyService({status: 200, message: $scope.curLanguage.PROFILE_EDITED});
                        $state.go('index', {}, {reload: true});
                    });
                }).catch(function (err) {
                    notifyService({
                        data: {message: $scope.curLanguage.EMAIL_ALREADY_IN_USE}
                    });
                });
            })
        }

    }

    $scope.getLocationInfo($rootScope.profile.user);

}