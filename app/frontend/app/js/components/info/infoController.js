'use strict';

angular.module('smartShip')
    .controller('InfoController', InfoController);

/** @ngInject */
function InfoController($scope, $log, $controller, $timeout, NgMap, gMapService, GOOGLE_MAP_OPTIONS, PARTNERS_NAME) {
    $log.debug('InfoController');

    $controller('LoginController', {$scope: $scope});

    var service;
    $scope.positions = [];
    $scope.types = [1];
    $scope.markerList = {};
    $scope.showSidebar = false;
    $scope.showDetail = false;
    $scope.fullScreen = false;
    $scope.typeId = false;
    $scope.place = {};
    $scope.partnersName = PARTNERS_NAME;
    $scope.defOptions = {};
    angular.extend($scope.defOptions, GOOGLE_MAP_OPTIONS);


    $scope.getDetail = getDetail;
    $scope.hideMarkers = hideMarkers;
    $scope.showMarkers = showMarkers;
    $scope.address = address;
    $scope.toggleCheck = toggleCheck;
    $scope.isChecked = isChecked;
    $scope.getCurentMarker = getCurentMarker;
    $scope.fullScreenMap = fullScreenMap;

    getCoordsList($scope.types);
    getMarkers();

    NgMap.getMap().then(function(map) {
        $scope.map = map;
        service = new google.maps.places.PlacesService($scope.map);
        $scope.map.addListener('click', function() {
            $scope.map.setZoom(GOOGLE_MAP_OPTIONS.zoom);
            $scope.$apply(function(){
                $scope.showDetail = false;
            })
        });
    });

    function showMarkers() {
        for(var key in $scope.map.markers) {
            $scope.map.markers[key].setMap($scope.map);
        }
    }

    function hideMarkers () {
        for(var key in $scope.map.markers) {
            $scope.map.markers[key].setMap(null);
        }
    }

    function getDetail(event, marker) {
        var lat, lng, index, postcode,
            sType, postOffice, localPhone;

        if(!isNaN(marker)) {
            index = marker;
            lat = event.latitude;
            lng = event.longitude;
            postcode = event.postcode;
            sType = event.serviceType;
            postOffice = event.postOffice;
            localPhone = event.phone;
        } else {
            index = marker.$index;
            lat = marker.pos.latitude;
            lng = marker.pos.longitude;
            postcode = marker.pos.postcode;
            sType = marker.pos.serviceType;
            postOffice = marker.pos.postOffice;
            localPhone = marker.pos.phone;
        }

        angular.extend($scope.place, {
            postOffice: postOffice,
            localPhone: localPhone,
            postcode: postcode,
            sType: sType,
            lat: lat,
            lng: lng
        });

        address($scope.place);

        $timeout(function(){
            $scope.showDetail = true;
        }, GOOGLE_MAP_OPTIONS.sidebarDelay);

        for(var key in $scope.map.markers) {
            markerSettigs($scope.map.markers[key], {icon: GOOGLE_MAP_OPTIONS.map_icons.red, zIndex: 100});
            if(key == index) markerSettigs($scope.map.markers[key], {icon: GOOGLE_MAP_OPTIONS.map_icons.blue, zIndex: 101, index: index});
        }
        $scope.src = "https://maps.googleapis.com/maps/api/streetview?size=400x400&location=" + lat + "," + lng + "&fov=90&heading=235&pitch=10&key=" + GOOGLE_MAP_OPTIONS.api_key;
    }


    function address(data) {
        var latlng = {lat: parseFloat(data.lat), lng: parseFloat(data.lng)};

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( {'location': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (status === GOOGLE_MAP_OPTIONS.geo_results.zero_results) return;
                service.getDetails({ placeId: results[1].place_id}, function(place, status){
                    $scope.$apply(function() {
                        angular.extend(data, place);
                        data.mainPhoto = null;
                        if(place.photos && place.photos != undefined) data.mainPhoto = data.photos[0].getUrl({'maxWidth': 300, 'maxHeight': 300});
                    });
                });
            }

        });
    }

    function getCoordsList(type) {
        gMapService.getCoordsList(type)
            .then(function(res) {
                $scope.positions = res.data;
            })
            .catch(function(err) {
                console.log(err);
            });
    }

    function toggleCheck(type) {
        var idx = $scope.types.indexOf(type);

        if(idx !== -1) {
            $scope.types.splice(idx, 1);
            getCoordsList($scope.types);
            return true;
        }
        $scope.types.push(type);
        getCoordsList($scope.types);
    }

    function isChecked(type) {
        return $scope.types.indexOf(type) !== -1;
    }

    function getMarkers() {
        $scope.partnersName.forEach(function(item, i) {
            gMapService.getCoordsList(item.type)
                .then(function(res) {
                    $scope.markerList['list' + i] = res.data;
                })
                .catch(function (err) {
                    console.log(err)
                })
        });
    }

    function getCurentMarker(mark) {
        $scope.positions.forEach(function(item, i) {
            if(item.postcode.toString().indexOf(mark.postcode.toString()) !== -1) getDetail($scope.positions[i], i)
        });
    }

    function centerChanged(index) {
        $scope.map.panTo($scope.map.markers[index].getPosition());
        $scope.map.setZoom(10);
    }

    function markerSettigs(marker, options) {
        marker.setIcon(options.icon);
        marker.setZIndex(options.zIndex);
        if(options.index){
            centerChanged(options.index);
        }
    }

    function fullScreenMap() {
        $scope.fullScreen = !$scope.fullScreen;
        $timeout(function() {
            google.maps.event.trigger($scope.map, 'resize');
        }, 10);

    }


}