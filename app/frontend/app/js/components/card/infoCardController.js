'use strict';

angular.module('smartShip')
    .controller('InfoCardController', InfoCardController);

/** @ngInject */
function InfoCardController($scope, $stateParams, $log, $filter, $controller, shippingPackagesService) {

    $log.debug('InfoCardController');

    $controller('AmountBalanceController', {$scope: $scope});

    var payloadForm = document.forms.payloadForm;

    $scope.card = {};
    $scope.packages =[];
    $scope.cardInfo = {};
    $scope.personData = {};
    // $scope.card.storeCard = false;
    $scope.expDateErr = false;
    $scope.invalidCardNumber = false;

    $scope.makeAPayment = makeAPayment;
    $scope.getBanksName = getBanksName;

    getPackage();

    function makeAPayment(isValid, card) {
        $scope.submitted = true;

        if(isValid) {
            var cardInfo = angular.copy(card);
            var cardDate = cardInfo.expiration_data.split('/');

            cardInfo.month = cardDate[0];
            cardInfo.year = cardDate[1];

            My2c2p.getEncrypted("cardForm", function(encryptedData, errCode, errDesc) {
                if(errCode!=0) {
                    if(errDesc === 'card number is invalid') {
                        $scope.invalidCardNumber = errDesc;
                        $scope.expDateErr = false;
                        return;
                    } else {
                        $scope.expDateErr = errDesc;
                        $scope.invalidCardNumber = false;
                        return;
                    }
                }

                $scope.personData = encryptedData;
                $scope.personData.cardholderName = cardInfo.cardholderName;
                // $scope.personData.storeCard = $filter('convertToNumber')(cardInfo.storeCard);
                $scope.personData.panBank = cardInfo.bank.value;
                $scope.personData.packageId = $stateParams.id;
                $scope.personData.frontState = $filter('stateUrlFilter')($stateParams.params);

                $scope.expDateErr = false;
                // $state.go('index.shipping');
                // if($stateParams.params) $state.go($stateParams.params);

                setPackage($scope.personData);
            });
        } else {
            console.log('Data not validate');
        }

    }

    function getPackage() {
        shippingPackagesService.getPackages()
            .then(function(res) {
                $scope.packages = res.data;
                $scope.packages.forEach(function(item, i){
                    if (item.id == $stateParams.id) {
                        $scope.cardInfo = item;
                        $scope.cardInfo.description = $scope.packagesDesc[i].description;
                        $scope.cardInfo.logo = $scope.packagesDesc[i].logo;
                    }
                });
            })
            .catch(function(err){
                console.log('Err', + err)
            })
    }

    function setPackage(data) {
        shippingPackagesService.setPackage(data)
            .then(function(res) {
                submitPayloadForm(res);
            })
            .catch(function(err) {
                console.log('ERROR', err);
            });
    }

    function submitPayloadForm(obj) {
        payloadForm.paymentRequest.value = obj.data.payload;
        payloadForm.submit();
    }

    function getBanksName(name) {
        shippingPackagesService.getBanksName(name)
            .then(function(res) {
                $scope.banks = res.data;
            })
            .catch(function(err) {
                console.log('getBanksName error is', err);
            })
    }

}