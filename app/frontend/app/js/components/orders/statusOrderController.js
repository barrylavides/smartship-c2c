'use strict';

angular.module('smartShip')
    .controller('statusOrderController', statusOrderController);

/** @ngInject */
function statusOrderController($scope, $log) {

    $log.debug('statusOrderController');

    $scope.statuses = {
        pending: $scope.curLanguage.PENDING,
        intransit: $scope.curLanguage.INTRANSIT,
        delivered: $scope.curLanguage.DELIVERED,
        cancelled: $scope.curLanguage.CANCELLED,
        rejected: $scope.curLanguage.REJECTED_BY_CUSTOMER,
        failed: $scope.curLanguage.FAILED_TO_DELIVERY
    };

    $scope.statusConverter = statusConverter;
    $scope.disableInvoice = disableInvoice;

    String.prototype.transformString = transformString;

    function statusConverter(status, obj, item, unCut) {
        var i = 1;

        for(var key in obj) {
            if(status === 0 || status === 7) {
                item.statusClass = 'pending'.transformString();
                item.statusText = $scope.curLanguage.PENDING.transformString();
            }
            if(i === status){
                item.statusClass = key;
                item.statusText = obj[key].transformString();
                if(unCut) item.statusText = obj[key];
            }
            i++;
        }
        return item;
    }

    function disableInvoice(obj, status) {
        if(obj && obj.statusText.indexOf(status.transformString()) === -1) return true;
        return false;
    }

    function transformString() {
        return this.split(' ')[0].toLowerCase();
    }
}
