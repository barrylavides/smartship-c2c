'use strict';

angular.module('smartShip')
    .controller('CreateOrderController', CreateOrderController);

/** @ngInject */
function CreateOrderController($scope, $state, $log, orderService, notifyService, appConstants, $controller, modalBoxService, rateCardService) {

    $log.debug('CreateOrderController');

    $controller('getPostcodeController', {$scope: $scope});

    $scope.validationErrors = {
        checkTotalPrice: checkTotalPrice
    };

    //variables
    $scope.postcodeList = [];
    $scope.autocomplitePostcodeList = [];
    $scope.postcode = null;
    $scope.loaderActive = false;
    $scope.cityList = [{'name': $scope.curLanguage.SELECT + ' ' + $scope.curLanguage.DISTRICT}];
    $scope.rateCard = [];

    $scope.newOrder = {
        receiver: {
            province: ''
        },
        insurance: false
    };
    $scope.serviceType = [
        {
            field: 1,
            listName: 'Thai Post',
            name: $scope.curLanguage.SMARTSHIP_THAIPOST
        },
        {
            field: 4,
            listName: 'Kerry',
            name: $scope.curLanguage.SMARTSHIP_KERRY
        },
        {
            field: 5,
            listName: 'Drop off',
            name: $scope.curLanguage.SMARTSHIP_DROP_OFF
        }
    ];
    $scope.shippingType = [
        {
            field: 3,
            name: $scope.curLanguage.EXPRESS
        }
    ];
    $scope.modalTemplate = 'views/modals/modalRateCard.html';
    $scope.modalOptions = {
        animation: true,
        windowClass: 'middle-modal',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: $scope.modalTemplate,
        controller: 'modalOrderController',
        scope: $scope
        // resolve: {
        //     items: function() {
        //         return getRateCardData($scope);
        //     }
        // }
    };

    //Functions
    $scope.getTotalPrice = getTotalPrice;
    $scope.checkPrice = checkPrice;
    $scope.clearOrderForm = clearOrderForm;
    $scope.postcodeSelectInOrders = postcodeSelectInOrders;
    $scope.saveOrder = saveOrder;
    $scope.openLoginDialog = openLoginDialog;
    $scope.closeLoginDialog = closeLoginDialog;
    $scope.setInfoCheck = setInfoCheck;
    // $scope.updatePostcodes = updatePostcodes;

    function checkPrice(e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            e.preventDefault();
        }
    }

    function checkTotalPrice() {
        if ($scope.newOrder.gross < 0) {
            $scope.orderAddForm.$valid = false;

            $scope.validationErrors.totalPrice = 'Price can\'t be negative!';
            return true;
        }
    }

    function clearOrderForm() {
        $scope.cityList = [{'name': "Select City"}];
        $scope.newOrder.receiver.province = null;
        $scope.newOrder.receiver.postcode = null;
    }

    function postcodeSelectInOrders(code) {
        $scope.clearOrderForm();

        $scope.openDistrict = true;

        var postcodeExist = 0;
        _.forEach($scope.postcodeList, function (postcode) {

            if (postcode.code == code) {
                /**** update city list ****/
                $scope.cityList = postcode.cities;
                /** update user postcode **/
                $scope.newOrder.receiver.postcode = postcode.code;
                postcodeExist = 1;
            }

        });
        if (postcodeExist) {
            $scope.validationErrors.postcode = null;
            /** set region **/
            if ($scope.cityList[0].region) {
                $scope.newOrder.receiver.province = $scope.cityList[0].region.name;
            }
            /** set city as default **/
            $scope.newOrder.receiver.district = $scope.cityList[$scope.cityList.length - 1]
        } else {
            $scope.validationErrors.postcode = 'Incorrect postcode. Please try again!';
        }
    }


    function saveOrder(isValid, orderVal) {
        $scope.submitted = true;

        if (isValid) {
            var newOrder = angular.copy($scope.newOrder);
            newOrder.receiver.postcode = parseInt($scope.newOrder.receiver.postcode);
            newOrder.receiver.phone = '66' + $scope.newOrder.receiver.phone;

            if (newOrder.pickup) {
                newOrder.pickup.pickupPostcode = parseInt($scope.newOrder.receiver.postcode);
                newOrder.pickup.pickupPhone = '66' + $scope.newOrder.pickup.pickupPhone;
            }

            if (newOrder.equal) {
                newOrder.pickup = newOrder.receiver;
            }

            $scope.loaderActive = true;
            if (!newOrder.insurance) delete newOrder.declareValue;

            orderService.saveOrder(newOrder).then(function (order) {
                notifyService({
                    message: $scope.curLanguage.ORDER_SAVED, status: 200
                });
                $scope.newOrder = {};

                $state.go('index.orders');

            }).catch(function (err) {
                notifyService({
                    data: {message: err.data.message}
                });
                $scope.loaderActive = false;
                if (err.status === 401) {
                    $scope.logOut();
                }
            })
        }

    }

    function getTotalPrice() {
        $scope.newOrder.gross = $scope.newOrder.orderQuantity * $scope.newOrder.unitPrice;
    }


    function openLoginDialog(type) {
        getRateCardData(type);

        modalBoxService.openLoginDialog($scope.modalOptions);
    }


    function closeLoginDialog() {
        modalBoxService.closeLoginDialog();
    }

    function getRateCardData(type) {
        // var type = scope.newOrder.service;
        return rateCardService.getRateCard(type)
            .then(function(res) {
                $scope.rateCard = res.data;
                return res.data;
            })
            .catch(function(err) {
                console.log('RateCardData ERROR', err);
            });
    }


    function setInfoCheck() {
        $scope.newOrder.insurance = !$scope.newOrder.insurance;
    }

    angular.element(document).on('blur', '#phone', function () {
        if ($scope.newOrder.phone) {
            var correctPhone = $scope.newOrder.phone.replace(/[^0-9.]/g, "");
            $scope.newOrder.phone = correctPhone;
        }
    });
}