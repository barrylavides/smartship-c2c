'use strict';

angular.module('smartShip')
    .controller('filterController', filterController);

/** @ngInject */
function filterController($scope, $log, $timeout, TYPING_INTERVAL) {

    $log.debug('filterController');

    $scope.defArr = ['createdAt', 'receiverEmail', 'receiverName', 'status', 'trackingId', 'updatedAt'];

}