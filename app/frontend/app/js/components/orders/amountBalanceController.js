'use strict';

angular.module('smartShip')
    .controller('AmountBalanceController', AmountBalanceController);

/** @ngInject */
function AmountBalanceController($scope, $log, isHelpers) {
    $log.debug('AmountBalanceController');

    $scope.havePackage = isHelpers.parseLocalStorage('currentUser', 'havePackage');
    $scope.availableAmount = isHelpers.parseLocalStorage('currentUser', 'availableAmount');
    $scope.availableCount = isHelpers.parseLocalStorage('currentUser', 'availableCount');
    $scope.currentLowLimit = isHelpers.parseLocalStorage('currentUser', 'currentLowLimit');
    $scope.packageType = isHelpers.parseLocalStorage('currentUser', 'packageType');
    $scope.isAccessLimit = isAccessLimit;

    function isAccessLimit() {
        if($scope.availableAmount > 0 && $scope.availableAmount <= $scope.currentLowLimit &&
            $scope.availableCount > 0) {
            return 1;
        } else if ($scope.availableAmount <= 0 || $scope.availableCount <= 0){
            return 2;
        } else {
            return 3;
        }
    }


}