'use strict';

angular.module('ordersCtrl', [])
    .controller('OrdersController', OrdersController);

/** @ngInject */
function OrdersController($scope, $timeout, $log, $state, $location, $controller, $filter, archiveService, orderService,
                          notifyService, cvsFileService, TYPING_INTERVAL, modalBoxService,
                          fileUploadService, CSV_FORMAT) {

    $log.debug('OrdersController');

    $controller('statusOrderController', {$scope: $scope});
    $controller('AmountBalanceController', {$scope: $scope});

    //variables
    var promiseTimeout;
    var typingInterval = TYPING_INTERVAL;
    $scope.orderList = [];
    $scope.csvCheckList = [];
    $scope.csvCheckListExludes = [];
    $scope.sort = 'id';
    $scope.direction = 'DESC';
    $scope.cancelAll = false;
    $scope.hasUpdate = false;
    $scope.incorrectFormat = false;
    $scope.upload = false;
    $scope.pagination = {
        pageLimit: 10,
        currentPage: 1
    };
    $scope.hasPdfUrl = null || localStorage.getItem('pdfUrl');
    $scope.orderCount = null;

    $scope.defaultParams = {
        limit: $scope.pagination.pageLimit,
        offset: $scope.offset,
        sort: $scope.sort,
        direction: $scope.direction
    };
    $scope.filterParams = $scope.defaultParams;
    $scope.oldFilterParams = {};
    $scope.defArr = ['createdAt', 'receiverEmail', 'receiverName', 'status', 'trackingId', 'updatedAt'];
    $scope.loaderActive = false;
    $scope.changedList = false;
    $scope.modalOptions = {};

    //Functions
    $scope.getArchive = getArchive;
    $scope.getArchiveTaxInvoice = getArchiveTaxInvoice;
    $scope.getOrders = getOrders;
    $scope.sortOrders = sortOrders;
    $scope.pageChanged = pageChanged;
    $scope.filterOrders = filterOrders;
    $scope.orderSubmit = orderSubmit;
    $scope.goTo = goTo;
    $scope.goToState = goToState;
    $scope.keyUp = keyUp;
    $scope.keyDown = keyDown;
    $scope.getCsvFile = getCsvFile;
    $scope.checkOrderCSV = checkOrderCSV;
    $scope.unCheckAll = unCheckAll;
    $scope.openUploadDialog = openUploadDialog;
    $scope.closeUploadDialog = closeUploadDialog;
    $scope.getShippingCardPDF = getShippingCardPDF;
    $scope.isArrayHere = isArrayHere;
    $scope.isFiltering = isFiltering;

    $scope.getOrders($scope.defaultParams);
    $scope.init();

    function modalOption(options) {
        return $scope.modalOptions = {
            animation: true || options.animation,
            windowClass: 'middle-modal' || options.windowClass,
            ariaLabelledBy: 'modal-title' || options.ariaLabelledBy,
            ariaDescribedBy: 'modal-body' || options.ariaDescribedBy,
            templateUrl: options.templateUrl,
            controller: 'OrdersController' || options.controller,
            scope: $scope || options.scope
        };
    }

    function goTo(state) {
        $state.go(state, {params: encodeURIComponent($location.path())});
    }

    function goToState(state) {
        $state.go(state)
    }

    function pageChanged() {
        $scope.changedList = false;
        $scope.getOrders($scope.defaultParams);
    }

    function filterOrders(isValid, params) {
        $scope.sumbited = true;
        if (isValid) $scope.getOrders(params);
    }

    function keyUp() {
        $timeout.cancel(promiseTimeout);
        promiseTimeout = $timeout(orderSubmit, typingInterval);
    }

    function keyDown() {
        $timeout.cancel(promiseTimeout);
    }

    function orderSubmit() {
        // if(event) return keyCode(event);

        for(var key in $scope.filterParams) {
            if($scope.filterParams[key] != $scope.oldFilterParams[key]) {
                $scope.csvCheckList = [];
                $scope.csvCheckListExludes = [];
                if($scope.defArr.indexOf(key) !== -1 || $scope.oldFilterParams[key] === undefined) {
                    $scope.hasUpdate = $scope.filterParams[key] !== '';
                    $scope.changedList = true;
                }
                $scope.getOrders($scope.filterParams);
                $scope.oldFilterParams = angular.copy($scope.filterParams);
                break;
            }
        }

        return $scope.hasUpdate;
    }


    function getArchive(ids) {
        var fileName = ids + '.pdf';
        var downloadPdf = document.createElement('a');
        document.body.appendChild(downloadPdf);
        downloadPdf.style.cssText = 'display: none';

        archiveService.checkPDF()
            .then(function (res) {
                var file = new Blob([res], {type: 'application/pdf'});
                var fileURL = URL.createObjectURL(file);
                downloadPdf.href = fileURL;
                downloadPdf.download = fileName;
                downloadPdf.click();
            })
            .catch(function (err) {
                console.log('PDF ERROR', err)
            })
    }

    /**
     * sort Orders
     *
     * @param sortField
     */
    function sortOrders(sortField) {
        $scope.sort = sortField;
        if ($scope.defaultParams.direction === 'ASC') {
            $scope.defaultParams.direction = 'DESC';
        } else {
            $scope.defaultParams.direction = 'ASC';
        }
        $scope.getOrders($scope.defaultParams);
    }

    /**
     * get Orders
     */
    function getOrders(params) {
        params.offset = (($scope.pagination.currentPage - 1) * $scope.pagination.pageLimit);

        orderService
            .getOrders(params)
            .then(function (orders) {
                $scope.orderList = orders.data.rows;
                $scope.orderCount = orders.data.count;
                if (isFiltering()) {
                    $scope.csvCheckList = [];
                    $scope.orderList.forEach(function (order) {
                        order.checked = $scope.csvCheckListExludes.indexOf(order.id) === -1;
                        $scope.statusConverter(order.status, $scope.statuses, order);
                    });
                } else {
                    $scope.csvCheckListExludes = [];
                    $scope.orderList.forEach(function(item) {
                        if($scope.csvCheckList.indexOf(item.id) !== -1) {
                            item.checked = true;
                        }
                        $scope.statusConverter(item.status, $scope.statuses, item);
                    });
                }
                $scope.pagination.count = orders.data.count;
                $scope.pagination.pages = Math.ceil($scope.pagination.count / $scope.pagination.pageLimit);
            })
            .catch(function (err) {
                notifyService({
                    data: {message: err.data.message}
                });
                $scope.logOut(err);
        })
    }

    function getArchiveTaxInvoice(orderInfo) {
        if($scope.disableInvoice(orderInfo, $scope.statuses.delivered) || !orderInfo.documentAccess) {
                notifyService({
                    data: {message: $scope.curLanguage.NOT_PERMISSION}
                });
                return false;
        }

        archiveService
            .generateTaxInvoicePDF(orderInfo)
            .then()
            .catch(function (err) {
                console.log('PDF ERROR', err);
            });
    }

    function getCsvFile() {
        var csv_link = document.createElement('a');
        var csvContent, blob, url;
        $scope.loaderActive = true;
        if(!$scope.orderList.length){
            $scope.loaderActive = false;
            return false;
        }
        cvsFileService.getCSV($scope.filterParams, $scope.csvCheckList, $scope.csvCheckListExludes)
            .then(function (result) {
                csvContent = result.data;
                blob = new Blob([csvContent],{type: 'text/csv;charset=utf-8;'});
                url = URL.createObjectURL(blob);
                csv_link.href = url;
                csv_link.setAttribute('download', 'orders.csv');
                csv_link.click();
                $scope.loaderActive = false;
                notifyService({
                        message: 'File downloaded',
                        status: 200
                    });
            })
            .catch(function (err) {
                console.log('CSV ERROR', err);
                notifyService({
                    data: {message: $scope.curLanguage.CSV_EXPORT_ERROR_TOO_MUCH_ORDERS}
                });
                $scope.loaderActive = false;
            });
    }

    function checkOrderCSV(order) {
        var idx = $scope.csvCheckList.indexOf(order.id);
        var idxEx = $scope.csvCheckListExludes.indexOf(order.id);
        if (isFiltering()) {
            if (order.checked) {
                $scope.csvCheckListExludes.push(order.id);
            } else {
                $scope.csvCheckListExludes.splice(idxEx, 1);
            }
        } else {
            if (order.checked) {
                $scope.csvCheckList.splice(idx, 1);
            } else {
                $scope.csvCheckList.push(order.id);
            }
        }
        order.checked = !order.checked;
    }

    function openUploadDialog(flag) {

        var modalTemplate;
        $scope.flag = flag || undefined;

        if(flag || !$scope.havePackage || $scope.isAccessLimit() === 1 || $scope.isAccessLimit() === 2) {
            modalTemplate = 'views/modals/modalPayShipping.html';
        } else {
            modalTemplate = 'views/modals/modalUploadCsv.html';
        }

        if(!flag || !$scope.havePackage || $scope.isAccessLimit() === 1 || $scope.isAccessLimit() === 2) {
            modalBoxService.openLoginDialog(modalOption({
                templateUrl: modalTemplate
            }));
            return;
        }
        $state.go('index.orders.add');

    }

    function closeUploadDialog() {
        if($scope.isAccessLimit() === 3 || $scope.isAccessLimit() === 2){
            modalBoxService.closeLoginDialog();
            return;
        } else if ($scope.isAccessLimit() == 1 && !$scope.flag) {
            modalBoxService.closeLoginDialog();
            return;
        }
        $state.go('index.orders.add');
    }


    $scope.uploadFiles = function(files, errFiles) {

        var file = files[0];

        $scope.files = files;
        $scope.errFiles = errFiles;
        $scope.upload = true;

        if(compareVal(file, 'name', CSV_FORMAT) && $scope.isAccessLimit() === 3) {
            fileUploadService.fileUpload(file)
                .then(function(res){
                    $scope.closeUploadDialog();
                    $scope.upload = false;
                    $scope.incorrectFormat = false;
                    $scope.$parent.getOrders($scope.defaultParams);
                    // $state.reload();
                })
                .catch(function(err){
                    console.log('err', err);
                    $scope.errorMsg = err.status + ': ' + err.data;
                    $scope.errorList = $filter('csvErrors')(err.data.message, $scope.curLanguage);
                    $scope.upload = false;
                    $scope.incorrectFormat = false;
                });
        } else {
            $scope.incorrectFormat = true;
            $scope.upload = false;
            $scope.errorList = [];
        }
    };

    function isArrayHere(data) {
        return !!Array.isArray(data);
    }

    function getShippingCardPDF(order) {
        //drop off service
        if (order.serviceType === 5) {
            var url = '/print?partnerName=' + $scope.profile.user.firstName + ' ' + $scope.profile.user.lastName +
                '&partnerId=' + $scope.profile.user.id + '&orderId=' + order.id;

            return window.open(url,'_blank');
        }

        if(order.documentAccess && order.packageLabel !== null) {
            archiveService
                .getShippingCardPDF(order.id)
                .then()
                .catch(function(err) {
                    console.log(err);
                    notifyService({
                        message: err.data.message,
                        status: 200
                    });
                    console.log('getShippingCardPDF ERROR ', err.data.message);
                });
            return false;
        }
        notifyService({
            status: 200,
            message: $scope.curLanguage.GENERATING_LABEL
        });
    }

    function compareVal(data, prop, val) {
        if(!data) return false;
        var name = data[prop].split('.').pop();
        return name === val;
    }

    function unCheckAll() {
        $scope.cancelAll = true;
        $scope.csvCheckList.length = 0;
        $scope.orderList.forEach(function(item) {
            checkOrderCSV(item);
        });
        return true;
    }

    // Input allow only numeric input [0-9+]
    function keyCode (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if(charCode > 31 && (charCode < 48 || charCode > 57)) return false;
        return orderSubmit.call(event);
    }

    function isFiltering () {
        var params = $scope.filterParams;
        return !!params.createdAt || !!params.receiverEmail || !!params.receiverName ||
            !!params.status || !!params.trackingId || !!params.updatedAt;
    }

}