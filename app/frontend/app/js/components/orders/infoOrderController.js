'use strict';

angular.module('smartShip')
    .controller('InfoOrderController', infoOrderController);

/** @ngInject */
function infoOrderController($scope, $state, $log, $controller, orderService, notifyService, archiveService) {

    $log.debug('infoOrderController');

    $controller('statusOrderController', {$scope: $scope});
    $controller('AmountBalanceController', {$scope: $scope});

    $scope.cvsCheckList = [];
    $scope.getArchiveTaxInvoice = getArchiveTaxInvoice;
    $scope.getShippingCardPDF = getShippingCardPDF;
    $scope.getOrder = getOrder;
    $scope.getOrder();

    function getArchiveTaxInvoice() {
        if($scope.disableInvoice($scope.orderInfo, $scope.statuses.delivered) || !$scope.orderInfo.documentAccess) {
            notifyService({
                data: {message: 'You don\'t have permission'}
            });
            return false;
        }


        archiveService
            .generateTaxInvoicePDF($scope.orderInfo)
            .then()
            .catch(function (err) {
                console.log('PDF ERROR', err)
            })
    }

    function getOrder() {
        orderService.getOrderInfo($state.params.id).
        then(function (result) {
            $scope.orderInfo = result.data;
            $scope.orderInfo.id = $state.params.id;
            $scope.statusConverter($scope.orderInfo.status, $scope.statuses, $scope.orderInfo);

        }, function (err) {
            notifyService({
                data:{message: err.data.message}
            });
            if (err.status === 401) {
                $scope.logOut();
            }
        })

    }

    function getShippingCardPDF(id) {
        if(!id) id = $state.params.id;

        //drop off service
        if ($scope.orderInfo.serviceType === 5) {
            var url = '/print?partnerName=' + $scope.profile.user.firstName + ' ' + $scope.profile.user.lastName +
                '&partnerId=' + $scope.profile.user.id + '&orderId=' + $scope.orderInfo.id;

            return window.open(url,'_blank');
        }

        if($scope.orderInfo.documentAccess && $scope.orderInfo.packageLabel !== null) {
            archiveService
                .getShippingCardPDF(id)
                .then(function (res) {
                    // downloadPdf.href = res.packageLabel;
                    // downloadPdf.setAttribute('download', res.receiverName + '.pdf');
                    // downloadPdf.click();
                })
                .catch(function (err) {
                    console.log('getShippingCardPDF ERROR ', err);
                });
            return false;
        }
        notifyService({
            status: 200,
            message: 'Generating shipping label... Try again later'
        });
    }
}