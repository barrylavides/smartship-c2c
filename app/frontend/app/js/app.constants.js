'use strict';
angular.module('smartShip')
    .constant('appConstants', {
        'APP_NAME': 'smart_ship',
        'BASE_URL': window.location.host,
        'API_URL': '/api/v1/',
        'SERVER_URL': '/',
        'RE_CAPTCHA_KEY': '6LcTkQkUAAAAAEu4aUZHBfR_9WffjPqPbUgqavgj',
        'SERVICE_TYPE': [
            {
                field: 1,
                listName: 'Thai Post',
                name: 'Smartship Thaipost'
            },
            {
                field: 4,
                listName: 'Kerry',
                name: 'Smartship Kerry'
            },
            {
                field: 5,
                listName: 'Drop off',
                name: 'Smartship Drop off'
            }
        ],
        'SHIPPING_TYPE': [
            {
                field: 3,
                name: 'Express (1,2 days)'
            }
        ],
        'LANGUAGES': {
            english: {
                type: 1,
                name: 'English'
            },
            thai: {
                type: 2,
                name: 'ไทย'
            }
        },
        'CARUSEL_IMGS': {
            main: '/img/Smartship_Main.jpg',
            rates: '/img/Shipping_Rates_Image.png',
            widest: '/img/Dropoff_and_Delivery_Network.jpg',
            technology: '/img/Technology.jpeg'
        },
        'LOGOS': {
            red: '/img/logos/curious_shipper.png',
            gold: '/img/logos/power.png',
            blue: '/img/logos/regular.png',
            green: '/img/logos/starter.png'
        }
    })
    .constant('appUrls', {
        'ERROR': {'name': 'error', 'url': '/error'},
        'HOME': {'name': 'home', 'url': '/'}
    })
    .constant('INTERVAL', 5000)
    .constant('TYPING_INTERVAL', 500)
    .constant('STATUSES', {
        pending: 'Pending',
        intransit: 'Intransit',
        delivered: 'Delivered',
        cancelled: 'Cancelled',
        rejected: 'Rejected by Customer',
        failed: 'Failed to Deliver'
    })
    .constant('TRANS_STATUSES', {
        approved: 'Approved',
        payment: 'Payment Failed / Authorization Failed',
        authentication: 'Authentication Rejected(MPI Reject)',
        corporate: 'Corporate BIN Reject',
        fraud: 'Fraud Rule',
        rejected: 'Rejected',
        routing: 'Routing Failed',
        invalid: 'Invalid Promotion',
        process: 'Failed to process payment',
        settled: 'Settled',
        refunded: 'Refunded',
        voided: 'Voided',
        refund: 'Refund Rejected'
    })
    .constant('MAXORDERS', 'You can upload maximum 20 orders at once. Make you file shorter.')
    .constant('TAXINVOICE', '/pdf/tax-invoice')
    .constant('GOOGLE_MAP_OPTIONS', {
        api_key: 'AIzaSyAr0Jtku-1t7nuHs-sE6gHiuLt1H0WHOws',
        coords: {
            lat: 16.670384,
            lng: 101.080314
        },
        geo_results: {
            zero_results: 'ZERO_RESULTS'
        },
        map_icons: {
            red: '../img/map-marker-icon-red.png',
            green: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
            blue: '../img/map-marker-icon-blue.png'
        },
        zoom: 7,
        type_id: {
            roadmap: 'ROADMAP',
            satellite: 'SATELLITE'
        },
        sidebarDelay: 400
    })
    .constant('PARTNERS_NAME', [
        {
            name: 'Thaipost',
            type: 1
        }, {
            name: 'Kerry',
            type: 4
        }, {
            name: 'Airpay',
            type: 6
        }, {
            name: 'Skybox',
            type: 7
        }
    ])
    .constant('CSV_FORMAT', 'csv')
    .constant('ROUTES', {
        login: 'login',
        index: 'index',
        info: 'info',
        promo: 'promo',
        user: 'user',
        password: 'password',
        shipping: 'shipping',
        card: 'card',
        transaction: 'transaction',
        pdf_view: 'pdf-view',
        privacy_policy: 'privacy-policy',
        conditions: 'conditions'
    })
    .constant('COUNTRIES', [
        {
            name: 'thailand',
            flag: '../img/flags/thailand.png',
            available: true
        },
        {
            name: 'singapore',
            flag: '../img/flags/singapore.png',
            available: false
        },
        {
            name: 'philippines ',
            flag: '../img/flags/philippines.png',
            available: false
        },
        {
            name: 'indonesia',
            flag: '../img/flags/indonesia.png',
            available: false
        },
        {
            name: 'malaysia',
            flag: '../img/flags/malaysia.png',
            available: false
        },
        {
            name: 'vietnam',
            flag: '../img/flags/vietnam.png',
            available: false
        }
    ])
    .constant('CSV_ERRORS', {
        UPLOAD_CSV_ITEM_REQUIRED: 'Item name field is required',
        UPLOAD_MAX_20_ORDERS: 'You can upload maximum',
        DECLARED_VALUE_NOT_ZERO: 'Declared value',
        UPLOAD_CSV_RECEIVER_NAME_REQUIRED: 'Receiver name',
        UPLOAD_CSV_RECEIVER_EMAIL_REQUIRED: 'Receiver email',
        UPLOAD_CSV_PHONE_REQUIRED: 'Receiver phone number',
        UPLOAD_CSV_ADDRESS1_REQUIRED: 'Receiver address',
        UPLOAD_CSV_POSTCODE_REQUIRED: 'Receiver postcode',
        UPLOAD_CSV_DISTRICT_REQUIRED: 'Receiver district',
        PHONE_LONG: 'Phone is too long',
        PHONE_INVALID_EXAMPLE: 'Phone format is invalid',
        EMAIL_ALREADY_IN_USE: 'This email already',
        FIELDS_INVALID: 'Fields is invalid',
        SERVICE_TYPE_INVALID: 'Service type is invalid',
        SHIPPING_TYPE_INVALID: 'Shipping type is invalid',
        POSTCODE_INVALID: 'Postcode is invalid',
        FIELDS_EMPTY: 'All fields'
    });

