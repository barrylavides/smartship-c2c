angular
    .module('smartShip')
    .run(appRun);

//Angular run config
/** @ngInject */
function appRun($rootScope, $state, $stateParams, userService, $location, ROUTES, isHelpers) {
    $rootScope.$state = $state;
    $rootScope.profile = {};
    $rootScope.mainClass = '';

    var currentUser, isUserToLogin, infoPage, promoPage,
        privacyPage, indexAddOrderPage, isAuthenticated;

    var stateFrom = new StateFromUrl();

    function StateFromUrl() {
        this.path = $location.path();
        this.hash = $location.hash();
    }

    if(localStorage.getItem('isAuthenticated') || $rootScope.profile.user || $rootScope.profile.isAuthenticated) {
        $state.go(ROUTES.index);
    } else if(stateFrom.path === '/' + ROUTES.info) {
        $state.go(ROUTES.info);
    } else if (stateFrom.path === '/' + ROUTES.promo) {
        $state.go(ROUTES.promo);
    } else if (stateFrom.path === '/home') {
        $state.go(ROUTES.login);
    } else if (stateFrom.path === '/privacy-policy') {
        $state.go(ROUTES.privacy_policy);
    } else if (stateFrom.path === '/conditions') {
        $state.go(ROUTES.conditions);
    } else {
        $state.go(ROUTES.promo);
    }

    $rootScope.$on('$stateChangeStart', onStateChangeStart);

    function onStateChangeStart(event, toState, toParams, fromState, fromParams) {

        isUserToLogin = toState.name === ROUTES.login;
        infoPage = toState.name === ROUTES.info;
        promoPage = toState.name === ROUTES.promo;
        privacyPage = toState.name === ROUTES.privacy_policy;

        indexAddOrderPage = toState.name === ROUTES.index + '.orders.add';
        currentUser = isHelpers.parseLocalStorage('currentUser');
        isAuthenticated = isHelpers.parseLocalStorage('isAuthenticated');

        if(currentUser && currentUser.availableAmount <= 0 ||
            currentUser && !currentUser.havePackage ||
            (currentUser && currentUser.availableCount <= 0 && currentUser.packageType !== 4)
        ) {
            if(indexAddOrderPage) {
                $state.go(ROUTES.index + '.main');
                event.preventDefault();
                return;
            }
        }

        if (toState.data && angular.isDefined(toState.data.specialClass)) {
            $rootScope.mainClass = toState.data.specialClass;
            // return;
        } else {
            $rootScope.mainClass = '';
        }

        if(isUserToLogin && !isAuthenticated) return;

        if(!isAuthenticated) {
            if(!infoPage && !promoPage && !privacyPage) {
                $rootScope.profile = {};
                $state.go(ROUTES.login);
                event.preventDefault();
                return;
            }
        }
    }

    (function () {
        // If we've already installed the SDK, we're done
        if (document.getElementById('facebook-jssdk')) {
            return;
        }

        // Get the first script element, which we'll use to find the parent node
        var firstScriptElement = document.getElementsByTagName('script')[0];

        // Create a new script element and set its id
        var facebookJS = document.createElement('script');
        facebookJS.id = 'facebook-jssdk';

        // Set the new script's source to the source of the Facebook JS SDK
        facebookJS.src = '//connect.facebook.net/en_US/all.js';

        // Insert the Facebook JS SDK into the DOM
        firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
    }());

}