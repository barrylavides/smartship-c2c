(function () {
    angular.module('smartShip', [
        'ngAnimate',
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'chart.js',
        'ngDialog',
        'ngMessages',
        'ngFacebook',
        'ngSanitize',
        'ui.select',
        'cgNotify',
        'vcRecaptcha',
        'ngFileUpload',
        'ngMap',
        'ordersCtrl'
    ])
})();

