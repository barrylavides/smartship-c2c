$( document ).ready(function() {
    var params = getJsonFromUrl();
    params.orderId = "C2C" + params.orderId;
    $('#partnerName').text(params.partnerName);
    $('#partnerId').text(params.partnerId);
    $('#orderId').text(params.orderId);
    JsBarcode("#barcode", params.orderId, {
        width: 2,
        height: 70,
    });
    window.setTimeout(function() {
        window.print();
    }, 50);

    function getJsonFromUrl() {
        var query = location.search.substr(1);
        var result = {};
        query.split("&").forEach(function(part) {
            var item = part.split("=");
            result[item[0]] = decodeURIComponent(item[1]);
        });
        return result;
    }
});