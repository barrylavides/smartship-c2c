angular
    .module('smartShipAdm')
    .filter('serviceType', ['appConstantsAdm', function (appConstants) {
        return function (input, fieldName) {
            var type = appConstants.SERVICE_TYPE.find(function (type) {
                return type.field == input;
            });
            return type ? type[fieldName] : '';
        }

    }]);