function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $locationProvider) {

    $urlRouterProvider.otherwise("/adminpanel");

    $locationProvider.html5Mode({
        enabled: true
    });

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('home', {
            url: "/adminpanel",
            templateUrl: "adminpanel/app/views/common/content.html",
        })
        .state('signin', {
            url        : "/adminpanel/signin",
            controller : 'AuthController',
            templateUrl: "adminpanel/app/js/components/auth/signin.html",
            data       : {
                pageTitle: 'Sign In'
            }

        })
        .state('home.main', {
            url: "/main",
            templateUrl: "adminpanel/app/views/main.html"
        })

        .state('home.orders', {
            url: "/orders",
            templateUrl: "adminpanel/app/js/components/orders/orders.html",
            controller: 'AdminsOrdersController',
            data: {pageTitle: 'orders'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            files: ['../bower_components/jqueryui-datepicker/datepicker.js', '../bower_components/jqueryui-datepicker/datepicker.css']
                        },
                    ]);
                }
            }
        })



}


angular
    .module('smartShipAdm')
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$locationProvider', config])
    .run(function ($rootScope, $state, adminService, $location) {
        $rootScope.$state = $state;
        $rootScope.profile = {};

        console.log('ADMINPANEL!!!!!!!!')
        checkAccess();
        //
        //
        // // $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
        // //     checkAccess();
        // // });
        //
        function checkAccess() {

            adminService.getCurrent().then(function (admin) {
                $rootScope.profile.email = admin.email;
                if (admin.role !== 1) {
                    $rootScope.$evalAsync(function () {
                        $location.path('/adminpanel/main');
                        // $state.go('home');
                    });

                }
            }).catch(function (err) {
                console.log(err)
                console.log('!!!!!!!!!!11')
                $location.path('/adminpanel/signin')
            });
        }
    });
