'use strict';

angular.module('smartShipAdm')
    .controller('AuthController', [
        '$scope',
        '$state',
        '$rootScope',
        '$location',
        'adminService',
        'notifyService',
        function ($scope, $state, $rootScope, $location, adminService, notifyService) {

            $scope.auth = function (isValid, user) {
                $scope.submitted = true;
                if (isValid) {
                    adminService.auth(user).then(function (res) {
                        $location.path('/adminpanel');
                        location.reload();
                    }, function (err) {
                        notifyService({
                            data: {message: err.data.message}
                        });
                    })
                }
            }
        }]);