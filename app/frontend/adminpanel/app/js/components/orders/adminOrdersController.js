'use strict';

angular.module('smartShipAdm')
    .controller('AdminsOrdersController', [
        '$scope',
        '$rootScope',
        'ngDialog',
        'adminOrderService',
        'notifyService',
        function ($scope, $rootScope, ngDialog, adminOrderService, notifyService) {
            var modalCreateOrder = null;

            $scope.newOrder = {};
            $scope.orderList = [];
            $scope.sort = 'createdAt';
            $scope.direction = 'DESC';
            $scope.pagination = {
                pageLimit: 10,
                currentPage: 1
            };


            $scope.sortOrders = sortOrders;
            $scope.pageChanged = pageChanged;
            $scope.getOrders = getOrders;
            

            function pageChanged (){
                $scope.getOrders();
                console.log($scope.pagination);
            };

            /**
             * get Orders
             */
            function getOrders (){
                var offset = (($scope.pagination.currentPage - 1) * $scope.pagination.pageLimit);
                var params = {
                    limit: $scope.pagination.pageLimit,
                    offset: offset,
                    sort: $scope.sort,
                    direction: $scope.direction
                };
                adminOrderService.getAllOrders(params).then(function (orders) {
                    $scope.orderList = orders.data;

                    $scope.pagination.count  = orders.headers()["x-count"];
                    $scope.pagination.pages = Math.ceil($scope.pagination.count / $scope.pagination.pageLimit)
                    console.log($scope.pagination)
                }).catch(function (err) {
                    notifyService({
                        data:{message: err.data.message}
                    });
                })
            }


            function sortOrders(sortField) {
                $scope.sort = sortField;
                if ($scope.direction == 'ASC') {
                    $scope.direction = 'DESC';
                } else {
                    $scope.direction = 'ASC';
                }
                $scope.getOrders();
            }

            $(document).on('blur', '#phone', function() {
                if($scope.newOrder.phone) {
                    var correctPhone = $scope.newOrder.phone.replace(/[^0-9.]/g, "");
                    $scope.newOrder.phone = correctPhone;
                }
            });

            /** get orders **/
            $scope.getOrders({});
        }]);