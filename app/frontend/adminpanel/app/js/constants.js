'use strict';
angular.module('smartShipAdm')
    .constant('appConstantsAdm', {
        'APP_NAME': 'smart_ship',
        'BASE_URL': window.location.host,
        'API_URL': '/api/v1/',
        'API_ADMINPANEL_URL': "/api/adminpanel/v1/",
        'SERVER_URL' : '/',
        'SERVICE_TYPE': [
            {
                field: 1,
                listName: 'Thai Post',
                name: 'Smartship Thaipost'
            },
            {
                field: 4,
                listName: 'Kerry',
                name: 'Smartship Kerry'
            }
        ]
    })
    .constant('appUrls', {
        'ERROR': {'name': 'error', 'url': '/error'},
        'HOME': {'name': 'home', 'url': '/'},
    });
