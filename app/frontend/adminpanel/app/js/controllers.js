/**
 * MainCtrl - controller
 */
function MainCtrl($scope, $rootScope, $window, $location, adminService) {
    $scope.init = function() {

        var currentAdmin = localStorage.getItem('currentAdmin');
        var isAuthenticated = localStorage.getItem('isAuthenticated');
        if(currentAdmin){
            $scope.currentAdmin = JSON.parse(currentAdmin);
            $scope.isAuthenticated = JSON.parse(isAuthenticated);
        }
    };

    this.helloText = 'Welcome to Smart-Ship Admin Panel!';
    this.descriptionText = 'Smart Ship Admin Panel';

    $scope.logOut = function(){
        adminService.logout(function(){
            $window.location.reload();
        });
    };


    $scope.init();

}


angular
    .module('smartShipAdm')
    .controller('MainAdminCtrl', MainCtrl);