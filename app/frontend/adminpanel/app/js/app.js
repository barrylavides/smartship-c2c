(function () {
    angular.module('smartShipAdm', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'ngDialog',
        'cgNotify',
        'ngMessages',
        'autocomplete'
    ])
})();

