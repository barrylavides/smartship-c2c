"use strict";

angular.module('smartShipAdm')
    .service('adminService', [
        '$q',
        '$http',
        'appConstantsAdm',
        '$rootScope',
        function (
            $q,
            $http,
            appConstants,
            $rootScope
        ){
            var f = {

                auth: function (data) {
                    var deferred = $q.defer();
                    $http.post(appConstants.API_ADMINPANEL_URL + 'auth', data)
                        .then(function (response) {
                            return f.setAsCurrent(response.data)
                        })
                        .then(function (admin) {
                            deferred.resolve(admin)
                        })
                        .catch(function (err) {
                            deferred.reject(err)
                        });

                    return deferred.promise;
                },

                /**
                 * set user as current
                 * @param user
                 * @returns {Promise}
                 */
                setAsCurrent: function (user) {
                    var deferred = $q.defer();

                    /** model attributes **/
                    var data = user;

                    $http.defaults.headers.common.Authorization = user.token;
                    $http.defaults.headers.common['Content-Type'] = 'application/json;charset=utf-8';
                    $http.defaults.headers.common['Cache-Control'] = 'no-cache, no-store, must-revalidate';
                    $http.defaults.headers.common['Expires'] = 0;

                    // $http.get(appConstants.API_URL + 'users/' + data.id)
                    //     .then(function (response) {
                    //         if (!response.data.error || response.statusText === 'OK') {
                    //             localStorage.setItem('currentUser', JSON.stringify(data));
                    //             localStorage.setItem('isAuthenticated', true);
                    //         }
                    //         deferred.resolve(response.data);
                    //
                    //     }, function (err) {
                    //         deferred.reject(err)
                    //     });
                    //
                    // return deferred.promise;

                    localStorage.setItem('currentAdmin', JSON.stringify(data));
                    localStorage.setItem('isAuthenticated', true);

                    deferred.resolve(user);

                    return deferred.promise;

                },

                /**
                 * get current user
                 *
                 * @returns {Promise}
                 */
                getCurrent: function () {
                    var data = {};
                    var deferred = $q.defer();

                    try {
                        data = localStorage.getItem('currentAdmin');
                        data = JSON.parse(data);
                    } catch (e) {
                        data = {};
                    }

                    data = data || {};
                    if (data.id) {
                        deferred.resolve(data);
                        f.setAsCurrent(data)
                    } else {
                        f.logout();
                        deferred.reject();
                    }

                    return deferred.promise;
                },

                /**
                 * logout user
                 */
                logout: function (cb){
                    var data = {};
                    localStorage.clear();
                    $http.defaults.headers.common.Authorization = '';
                    if(cb){
                        cb();
                    }
                }
            };

            return f;
        }]);