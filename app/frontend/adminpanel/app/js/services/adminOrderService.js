"use strict";

angular.module('smartShipAdm')
    .service('adminOrderService', [
        '$q',
        '$http',
        'appConstantsAdm',
        '$rootScope',
        function (
            $q,
            $http,
            appConstants,
            $rootScope
        ){
            var f = {
                saveOrder: function (data) {
                    var deferred = $q.defer();

                    $http.post(appConstants.API_URL + 'orders', data)
                        .then(function (response) {
                            deferred.resolve(response);
                        })
                        .catch(function(err){
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                getAllOrders: function (params) {
                    var deferred = $q.defer();
                    var conditions = [];
                    _.forEach(params, function(value, key) {
                        conditions.push(key + '=' + value);
                    });
                    conditions = conditions.join('&');

                    $http.get(appConstants.API_ADMINPANEL_URL + 'orders?' + conditions)
                        .then(function (response) {
                            deferred.resolve(response);
                        })
                        .catch(function(err){
                            deferred.reject(err);
                        });

                    return deferred.promise;
                },

                getOrderInfo: function (id) {
                    var deferred = $q.defer();

                    $http.get(appConstants.API_URL + 'orders/' + id)
                        .then(function (response) {
                            deferred.resolve(response)
                        }, function (err) {
                            deferred.reject(err)
                        })

                    return deferred.promise;
                }
            };

            return f;
        }])