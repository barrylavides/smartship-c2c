"use strict";

angular.module('smartShipAdm')
    .service('notifyService', [
        '$rootScope',

        'notify',
        function (
            $rootScope,
            notify
        ){
            var f = function (data) {
                console.log(data.status);
                switch (data.status) {
                    case 200:
                        notify(
                            {
                                message:  data.message,
                                templateUrl: 'views/common/notify.html',
                                classes: 'alert-success',
                                duration: 2000
                            }
                        );

                        break;

                    // case 401:
                    //     userService.logout();
                    //
                    //     notify(
                    //         {
                    //             message:  data.message,
                    //             templateUrl: 'views/common/notify.html',
                    //             classes: 'alert-success'
                    //         }
                    //     );
                    //
                    //     break;

                    default:
                        if (data.data) {
                            notify(
                                {
                                    message: 'Error :' + data.data.message,
                                    templateUrl: 'views/common/notify.html',
                                    classes: 'alert-danger',
                                    duration: 2000
                                }
                            );
                        } else {
                            console.log(data)
                        }

                        break;
                }
            };

            return f;
        }])