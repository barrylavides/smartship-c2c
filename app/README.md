Acommerce Orders
========================

#Config
```bash
cd config/
cp -r env.dist prod
```
For prod:
```bash
cp -r env.dist prod
```
For dev:
```bash
cp -r env.dist dev
```
Next step, you need to copy env.dist directory and paste her in config directory. Then, rename new folder to selected env. Put credentials from your database into config.json for selected env and into db.js (in your env directory: for prod - directory prod, for dev - directory dev).

Create logs directory in the project folder
```bash
mkdir logs
```

#Install npm packages
```bash
npm install
```
#Install bower packages
```bash
cd frontend/
bower install

cd frontend/adminpanel/ (unneeded for now)
bower install
```
#Run Migrations
```bash
$ node_modules/.bin/sequelize db:migrate --env {env}
```
#Import sqls
```bash
$ node import.js {path to sql migrations}
```
For example: node import.js /migrations/sql

#Start server
```bash
node app.js
```

