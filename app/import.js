'use strict';

let fs = require('fs');
let migrationsPath = process.argv[2];
let sqlOrder = [];
let requiredOrder = [];
let file;
let sequelize = require('./server/models/v1').sequelize;

if (!migrationsPath) {
    throw new Error('Put path to migrations directory!')
}

if (!migrationsPath[0] !== '/') {
    migrationsPath = '/' + migrationsPath;
}

if (!migrationsPath[migrationsPath.length - 1] !== '/') {
    migrationsPath = migrationsPath + '/';
}

let files = fs
    .readdirSync(__dirname + migrationsPath)
    .reverse();

let citiesIndex = files.indexOf('cities.sql');
let postcode2cityIndex = files.indexOf('postcode2city.sql');
let citiesIndexVal = files[citiesIndex];

files[citiesIndex] = files[postcode2cityIndex];
files[postcode2cityIndex] = citiesIndexVal;

files.forEach(function (sqlFile) {
    file = fs.readFileSync(__dirname + migrationsPath + sqlFile);
    doImportSQLData(file.toString());
});

function doImportSQLData(sqlRow) {
    let rowArr = sqlRow.split(' ');
    let queryType = rowArr[0];
    let query = sequelize.query(sqlRow,
        {type: sequelize.QueryTypes[queryType]})
        .then(function (success) {
        })
        .catch(function (err) {
            console.log('Error!', err)
        })
}
