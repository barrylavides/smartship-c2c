'use strict';

const englishLanguage = require('./english.json');
const thailandLanguage = require('./thailand.json');

module.exports = {
    english: englishLanguage,
    thailand: thailandLanguage
};