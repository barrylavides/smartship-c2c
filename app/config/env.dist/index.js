var db = require('./db');

var defaults = {
    server: {
        port: parseInt(process.env.PORT) || 4011,
        host: process.env.HOST || 'localhost',
        https: {
            port: 8888
        }
    },
    db: db,
    jwtKey: 'lyWG7gDpwlvUc2s',
    SITE_KEY : '6LcTkQkUAAAAAEu4aUZHBfR_9WffjPqPbUgqavgj',
    SECRET_KEY : '6LcTkQkUAAAAAD5p6zuVjb0iRj1De9qQl5CmFFft',
    aCommerce: {
        credentials: {
            username: "smartship-dev",
            apikey: "SmartShip123!",
            partnerId: 1212,
            baseUrl: "https://api.acommercedev.com"
        }
    },
    PAYMENT_2C2P_DATA: {
        MERCHANT_ID: "764764000000140",
        SECRET_KEY: "rM6C450dO4xO",
        API_VERSION: "9.1",
        PASSWORD_FOR_KEY: "acompem",
        TRANSACTION_STATUSES: {
            A: 1,
            PF: 2,
            AR: 3,
            CBR: 4,
            FF: 5,
            ROE: 6,
            IP: 7,
            F: 8,
            S: 9,
            RF: 10,
            V: 11,
            RR: 12
        }
    }
};

defaults.server.baseUrl = ['http://', defaults.server.host, ':', defaults.server.port].join('');

module.exports = defaults;