module.exports = {
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 3306,
    dbname: process.env.DB_NAME || 'smart_ship',
    user: process.env.DB_USER || 'dev',
    password: process.env.DB_PASSWORD || 'developer',
    charset: 'utf8mb4',
    connectionRetryCount: 5,
    maxConnections: 10,
    delayBeforeReconnect: 3000,
    showErrors: true,
    config: {
        logging: true
    }
};