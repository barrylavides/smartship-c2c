# DOCKER BUILD REFERENCE
## Version 1

*Needs to be connected to VPN*

#### Step 01.
Login to docker hub registry 
```
$ docker login
```

#### Step 02.
Clone this repository
```
$ git clone https://{{username}}@bitbucket.org/barrylavides/smartship-c2c.git
$ cd smartship-c2c
```

#### Step 03.
Define the image name and build version by supplying the value of smartshipc2c `image` in docker-compose file

```
Example:  docker-compose.yml

version: '2'
services:
  smartshipc2c:
    build: .
    image: smartship-c2c:v0.1
    volumes:
      - ./app:/app
```

#### Step 04.
Build from based image, specify environment {{dev}} and run the container
```
$ ./build.sh {{dev}}
```

#### Step 05.
Run image
```
$ docker run -p 4011:4011 {{image_name:version}} bash -c "node app.js"
```

## Version 2

#### Step 01.
Start docker MySQL container
```
$ docker run -d -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=smartshipc2c_dev -p 3306:3306 --name smartshipc2c_mysql mysql:5.7.17
```

Run MySQL container in interactive mode
```
$ docker exec -it {{MySQL container_id}} /bin/bash
```

#### Step 02.
Start application container and connect it to MySQL container
```
$ NODE_ENV={{prod or dev}} docker run -p 4011:4011 --link smartshipc2c_mysql barrylavides/smartship-c2c-ubuntu:v0.20 bash -c "node app.js"
```

#### Step 03.
Run application container in interactive mode
```
$ docker exec -it {{application container_id}} /bin/bash
```

Start the migration, 
```
$ node_modules/.bin/sequelize db:migrate --env development
```
*If there's an error in the migration, just drop and create the database and then try the migration script again*

#### Step 04.
In the same order, manually migrate postcodes, regions, cities, postcode2city, packages, panBanks, rateValues. SQL script is located in ```./app/migrations/sql/```