FROM smartship-c2c/ubuntu:v0.02
WORKDIR /app
COPY app /app
RUN ln -s "$(which nodejs)" /usr/bin/node && npm install -d && cd ./frontend && bower install --allow-root --force-latest && cd ..
CMD ["node", "app.js"]
